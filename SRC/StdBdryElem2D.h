#ifndef StdBdryElem2D_H
#define StdBdryElem2D_H

#include "StdBdryElemInterp.h"

/**
 * Um objeto do tipo StdBdryElem2D é um transformador de elementos
 * gerais para elementos padrão, e também um interpolador de funções
 * base para o elemento padrão. Tal elemento é nesse caso um elemento da
 * fronteira de um outro elemento
 */
class StdBdryElem2D : public StdBdryElemInterp<2>
{
public:
    typedef Vector<1> LocalPoint;
    typedef Vector<2> GlobalPoint;
    typedef Vector<2> NormalPoint;
    typedef Vector<2> Gradient;
    typedef Matrix<1, 1> Jacobian;

    StdBdryElem2D() = default;
    ~StdBdryElem2D() = default;

    void
    setUp(const GlobalPoint& P1, const GlobalPoint& P2, const int& bdryType);
    void setUp(const Element<2>& elem, const int& bdryType) override;

    /*
     * Funções relacionadas à transformação
     */
    GlobalPoint globalPoint(const LocalPoint& localPoint) override;
    NormalPoint normalPoint(const LocalPoint& localPoint) override;
    Jacobian jacobian(const LocalPoint& localPoint) override;
    double jacDeterminant(const LocalPoint& localPoint) override;

    /*
     * Funções relacionadas às funções base
     */
    std::size_t nBaseFunctions() const override;
    double
    baseFunction(std::size_t index, const LocalPoint& localPoint) override;
    Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint) override;

private:
    double Vnorm;
    GlobalPoint P1_, P2_, V_, N_;
};

#endif
