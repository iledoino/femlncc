#ifndef RigidityMatrix_H
#define RigidityMatrix_H

#include "ElemMatrix.h"
#include "Grid.h"
#include "SprMtr.h"
#include <cassert>
#include <cmath>
#include <vector>

// TO DO: Quebrar Rigidity Matrix para que não seja necessário o arquivo .cc
//#define DEBUG_RIGIDITY

inline double
w(double x, double y)
{
    double eps = 2.22e-16;
    if (std::abs(x) < eps) {
        return 1.;
    }
    else if (std::abs(x - 1.) < eps || std::abs(y - 1.) < eps) {
        return 0.;
    }
    else {
        if (x <= 1. / 3.) {
            return 1.;
        }
        else {
            return 0;
        }
    }
}

/**
 * Um objeto do tipo RigidityMatrix<N> é uma matrix de rigidez para o
 * problema de elementos finitos com elementos em dimensão N
 */
template <std::size_t N>
class RigidityMatrix
{
public:
    RigidityMatrix(const Grid<N>& grid, const ElemMatrix<N>& elemMatrix);
    ~RigidityMatrix() = default;

    virtual void get(std::vector<double>& matrix, std::vector<double>& rhs);
    virtual void get(SprMtr& matrix, SprMtr& rhs);

protected:
    template <class C>
    void get_base(C& matrix, C& rhs);
    template <class C>
    void reposition(C& matrix, C& rhs, std::vector<double>& localMatrix,
            std::vector<double>& localRHS, const Element<N>& elem);
    template <class C>
    void adder(C& matrix, const std::size_t& nRows, const std::size_t& nColumns,
            const std::size_t& row, const std::size_t& column, double entry);

private:
    const Grid<N>& grid_;
    const ElemMatrix<N>& elemMatrix_;
};

template <std::size_t N>
RigidityMatrix<N>::RigidityMatrix(
        const Grid<N>& grid__, const ElemMatrix<N>& elemMatrix__)
    : grid_(grid__), elemMatrix_(elemMatrix__)
{
}

template <std::size_t N>
template <class C>
void
RigidityMatrix<N>::reposition(C& matrix_, C& rhs_,
        std::vector<double>& localMatrix, std::vector<double>& localRHS,
        const Element<N>& elem_)
{
    std::size_t globalSize = grid_.size();
    std::size_t localSize_ = elem_.globalIndices().size();
    for (std::size_t i = 0, ii = 0; i < localSize_; ++i, ii += localSize_) {
        for (std::size_t j = 0; j < localSize_; ++j) {
            /*
             * Adição da matrix local à matrix global
             */
            adder<C>(matrix_, globalSize, globalSize, elem_.globalIndices()[i],
                    elem_.globalIndices()[j], localMatrix[ii + j]);
#ifdef DEBUG_RIGIDITY
            std::cerr << "matriz elementar (" << i << ", " << j << ") --> "
                      << localMatrix[ii + j] << std::endl;
#endif
        }
        /*
         * Adição do lado direito local ao lado direito global
         */
        adder<C>(rhs_, globalSize, 1, elem_.globalIndices()[i], 0, localRHS[i]);
#ifdef DEBUG_RIGIDITY
        std::cerr << "rhs elementar (" << i << ") --> " << localRHS[i]
                  << std::endl;
#endif
    }
}

/*
 * Função que preenche a matrix de rigidez, bem como o termo de carga
 */
template <std::size_t N>
template <class C>
void
RigidityMatrix<N>::get_base(C& matrix_, C& rhs_)
{
#ifdef DEBUG_RIGIDITY
    std::cerr << "\n\nElementos interiores" << std::endl;
#endif
    /*
     * Computação para elementos interiores
     */
    for (const auto& elem_ : grid_.interior()) {
        std::vector<double> localMatrix(elemMatrix_.get(elem_));
        std::vector<double> localRHS(elemMatrix_.rhs(elem_));

#ifdef SECURE_COMPUTATION
        std::size_t localSize = elem_.globalIndices().size();
        assert(localMatrix.size() == (localSize * localSize));
        assert(localRHS.size() == localSize);
#endif

        reposition<C>(matrix_, rhs_, localMatrix, localRHS, elem_);
    }

#ifdef DEBUG_RIGIDITY
    std::cerr << "\n\nElementos de Fronteira" << std::endl;
#endif
    /*
     * Computação para elementos de fronteira
     */
    for (const auto& bdryElem_ : grid_.boundary()) {
        std::vector<double> localMatrix(
                elemMatrix_.get(bdryElem_.elem, bdryElem_.bdryType));
        std::vector<double> localRHS(
                elemMatrix_.rhs(bdryElem_.elem, bdryElem_.bdryType));

#ifdef SECURE_COMPUTATION
        std::size_t localSize = bdryElem_.globalIndices().size();
        assert(localMatrix.size() == (localSize * localSize));
        assert(localRHS.size() == localSize);
#endif

        reposition<C>(matrix_, rhs_, localMatrix, localRHS, bdryElem_.elem);
    }
}

template <std::size_t N>
void
RigidityMatrix<N>::get(std::vector<double>& matrix_, std::vector<double>& rhs_)
{
    std::size_t globalSize = grid_.size();
    matrix_.resize(globalSize * globalSize);
    rhs_.resize(globalSize);
    get_base<std::vector<double>>(matrix_, rhs_);

    for (const auto& bdryElem_ : grid_.boundary()) {
        auto ind = bdryElem_.elem.globalIndices();
        for (std::size_t i = 0; i < ind.size(); ++i) {
            for (std::size_t j = ind[i] * globalSize;
                    j < (ind[i] + 1) * globalSize; ++j) {
                matrix_[j] = 0.;
            }
            matrix_[ind[i] * globalSize + ind[i]] = 1.;

            rhs_[ind[i]] = w(bdryElem_.elem.points()[i][0],
                    bdryElem_.elem.points()[i][1]);
        }
    }
}

template <std::size_t N>
void
RigidityMatrix<N>::get(SprMtr& matrix_, SprMtr& rhs_)
{
    std::size_t globalSize = grid_.size();
    matrix_destroy(matrix_);
    matrix_create(&matrix_, globalSize, globalSize);
    matrix_destroy(rhs_);
    matrix_create(&rhs_, globalSize, 1);
    get_base<SprMtr>(matrix_, rhs_);
}

#endif
