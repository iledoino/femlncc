#include "StdBdryElem1D.h"
#include <cassert>

void
StdBdryElem1D::setUp(double P__, const int& bdryType__)
{
    P_[0] = P__;
    bdryType() = bdryType__;
}

void
StdBdryElem1D::setUp(const GlobalPoint& P__, const int& bdryType__)
{
    P_ = P__;
    bdryType() = bdryType__;
}

void
StdBdryElem1D::setUp(const Element<1>& elem__, const int& bdryType__)
{
#ifdef SECURE_COMPUTATION
    assert(elem__.points().size() == 1)
#endif
            P_
            = elem__.points()[0];
    bdryType() = bdryType__;
}
