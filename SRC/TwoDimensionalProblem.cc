#include "TwoDimensionalProblem.h"

#define SUPG

inline double
helem(StdElemInterp<2>& interp)
{
    double havrg = 0.;
    if (interp.nBaseFunctions() == 4) {
        Vector<2> p0(interp.globalPoint({ -1., -1. }));
        Vector<2> p1(interp.globalPoint({ 1., -1. }));
        Vector<2> p2(interp.globalPoint({ 1., 1. }));
        Vector<2> p3(interp.globalPoint({ -1., 1. }));

        Vector<2> diag1, diag2;
        diag1[0] = diag2[0] = diag1[1] = diag2[1] = 0.;

        for (int i = 0; i < 2; ++i) {
            diag1[i] = p2[i] - p0[i];
            diag2[i] = p3[i] - p1[i];
        }

        double nd1 = norm(diag1), nd2 = norm(diag2);
        if (nd1 > nd2)
            havrg += nd1;
        else
            havrg += nd2;
    }
    else if (interp.nBaseFunctions() == 3) {
        Vector<2> p0(interp.globalPoint({ 0., 0. }));
        Vector<2> p1(interp.globalPoint({ 0., 1. }));
        Vector<2> p2(interp.globalPoint({ 1., 0. }));

        Vector<2> diag1, diag2, diag3;
        diag1[0] = diag2[0] = diag3[0] = diag1[1] = diag2[1] = diag3[1] = 0.;

        for (int i = 0; i < 2; ++i) {
            diag1[i] = p0[i] - p1[i];
            diag2[i] = p1[i] - p2[i];
            diag3[i] = p0[i] - p2[i];
        }

        double nd1 = norm(diag1), nd2 = norm(diag2), nd3 = norm(diag3);
        havrg += nd3 * nd2 * nd1
                / std::sqrt(std::abs((nd1 + nd2 + nd3) * (nd2 + nd3 - nd1)
                        * (nd3 + nd1 - nd2) * (nd1 + nd2 - nd3)));
    }
    return havrg;
}

/*
 * PROBLEMA CONTÍNUO: encontrar u pertencente a U tal que
 *
 *    { -∇·(Κ ∇u) + β ∇u + α u = f    em   Ω,
 *
 * sujeito às condições de fronteira:
 *
 *    {  ∇u·n + γ u = g    em   ΓR
 *    {  ∇u·n       = h    em   ΓN  .
 *    {   u         = w    em   ΓD
 *
 *
 *
 * PROBLEMA DISCRETO: encontrar uh pertencente a Uh tal que
 *
 *    { ∫_Ω (Κ ∇uh ∇vh) dA + ∫_Ω (β ∇uh vh) dA + ∫_Ω (α uhvh) dA + BDRY_LHS
 *    {
 *    {         = ∫_Ω (fvh) dA + BDRY_RHS,
 *
 * para todo uh pertencente a Uh, onde BDRY_LHS e BDRY_RHS são somas
 * de integrais definidas sobre os diferentes tipos de fronteira: Robin,
 * Neumann e Dirichlet. A saber, tais integrais são
 *
 * Robin:
 *      BDRY_LHS: ∫_ΓR  (γ uhvh) dS
 *      BDRY_RHS: ∫_ΓR  (gvh) dS
 *
 * Neumann:
 *      BDRY_LHS: 0
 *      BDRY_RHS: ∫_ΓN  (hvh) dS
 *
 * Dirichlet:
 *      BDRY_LHS: α_D ∫_ΓD  (∇vh·n uh) dS - ∫_ΓD  (∇uh·n vh) dS + β_D ∫_ΓD
 * (uhvh) dS BDRY_RHS: α_D ∫_ΓD  (∇vh·n w) dS + β_D ∫_ΓD  (wvh) dS
 *
 * No caso de Dirichlet, o método de Nitsche é empregado, e duas contantes
 * são necessárias: α_D, que multiplica os termos com gradiente, e
 * β_D que multiplica os termos com avaliações de funções base uh e vh.
 */

/***********************************************************************
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                         FORMA BILINEAR                            **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 ***********************************************************************
 */

std::vector<double> BilFormFunc2D::defaultParams_ = std::vector<double>();

BilFormFunc2D::BilFormFunc2D(StdElemInterp<2>& interp__,
        StdBdryElem2D& interpBdry__, const std::vector<double>& params__)
    : BilinearFormFunction<2>(interp__, interpBdry__), bc_(), params_(params__)
{
    bc_.resize(4);
    bc_[0] = (&BilFormFunc2D::no_boundary_condition);
    bc_[1] = (&BilFormFunc2D::robin_boundary_condition);
    bc_[2] = (&BilFormFunc2D::neumann_boundary_condition);
    bc_[3] = (&BilFormFunc2D::dirichlet_boundary_condition);

    defaultParams_.resize(10);
    defaultParams_[0] = -1.;
    defaultParams_[1] = 1.;
}

double
BilFormFunc2D::evaluate_interior(
        std::size_t i, std::size_t j, const LocalPointN& localPoint)
{
    auto grad_uh = stdElemInterp().baseGradient(i, localPoint);
    auto grad_vh = stdElemInterp().baseGradient(j, localPoint);
    auto uh = stdElemInterp().baseFunction(i, localPoint);
    auto vh = stdElemInterp().baseFunction(j, localPoint);
    /*
     * Implementação de ∫_Ω (Κ ∇uh ∇vh) dA + ∫_Ω ( β ∇uh vh) dA + ∫_Ω (α uhvh)
     * dA
     *
     */
#ifdef SUPG
    double vel = norm(beta(localPoint)), h_ = helem(stdElemInterp());
    double pe(vel * h_ * 0.5 / params_[2]);
    // double alpha_ = pe>=1.? h_*0.5/vel : h_*h_/(12.*params_[2]);
    double alpha_ = h_ * 0.5 * (1.0 / std::tan(pe) - 1.0 / pe) / params_[2];

    return mult(mult(kappa(localPoint), grad_uh), grad_vh)
            + mult(beta(localPoint), grad_uh) * vh + alpha(localPoint) * uh * vh
            + alpha_ * mult(beta(localPoint), grad_vh)
            * (mult(beta(localPoint), grad_uh) + alpha(localPoint) * uh);
#else
    return mult(mult(kappa(localPoint), grad_uh), grad_vh)
            + mult(beta(localPoint), grad_uh) * vh
            + alpha(localPoint) * uh * vh;
#endif
}

double
BilFormFunc2D::evaluate_boundary(
        std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
{
    /*
     * Implementação de integrações sobre a fronteira
     */
#ifdef SECURE_COMPUTATION
    return (this->*bc_.at(stdBdryElemInterp().bdry_type()))(i, j, localPoint);
#else
    return (this->*bc_[stdBdryElemInterp().bdry_type()])(i, j, localPoint);
#endif
}

double
BilFormFunc2D::no_boundary_condition(
        std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
{
    /*
     * Caso de fronteiras fantasma
     */
    return 0.;
}

double
BilFormFunc2D::robin_boundary_condition(
        std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
{
    auto uh = stdBdryElemInterp().baseFunction(i, localPoint);
    auto vh = stdBdryElemInterp().baseFunction(j, localPoint);
    /*
     * Implementação de ∫_ΓR  (γ uhvh) dS
     */
    return gamma(localPoint) * uh * vh;
}

double
BilFormFunc2D::neumann_boundary_condition(
        std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
{
    /*
     * Para o caso de Neumann, nada necessita ser adicionado
     */
    return 0.;
}

double
BilFormFunc2D::dirichlet_boundary_condition(
        std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
{
    auto grad_uh = stdBdryElemInterp().baseGradient(i, localPoint);
    auto grad_vh = stdBdryElemInterp().baseGradient(j, localPoint);
    auto uh = stdBdryElemInterp().baseFunction(i, localPoint);
    auto vh = stdBdryElemInterp().baseFunction(j, localPoint);
    auto n = stdBdryElemInterp().normalPoint(localPoint);
    /*
     * Implementação de α_D ∫_ΓD  (∇vh·n uh) dS - ∫_ΓD  (∇uh·n vh) dS + β_D ∫_ΓD
     * (uhvh) dS
     */
    return nitsche_grad_const() * mult(grad_vh, n) * uh - mult(grad_uh, n) * vh
            + nitsche_func_const() * uh * vh;
}

/***********************************************************************
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                           FORMA LINEAR                            **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 ***********************************************************************
 */

std::vector<double> LinearFormFunc2D::defaultParams_ = std::vector<double>();

LinearFormFunc2D::LinearFormFunc2D(StdElemInterp<2>& interp__,
        StdBdryElem2D& interpBdry__, const std::vector<double>& params__)
    : LinearFormFunction<2>(interp__, interpBdry__), bc_(), params_(params__)
{
    bc_.resize(4);
    bc_[0] = (&LinearFormFunc2D::no_boundary_condition);
    bc_[1] = (&LinearFormFunc2D::robin_boundary_condition);
    bc_[2] = (&LinearFormFunc2D::neumann_boundary_condition);
    bc_[3] = (&LinearFormFunc2D::dirichlet_boundary_condition);

    defaultParams_.resize(10);
    defaultParams_[0] = -1.;
    defaultParams_[1] = 1.;
}

double
LinearFormFunc2D::evaluate_interior(
        std::size_t i, const LocalPointN& localPoint)
{
    auto vh = stdElemInterp().baseFunction(i, localPoint);
    /*
     * Implementação de ∫_Ω (fvh) dA
     */
#ifdef SUPG
    auto grad_vh = stdElemInterp().baseGradient(i, localPoint);
    double vel = norm(Vector<2>({ 1., 3. })), h_ = helem(stdElemInterp());
    double pe(vel * h_ * 0.5 / params_[2]);
    // double alpha_ = pe>=1.? h_*0.5/vel : h_*h_/(12.*params_[2]);
    double alpha_ = h_ * 0.5 * (1.0 / std::tan(pe) - 1.0 / pe) / params_[2];

    return f(localPoint) * vh
            + alpha_ * mult(Vector<2>({ 1., 3. }), grad_vh) * (f(localPoint));
#else
    return f(localPoint) * vh;
#endif
}

double
LinearFormFunc2D::evaluate_boundary(
        std::size_t i, const LocalPointNm1& localPoint)
{
    /*
     * Implementação de integrações sobre a fronteira
     */
#ifdef SECURE_COMPUTATION
    return (this->*bc_.at(stdBdryElemInterp().bdry_type()))(i, localPoint);
#else
    return (this->*bc_[stdBdryElemInterp().bdry_type()])(i, localPoint);
#endif
}

double
LinearFormFunc2D::no_boundary_condition(
        std::size_t i, const LocalPointNm1& localPoint)
{
    /*
     * Caso de fronteiras fantasma
     */
    return 0.;
}

double
LinearFormFunc2D::robin_boundary_condition(
        std::size_t i, const LocalPointNm1& localPoint)
{
    auto vh = stdBdryElemInterp().baseFunction(i, localPoint);
    /*
     * Implementação de ∫_ΓR  (gvh) dS
     */
    return g(localPoint) * vh;
}

double
LinearFormFunc2D::neumann_boundary_condition(
        std::size_t i, const LocalPointNm1& localPoint)
{
    auto vh = stdBdryElemInterp().baseFunction(i, localPoint);
    /*
     * Implementação de ∫_ΓN  (hvh) dS
     */
    return h(localPoint) * vh;
}

double
LinearFormFunc2D::dirichlet_boundary_condition(
        std::size_t i, const LocalPointNm1& localPoint)
{
    auto grad_vh = stdBdryElemInterp().baseGradient(i, localPoint);
    auto vh = stdBdryElemInterp().baseFunction(i, localPoint);
    auto w_ = w(localPoint);
    auto n = stdBdryElemInterp().normalPoint(localPoint);
    /*
     * Implementação de α_D ∫_ΓD  (∇vh·n w) dS + β_D ∫_ΓD  (wvh) dS
     */
    return nitsche_grad_const() * mult(grad_vh, n) * w_
            + nitsche_func_const() * vh * w_;
}
