#ifndef GLQuadRule_H
#define GLQuadRule_H

#include "GLRule.h"
#include "LinearAlgebra.h"
#include <cassert>

/**
 * Um objeto do tipo GLQuadRule fornece pontos e pesos necessários
 * para se realizar uma integração numérica em 1D usando o método de
 * Gauss-Legendre.
 * PS: Tais pontos e pesos estão relacionados ao domínio [-1., 1.]
 */
template <std::size_t N>
class GLQuadRule : public GLRule1D
{
public:
    typedef Vector<N> Points;
    typedef Vector<N> Weights;

    GLQuadRule();
    ~GLQuadRule() = default;

    std::size_t nPoints(std::size_t index) const override;

    Points points() const { return points_; }
    Weights weights() const { return weights_; }

    double point(std::size_t index) const override;
    double weight(std::size_t index) const override;

private:
    Points points_;
    Weights weights_;
};

template <std::size_t N>
std::size_t GLQuadRule<N>::nPoints(std::size_t /* index */) const
{
    return N;
}

template <std::size_t N>
double
GLQuadRule<N>::point(std::size_t index) const
{
#ifdef SECURE_COMPUTATION
    assert(index < N);
#endif
    return points_[index];
}

template <std::size_t N>
double
GLQuadRule<N>::weight(std::size_t index) const
{
#ifdef SECURE_COMPUTATION
    assert(index < N);
#endif
    return weights_[index];
}

#endif
