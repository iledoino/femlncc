#include "TwoDimensionalGrid.h"

#define USE_TRIANGLE_SOFT
//#define USE_TRIANGLE_ELEM

Grid2D::Grid2D(const Vector<2>& corner1__, const Vector<2>& corner2__,
        const Index<2>& nElements__)
    : Grid<2>()
    , is_quadrilateral(true)
    , interiorCounterCall(0)
    , bdryCounterCall(0)
    , corner1_(corner1__)
    , corner2_(corner2__)
    , nElements_(nElements__)
    , fileName("NULL")
    , nodes_()
    , interior_()
    , boundary_()
{
}

Grid2D::Grid2D(std::string fileName__)
    : Grid<2>()
    , is_quadrilateral(false)
    , interiorCounterCall(0)
    , bdryCounterCall(0)
    , corner1_()
    , corner2_()
    , nElements_()
    , fileName(fileName__)
    , nodes_()
    , interior_()
    , boundary_()

{
    readInputFiles();
}

void
Grid2D::readInputFiles() const
{
#ifdef USE_TRIANGLE_SOFT
    std::string nodeFileName(fileName + ".1.node"), line_;
    std::ifstream nodeFile;
    nodeFile.open(nodeFileName);
    if (nodeFile.is_open() && std::getline(nodeFile, line_)) {
        std::size_t nNodes_;
        std::string dummy_;
        std::istringstream iss(line_);
        iss >> nNodes_;
        nodes_.resize(nNodes_);
        for (std::size_t i = 0; i < nNodes_; ++i) {
            line_.clear();
            std::getline(nodeFile, line_);
            std::istringstream iss_(line_);
            iss_ >> dummy_ >> nodes_[i][0] >> nodes_[i][1];
        }
    }
    nodeFile.close();

    interior_.clear();
    line_.clear();
    std::string elemFileName(fileName + ".1.ele");
    std::ifstream elemFile;
    elemFile.open(elemFileName);
    if (elemFile.is_open() && std::getline(elemFile, line_)) {
        std::size_t nElems_, p1, p2, p3;
        std::string dummy_;
        std::istringstream iss(line_);
        iss >> nElems_;
        for (std::size_t i = 0; i < nElems_; ++i) {
            line_.clear();
            std::getline(elemFile, line_);
            std::istringstream iss_(line_);
            iss_ >> dummy_ >> p1 >> p2 >> p3;

            SimplexElement<2> triangleElement(
                    { nodes_[p1], nodes_[p2], nodes_[p3] });
            triangleElement.globalIndices() = { p1, p2, p3 };

            interior_.push_back(triangleElement);
        }
    }
    elemFile.close();

    boundary_.clear();
    line_.clear();
    std::string sidesFileName(fileName + ".1.edge");
    std::ifstream sidesFile;
    sidesFile.open(sidesFileName);
    if (sidesFile.is_open() && std::getline(sidesFile, line_)) {
        std::size_t nSides_, p1, p2;
        int bdryType;
        std::string dummy_;
        std::istringstream iss(line_);
        iss >> nSides_;
        for (std::size_t i = 0; i < nSides_; ++i) {
            line_.clear();
            std::getline(sidesFile, line_);
            std::istringstream iss_(line_);
            iss_ >> dummy_ >> p1 >> p2 >> bdryType;

            if (bdryType > 0) {
                SimplexElementBdry<2> segmentElement(
                        { nodes_[p1], nodes_[p2] });
                segmentElement.globalIndices() = { p1, p2 };

                BoundaryElement bdryElem;
                bdryElem.elem = segmentElement;
                bdryElem.bdryType = bdryType;
                boundary_.push_back(bdryElem);
            }
        }
    }

    std::cerr << "O numero de elementos interiores eh " << interior_.size()
              << std::endl;
    std::cerr << "O numero de elementos de fronteira eh " << boundary_.size()
              << std::endl;
#else
    std::string nodeFileName(fileName + ".n"), line_;
    std::ifstream nodeFile;
    nodeFile.open(nodeFileName);
    if (nodeFile.is_open() && std::getline(nodeFile, line_)) {
        std::size_t nNodes_;
        std::string dummy_;
        std::istringstream iss(line_);
        iss >> nNodes_;
        nodes_.resize(nNodes_);
        for (std::size_t i = 0; i < nNodes_; ++i) {
            line_.clear();
            std::getline(nodeFile, line_);
            std::istringstream iss_(line_);
            iss_ >> dummy_ >> nodes_[i][0] >> nodes_[i][1];
        }
    }
    nodeFile.close();

    interior_.clear();
    line_.clear();
    std::string elemFileName(fileName + ".e");
    std::ifstream elemFile;
    elemFile.open(elemFileName);
    if (elemFile.is_open() && std::getline(elemFile, line_)) {
        std::size_t nElems_, p1, p2, p3;
        std::string dummy_;
        std::istringstream iss(line_);
        iss >> nElems_;
        for (std::size_t i = 0; i < nElems_; ++i) {
            line_.clear();
            std::getline(elemFile, line_);
            std::istringstream iss_(line_);
            iss_ >> dummy_ >> p1 >> p2 >> p3;

            SimplexElement<2> triangleElement(
                    { nodes_[p1], nodes_[p2], nodes_[p3] });
            triangleElement.globalIndices() = { p1, p2, p3 };

            interior_.push_back(triangleElement);
        }
    }
    elemFile.close();

    boundary_.clear();
    line_.clear();
    std::string sidesFileName(fileName + ".s");
    std::ifstream sidesFile;
    sidesFile.open(sidesFileName);
    if (sidesFile.is_open() && std::getline(sidesFile, line_)) {
        std::size_t nSides_, p1, p2;
        int isBdry1, isBdry2, bdryType;
        std::string dummy_;
        std::istringstream iss(line_);
        iss >> nSides_;
        for (std::size_t i = 0; i < nSides_; ++i) {
            line_.clear();
            std::getline(sidesFile, line_);
            std::istringstream iss_(line_);
            iss_ >> dummy_ >> p1 >> p2 >> isBdry1 >> isBdry2 >> bdryType;

            if ((isBdry2 == -1) || (isBdry1 == -1)) {
                std::size_t p1l(isBdry2 == -1 ? p1 : p2),
                        p2l(isBdry2 == -1 ? p2 : p1);
                SimplexElementBdry<2> segmentElement(
                        { nodes_[p1l], nodes_[p2l] });
                segmentElement.globalIndices() = { p1l, p2l };

                BoundaryElement bdryElem;
                bdryElem.elem = segmentElement;
                bdryElem.bdryType = bdryType;
                boundary_.push_back(bdryElem);
            }
        }
    }

    std::cerr << "O numero de elementos interiores eh " << interior_.size()
              << std::endl;
    std::cerr << "O numero de elementos de fronteira eh " << boundary_.size()
              << std::endl;
#endif
}

const std::list<Grid<2>::InteriorElement>&
Grid2D::interior() const
{

    if (is_quadrilateral) {
#ifdef USE_TRIANGLE_ELEM
        interior_.clear();
        double dx((corner2_[0] - corner1_[0]) / nElements_[0]);
        double dy((corner2_[1] - corner1_[1]) / nElements_[1]);

        for (std::size_t i = 0; i < nElements_[1]; i++) {
            double ii = static_cast<double>(i), jj;
            for (std::size_t j = 0; j < nElements_[0]; j++) {
                jj = static_cast<double>(j);

                Vector<2> p0, p1, p2, p3;
                p0[0] = corner1_[0] + jj * dx;
                p0[1] = corner1_[1] + ii * dy;
                p1[0] = p0[0] + dx;
                p1[1] = p0[1];
                p2[0] = p0[0] + dx;
                p2[1] = p0[1] + dy;
                p3[0] = p0[0];
                p3[1] = p0[1] + dy;

                SimplexElement<2> triangleElement2({ p0, p1, p2 });
                triangleElement2.globalIndices()
                        = { i * (nElements_[0] + 1) + j,
                              i * (nElements_[0] + 1) + j + 1,
                              (i + 1) * (nElements_[0] + 1) + j + 1 };

                SimplexElement<2> triangleElement1({ p2, p3, p0 });
                triangleElement1.globalIndices()
                        = { (i + 1) * (nElements_[0] + 1) + j + 1,
                              (i + 1) * (nElements_[0] + 1) + j,
                              i * (nElements_[0] + 1) + j };

                interior_.push_back(triangleElement1);
                interior_.push_back(triangleElement2);
            }
        }
#else
        interior_.clear();
        double dx((corner2_[0] - corner1_[0]) / nElements_[0]);
        double dy((corner2_[1] - corner1_[1]) / nElements_[1]);

        for (std::size_t i = 0; i < nElements_[1]; i++) {
            double ii = static_cast<double>(i), jj;
            for (std::size_t j = 0; j < nElements_[0]; j++) {
                jj = static_cast<double>(j);

                Vector<2> p0, p1, p2, p3;
                p0[0] = corner1_[0] + jj * dx;
                p0[1] = corner1_[1] + ii * dy;
                p1[0] = p0[0] + dx;
                p1[1] = p0[1];
                p2[0] = p0[0] + dx;
                p2[1] = p0[1] + dy;
                p3[0] = p0[0];
                p3[1] = p0[1] + dy;

                QuadElement<2> quadElement({ p0, p1, p2, p3 });
                quadElement.globalIndices() = { i * (nElements_[0] + 1) + j,
                    i * (nElements_[0] + 1) + j + 1,
                    (i + 1) * (nElements_[0] + 1) + j + 1,
                    (i + 1) * (nElements_[0] + 1) + j };

                interior_.push_back(quadElement);
            }
        }
#endif
    }
    else {
        // interiorCounterCall++;
        // if (interiorCounterCall != bdryCounterCall)
        // readInputFiles();
    }

    return interior_;
}

const std::list<Grid<2>::BoundaryElement>&
Grid2D::boundary() const
{
    if (is_quadrilateral) {
        /* Fronteiras
         *
         *          F3
         *    *------------*
         *    |            |
         *    |            |
         * F4 |            | F2
         *    |            |
         *    |            |
         *    *------------*
         *          F1
         */

        boundary_.clear();

        double dx((corner2_[0] - corner1_[0]) / nElements_[0]);
        double dy((corner2_[1] - corner1_[1]) / nElements_[1]);

        /*
         * Fronteira 1
         */
        for (std::size_t j = 0; j < nElements_[0]; j++) {
            double jj = static_cast<double>(j);

            Vector<2> p0, p1;
            p0[0] = corner1_[0] + jj * dx;
            p0[1] = corner1_[1];
            p1[0] = p0[0] + dx;
            p1[1] = p0[1];

            QuadElementBdry<2> quadElement({ p0, p1 });
            quadElement.globalIndices() = { j, j + 1 };
            BoundaryElement bdryElem;
            bdryElem.elem = quadElement;
            bdryElem.bdryType = 0;
            boundary_.push_back(bdryElem);
        }

        /*
         * Fronteira 2
         */
        for (std::size_t i = 0; i < nElements_[1]; i++) {
            double ii = static_cast<double>(i);

            Vector<2> p0, p1;
            p0[0] = corner2_[0];
            p0[1] = corner1_[1] + ii * dy;
            p1[0] = p0[0];
            p1[1] = p0[1] + dy;

            QuadElementBdry<2> quadElement({ p0, p1 });
            quadElement.globalIndices() = { (i + 1) * (nElements_[0] + 1) - 1,
                (i + 2) * (nElements_[0] + 1) - 1 };
            BoundaryElement bdryElem;
            bdryElem.elem = quadElement;
            bdryElem.bdryType = 0;
            boundary_.push_back(bdryElem);
        }

        /*
         * Fronteira 3
         */
        std::size_t lastRowIndex = (nElements_[1] + 1) * nElements_[0];
        for (std::size_t j = 0; j < nElements_[0]; j++) {
            double jj = static_cast<double>(j);

            Vector<2> p0, p1;
            p0[0] = corner1_[0] + jj * dx;
            p0[1] = corner2_[1];
            p1[0] = p0[0] + dx;
            p1[1] = p0[1];

            QuadElementBdry<2> quadElement({ p1, p0 });
            quadElement.globalIndices()
                    = { lastRowIndex + j + 1, lastRowIndex + j };
            BoundaryElement bdryElem;
            bdryElem.elem = quadElement;
            bdryElem.bdryType = 0;
            boundary_.push_back(bdryElem);
        }

        /*
         * Fronteira 4
         */
        for (std::size_t i = 0; i < nElements_[1]; i++) {
            double ii = static_cast<double>(i);

            Vector<2> p0, p1;
            p0[0] = corner1_[0];
            p0[1] = corner1_[1] + ii * dy;
            p1[0] = p0[0];
            p1[1] = p0[1] + dy;

            QuadElementBdry<2> quadElement({ p1, p0 });
            quadElement.globalIndices() = { (i + 1) * (nElements_[0] + 1),
                i * (nElements_[0] + 1) };
            BoundaryElement bdryElem;
            bdryElem.elem = quadElement;
            bdryElem.bdryType = 0;
            boundary_.push_back(bdryElem);
        }
    }
    else {
        // bdryCounterCall++;
        // if (interiorCounterCall != bdryCounterCall)
        // readInputFiles();
    }

    return boundary_;
}

std::size_t
Grid2D::size() const
{
    if (is_quadrilateral)
        return (nElements_[0] + 1) * (nElements_[1] + 1);
    else
        return nodes_.size();
}

void
Grid2D::print() const
{
    if (is_quadrilateral) {
#ifdef USE_TRIANGLE_ELEM
        double dx((corner2_[0] - corner1_[0]) / nElements_[0]);
        double dy((corner2_[1] - corner1_[1]) / nElements_[1]);
        std::cout << "X = [";
        for (std::size_t i = 0; i < nElements_[1]; ++i) {
            for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
                std::cout << corner1_[0] + static_cast<double>(j) * dx
                          << std::endl;
            }
            std::cout << "; ";
        }
        for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
            std::cout << corner1_[0] + static_cast<double>(j) * dx << std::endl;
        }
        std::cout << "];" << std::endl;

        std::cout << "Y = [";
        for (std::size_t i = 0; i < nElements_[1]; ++i) {
            for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
                std::cout << corner1_[1] + static_cast<double>(i) * dy
                          << std::endl;
            }
            std::cout << "; ";
        }
        for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
            std::cout << corner2_[1] << std::endl;
        }
        std::cout << "];" << std::endl;
        std::cout << "ELEM = [";
        for (const auto& e : interior_) {
            std::cout << e.globalIndices()[0] + 1 << " "
                      << e.globalIndices()[1] + 1 << " "
                      << e.globalIndices()[2] + 1 << std::endl;
        }
        std::cout << "];" << std::endl;
#else
        double dx((corner2_[0] - corner1_[0]) / nElements_[0]);
        double dy((corner2_[1] - corner1_[1]) / nElements_[1]);
        std::cout << "X = [";
        for (std::size_t i = 0; i < nElements_[1]; ++i) {
            for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
                std::cout << corner1_[0] + static_cast<double>(j) * dx << " ";
            }
            std::cout << "; ";
        }
        for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
            std::cout << corner1_[0] + static_cast<double>(j) * dx << " ";
        }
        std::cout << "];" << std::endl;

        std::cout << "Y = [";
        for (std::size_t i = 0; i < nElements_[1]; ++i) {
            for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
                std::cout << corner1_[1] + static_cast<double>(i) * dy << " ";
            }
            std::cout << "; ";
        }
        for (std::size_t j = 0; j < nElements_[0] + 1; ++j) {
            std::cout << corner2_[1] << " ";
        }
        std::cout << "];" << std::endl;
#endif
    }
    else {
        std::cout << "X = [";
        for (std::size_t i = 0; i < nodes_.size(); ++i)
            std::cout << nodes_[i][0] << std::endl;
        std::cout << "];" << std::endl;
        std::cout << "Y = [";
        for (std::size_t i = 0; i < nodes_.size(); ++i)
            std::cout << nodes_[i][1] << std::endl;
        std::cout << "];" << std::endl;
        std::cout << "ELEM = [";
        for (const auto& e : interior_) {
            std::cout << e.globalIndices()[0] + 1 << " "
                      << e.globalIndices()[1] + 1 << " "
                      << e.globalIndices()[2] + 1 << std::endl;
        }
        std::cout << "];" << std::endl;
    }
}
