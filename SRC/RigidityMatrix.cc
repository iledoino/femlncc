#include "RigidityMatrix.h"

template <>
template <>
void
RigidityMatrix<1>::adder<std::vector<double>>(std::vector<double>& matrix_,
        const std::size_t& /* nRows */, const std::size_t& nColumns,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_[row * nColumns + column] += entry_;
}

template <>
template <>
void
RigidityMatrix<1>::adder<SprMtr>(SprMtr& matrix_,
        const std::size_t& /* nRows */, const std::size_t& /* nColumns */,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_addelem(matrix_, row + 1, column + 1, entry_);
}

template <>
template <>
void
RigidityMatrix<2>::adder<std::vector<double>>(std::vector<double>& matrix_,
        const std::size_t& /* nRows */, const std::size_t& nColumns,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_[row * nColumns + column] += entry_;
}

template <>
template <>
void
RigidityMatrix<2>::adder<SprMtr>(SprMtr& matrix_,
        const std::size_t& /* nRows */, const std::size_t& /* nColumns */,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_addelem(matrix_, row + 1, column + 1, entry_);
}

template <>
template <>
void
RigidityMatrix<3>::adder<std::vector<double>>(std::vector<double>& matrix_,
        const std::size_t& /* nRows */, const std::size_t& nColumns,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_[row * nColumns + column] += entry_;
}

template <>
template <>
void
RigidityMatrix<3>::adder<SprMtr>(SprMtr& matrix_,
        const std::size_t& /* nRows */, const std::size_t& /* nColumns */,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_addelem(matrix_, row + 1, column + 1, entry_);
}

template <>
template <>
void
RigidityMatrix<4>::adder<std::vector<double>>(std::vector<double>& matrix_,
        const std::size_t& /* nRows */, const std::size_t& nColumns,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_[row * nColumns + column] += entry_;
}

template <>
template <>
void
RigidityMatrix<4>::adder<SprMtr>(SprMtr& matrix_,
        const std::size_t& /* nRows */, const std::size_t& /* nColumns */,
        const std::size_t& row, const std::size_t& column, double entry_)
{
    matrix_addelem(matrix_, row + 1, column + 1, entry_);
}
