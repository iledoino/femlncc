#ifndef QuadLinearInterp_H
#define QuadLinearInterp_H

#include "StdElemInterp.h"

/**
 * Um objeto do tipo QuadLinearInterp é um transformador de elementos
 * quadriláteros gerais para elementos quadriláteros padrão. O elemento
 * padrão (domínio local), é aquele formado por 2^N pontos cujas
 * entradas são a combinação de 1 e -1, enquanto que o elemento geral
 * (domínio global), é formado por 2^N pontos quaisquer (desde que formem
 * um quadrilátero convexo).
 *
 * Domínio local em 2D:
 *
 *             P4 = (-1,  1) *----* P3 = (1,  1)
 *                           |    |
 *                           |    |
 *             P1 = (-1, -1) *----* P2 = (1, -1)
 */
class QuadLinearInterp1D : public StdElemInterp<1>
{
public:
    typedef Vector<1> LocalPoint;
    typedef Vector<1> GlobalPoint;
    typedef Vector<1> Gradient;
    typedef Matrix<1, 1> Jacobian;
    typedef Matrix<1, 1> JacInverse;

    QuadLinearInterp1D() = default;
    ~QuadLinearInterp1D() = default;

    void setUp(const GlobalPoint& P1, const GlobalPoint& P2);
    void setUp(const Element<1>& elem) override;

    const GlobalPoint& P1() const { return P1_; }
    const GlobalPoint& P2() const { return P2_; }

    GlobalPoint globalPoint(const LocalPoint& localPoint) override;
    Jacobian jacobian(const LocalPoint& localPoint) override;
    JacInverse jacInverse(const LocalPoint& localPoint) override;
    double jacDeterminant(const LocalPoint& localPoint) override;

    std::size_t nBaseFunctions() const override;
    double
    baseFunction(std::size_t index, const LocalPoint& localPoint) override;
    Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint) override;
    Jacobian
    baseJacobian(std::size_t index, const LocalPoint& localPoint) override;

private:
    GlobalPoint P1_, P2_;
};

class QuadLinearInterp2D : public StdElemInterp<2>
{
public:
    typedef Vector<2> LocalPoint;
    typedef Vector<2> GlobalPoint;
    typedef Vector<2> Gradient;
    typedef Matrix<2, 2> Jacobian;
    typedef Matrix<2, 2> JacInverse;

    QuadLinearInterp2D() = default;
    ~QuadLinearInterp2D() = default;

    void setUp(const GlobalPoint& P1, const GlobalPoint& P2,
            const GlobalPoint& P3, const GlobalPoint& P4);
    void setUp(const Element<2>& elem) override;

    const GlobalPoint& P1() const { return P1_; }
    const GlobalPoint& P2() const { return P2_; }
    const GlobalPoint& P3() const { return P3_; }
    const GlobalPoint& P4() const { return P4_; }

    GlobalPoint globalPoint(const LocalPoint& localPoint) override;
    Jacobian jacobian(const LocalPoint& localPoint) override;
    JacInverse jacInverse(const LocalPoint& localPoint) override;
    double jacDeterminant(const LocalPoint& localPoint) override;

    std::size_t nBaseFunctions() const override;
    double
    baseFunction(std::size_t index, const LocalPoint& localPoint) override;
    Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint) override;
    Jacobian
    baseJacobian(std::size_t index, const LocalPoint& localPoint) override;

private:
    GlobalPoint P1_, P2_, P3_, P4_;
};

#endif
