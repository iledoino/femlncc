#include "ElemMatrix.h"

/*
 * Integradores em uma dimensão
 */
template <>
double
ElemMatrix<1>::integrate(std::size_t i, std::size_t j, int bdryType) const
{
    double integral(0.);

    if (bdryType != 0) {
        std::size_t nPoints = glRuleBoundary_.nPoints(0);

        for (std::size_t ii = 0; ii < nPoints; ++ii) {
            Index<0> index;
            Vector<0> localPoint;

            integral += glRuleBoundary_.weight(index)
                    * bilinearFormFunction_.evaluate_boundary(i, j, localPoint)
                    * bilinearFormFunction_.stdBdryElemInterp().jacDeterminant(
                            localPoint);
        }
    }
    else {
        std::size_t nPoints = glRuleInterior_.nPoints(0);

        for (std::size_t ii = 0; ii < nPoints; ++ii) {
            Index<1> index({ ii });
            Vector<1> localPoint(glRuleInterior_.point(index));

            integral += glRuleInterior_.weight(index)
                    * bilinearFormFunction_.evaluate_interior(i, j, localPoint)
                    * bilinearFormFunction_.stdElemInterp().jacDeterminant(
                            localPoint);
        }
    }

    return integral;
}

template <>
double
ElemMatrix<1>::integrate(std::size_t i, int bdryType) const
{
    double integral(0.);

    if (bdryType != 0) {
        std::size_t nPoints = glRuleBoundary_.nPoints(0);

        for (std::size_t ii = 0; ii < nPoints; ++ii) {
            Index<0> index;
            Vector<0> localPoint;

            integral += glRuleBoundary_.weight(index)
                    * linearFormFunction_.evaluate_boundary(i, localPoint)
                    * linearFormFunction_.stdBdryElemInterp().jacDeterminant(
                            localPoint);
        }
    }
    else {
        std::size_t nPoints = glRuleInterior_.nPoints(0);

        for (std::size_t ii = 0; ii < nPoints; ++ii) {
            Index<1> index({ ii });
            Vector<1> localPoint(glRuleInterior_.point(index));

            integral += glRuleInterior_.weight(index)
                    * linearFormFunction_.evaluate_interior(i, localPoint)
                    * linearFormFunction_.stdElemInterp().jacDeterminant(
                            localPoint);
        }
    }

    return integral;
}

/*
 * Integradores em duas dimensões
 */
template <>
double
ElemMatrix<2>::integrate(std::size_t i, std::size_t j, int bdryType) const
{
    double integral(0.);

    if (bdryType != 0) {
        std::size_t nPoints = glRuleBoundary_.nPoints(0);

        for (std::size_t ii = 0; ii < nPoints; ++ii) {
            Index<1> index({ ii });
            Vector<1> localPoint(glRuleBoundary_.point(index));

            integral += glRuleBoundary_.weight(index)
                    * bilinearFormFunction_.evaluate_boundary(i, j, localPoint)
                    * bilinearFormFunction_.stdBdryElemInterp().jacDeterminant(
                            localPoint);
        }
    }
    else {
        std::size_t nPointsX = glRuleInterior_.nPoints(0);
        std::size_t nPointsY = glRuleInterior_.nPoints(1);

        for (std::size_t ii = 0; ii < nPointsX; ++ii)
            for (std::size_t jj = 0; jj < nPointsY; ++jj) {
                Index<2> index({ ii, jj });
                Vector<2> localPoint(glRuleInterior_.point(index));

                integral += glRuleInterior_.weight(index)
                        * bilinearFormFunction_.evaluate_interior(
                                i, j, localPoint)
                        * bilinearFormFunction_.stdElemInterp().jacDeterminant(
                                localPoint);
            }
    }

    return integral;
}

template <>
double
ElemMatrix<2>::integrate(std::size_t i, int bdryType) const
{
    double integral(0.);

    if (bdryType != 0) {
        std::size_t nPoints = glRuleBoundary_.nPoints(0);

        for (std::size_t ii = 0; ii < nPoints; ++ii) {
            Index<1> index({ ii });
            Vector<1> localPoint(glRuleBoundary_.point(index));

            integral += glRuleBoundary_.weight(index)
                    * linearFormFunction_.evaluate_boundary(i, localPoint)
                    * linearFormFunction_.stdBdryElemInterp().jacDeterminant(
                            localPoint);
        }
    }
    else {
        std::size_t nPointsX = glRuleInterior_.nPoints(0);
        std::size_t nPointsY = glRuleInterior_.nPoints(1);

        for (std::size_t ii = 0; ii < nPointsX; ++ii)
            for (std::size_t jj = 0; jj < nPointsY; ++jj) {
                Index<2> index({ ii, jj });
                Vector<2> localPoint(glRuleInterior_.point(index));

                integral += glRuleInterior_.weight(index)
                        * linearFormFunction_.evaluate_interior(
                                i, glRuleInterior_.point(index))
                        * linearFormFunction_.stdElemInterp().jacDeterminant(
                                localPoint);
            }
    }

    return integral;
}
