#ifndef TriangLinearInterp_H
#define TriangLinearInterp_H

#include "StdElemInterp.h"

/**
 * Um objeto do tipo TriangLinearInterp é um transformador de elementos
 * triangulares gerais para elementos triangulares padrão. O elemento
 * padrão (domínio local), é aquele formado por três pontos cujas
 * entradas são a (0, 0), (0, 1) e (1, 0), enquanto que o elemento geral
 * (domínio global), é formado pelos pontos P1, P2, P3.
 *
 * Domínio local em 2D:
 *
 *             P3 = (0, 1) *\
 *                         | \
 *                         |  \
 *             P1 = (0, 0) *---* P2 = (1, 0)
 */
class TriangLinearInterp : public StdElemInterp<2>
{
public:
    typedef Vector<2> LocalPoint;
    typedef Vector<2> GlobalPoint;
    typedef Vector<2> Gradient;
    typedef Matrix<2, 2> Jacobian;
    typedef Matrix<2, 2> JacInverse;

    TriangLinearInterp() = default;
    ~TriangLinearInterp() = default;

    void
    setUp(const LocalPoint& P1, const LocalPoint& P2, const LocalPoint& P3);
    void setUp(const Element<2>& elem) override;

    const GlobalPoint& P1() const { return P1_; }
    const GlobalPoint& P2() const { return P2_; }
    const GlobalPoint& P3() const { return P3_; }

    GlobalPoint globalPoint(const LocalPoint& localPoint);
    Jacobian jacobian(const LocalPoint& localPoint);
    JacInverse jacInverse(const LocalPoint& localPoint);
    double jacDeterminant(const LocalPoint& localPoint);

    std::size_t nBaseFunctions() const override;
    double
    baseFunction(std::size_t index, const LocalPoint& localPoint) override;
    Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint) override;
    Jacobian
    baseJacobian(std::size_t index, const LocalPoint& localPoint) override;

private:
    GlobalPoint P1_, P2_, P3_;
    double x2mx1, x3mx1, y2my1, y3my1;
    double area, oneOverArea;
    double j11, j12, j21, j22;
    double b1, b2;
};

#endif
