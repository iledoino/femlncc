#ifndef LinearAlgebra_H
#define LinearAlgebra_H

#include <array>
#include <cmath>
#include <iostream>

/*
 * Definições de vetores e matrizes
 */
template <std::size_t N>
using Vector = std::array<double, N>;

template <std::size_t N>
using Index = std::array<std::size_t, N>;

template <std::size_t M, std::size_t N>
using Matrix = std::array<Vector<N>, M>;

/*
 * Definições de funções auxiliares
 */
template <std::size_t N>
Matrix<N, N> inverse(Matrix<N, N> matrix);

template <std::size_t M, std::size_t N>
Matrix<N, M>
transpose(Matrix<M, N> matrix)
{
    Matrix<N, M> matrixT;
    for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < M; ++j)
            matrixT[j][i] = matrix[i][j];
    return matrixT;
}

template <std::size_t N>
double determinant(Matrix<N, N> matrix);

template <std::size_t N>
double
norm(const Vector<N>& vector)
{
    double norm_(0.);

    for (std::size_t i = 0; i < N; ++i)
        norm_ += vector[i] * vector[i];

    return std::sqrt(norm_);
}

template <std::size_t M, std::size_t N>
void
assign(Matrix<M, N>& matrix, double val)
{
    for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < M; ++j)
            matrix[i][j] = val;
}

template <std::size_t N>
Vector<N>
mult(double val, const Vector<N>& vector)
{
    Vector<N> result;

    for (std::size_t i = 0; i < N; ++i)
        result[i] = vector[i] * val;

    return result;
}

template <std::size_t N>
double
mult(const Vector<N>& vectorT, const Vector<N>& vector)
{
    double result(0.);

    for (std::size_t i = 0; i < N; ++i)
        result += vectorT[i] * vector[i];

    return result;
}

template <std::size_t M, std::size_t N>
Vector<N>
mult(const Vector<M>& vector, const Matrix<M, N>& matrix)
{
    Vector<N> result;

    for (std::size_t i = 0; i < N; ++i)
        result[i] = vector[0] * matrix[0][i];

    for (std::size_t j = 1; j < M; ++j)
        for (std::size_t i = 0; i < N; ++i)
            result[i] += vector[j] * matrix[j][i];

    return result;
}

template <std::size_t M, std::size_t N>
Vector<M>
mult(const Matrix<M, N>& matrix, const Vector<N>& vector)
{
    Vector<M> result;

    for (std::size_t i = 0; i < M; ++i) {
        result[i] = matrix[i][0] * vector[0];
        for (std::size_t j = 1; j < N; ++j)
            result[i] += matrix[i][j] * vector[j];
    }

    return result;
}

template <std::size_t N>
Vector<N>
segmentPoint(const Vector<N>& direction, double t, const Vector<N>& point)
{
    Vector<N> result;

    for (std::size_t i = 0; i < N; ++i)
        result[i] = direction[i] * t + point[i];

    return result;
}

/*
 * Definições de impressão de vetores e matrizes
 */
template <std::size_t N>
std::ostream&
operator<<(std::ostream& os, const Vector<N>& p)
{
    os << "(" << p[0];
    for (std::size_t i = 1; i < N; ++i)
        os << ", " << p[i];
    os << ")";
    return os;
}

template <std::size_t M, std::size_t N>
std::ostream&
operator<<(std::ostream& os, const Matrix<M, N>& p)
{
    for (std::size_t j = 0; j < M; ++j) {
        os << "|" << p[j][0];
        for (std::size_t i = 1; i < N; ++i)
            os << ", " << p[j][i];
        os << "|\n";
    }
    return os;
}

#endif
