#ifndef LinearFormFunction_H
#define LinearFormFunction_H

#include "StdBdryElemInterp.h"
#include "StdElemInterp.h"

/**
 * Um objeto do tipo LinearFormFunction<N> fornece a função a ser
 * integrada para a obtenção da forma linear (para elementos interiores)
 */
template <std::size_t N>
class LinearFormFunction
{
public:
    typedef Vector<N> LocalPointN;
    typedef Vector<N - 1> LocalPointNm1;

    LinearFormFunction(StdElemInterp<N>& stdElemInterp,
            StdBdryElemInterp<N>& stdBdryElemInterp);

    StdElemInterp<N>& stdElemInterp() { return stdElemInterp_; }
    StdBdryElemInterp<N>& stdBdryElemInterp() { return stdBdryElemInterp_; }

    virtual double
    evaluate_interior(std::size_t i, const LocalPointN& localPoint)
            = 0;
    virtual double
    evaluate_boundary(std::size_t i, const LocalPointNm1& localPoint)
    {
        return 0.;
    }

private:
    StdElemInterp<N>& stdElemInterp_;
    StdBdryElemInterp<N>& stdBdryElemInterp_;
};

template <std::size_t N>
LinearFormFunction<N>::LinearFormFunction(StdElemInterp<N>& stdElemInterp__,
        StdBdryElemInterp<N>& stdBdryElemInterp__)
    : stdElemInterp_(stdElemInterp__), stdBdryElemInterp_(stdBdryElemInterp__)
{
}

#endif
