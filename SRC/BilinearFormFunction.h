#ifndef BilinearFormFunction_H
#define BilinearFormFunction_H

#include "StdBdryElemInterp.h"
#include "StdElemInterp.h"

/**
 * Um objeto do tipo BilinearFormFunction<N> fornece a função a ser
 * integrada para a obtenção da forma bilinear
 */
template <std::size_t N>
class BilinearFormFunction
{
public:
    typedef Vector<N> LocalPointN;
    typedef Vector<N - 1> LocalPointNm1;

    BilinearFormFunction(StdElemInterp<N>& stdElemInterp,
            StdBdryElemInterp<N>& stdBdryElemInterp);

    StdElemInterp<N>& stdElemInterp() { return stdElemInterp_; }
    StdBdryElemInterp<N>& stdBdryElemInterp() { return stdBdryElemInterp_; }

    virtual double evaluate_interior(
            std::size_t i, std::size_t j, const LocalPointN& localPoint)
            = 0;
    virtual double evaluate_boundary(
            std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
    {
        return 0;
    }

    virtual bool is_symmetric() const = 0;

private:
    StdElemInterp<N>& stdElemInterp_;
    StdBdryElemInterp<N>& stdBdryElemInterp_;
};

template <std::size_t N>
BilinearFormFunction<N>::BilinearFormFunction(StdElemInterp<N>& stdElemInterp__,
        StdBdryElemInterp<N>& stdBdryElemInterp__)
    : stdElemInterp_(stdElemInterp__), stdBdryElemInterp_(stdBdryElemInterp__)
{
}

#endif
