#ifndef GLRule_H
#define GLRule_H

#include "LinearAlgebra.h"
#include <cassert>

/**
 * Um objeto do tipo GLRule<N> fornece pontos e pesos necessários
 * para se realizar uma integração numérica em N-D usando o método de
 * Gauss-Legendre
 */
template <std::size_t N>
class GLRule
{
public:
    typedef Vector<N> Point;

    GLRule() = default;
    ~GLRule() = default;

    virtual std::size_t nPoints(std::size_t index) const = 0;

    virtual Point point(Index<N> index) const = 0;
    virtual double weight(Index<N> index) const = 0;
};

/**
 * Um objeto do tipo GLRule0D é apenas uma generalização que possibilita
 * uso da classe GLRule em rotinas de integração
 */
class GLRule0D : public GLRule<0>
{
public:
    typedef GLRule<0>::Point Point;

    GLRule0D() = default;
    ~GLRule0D() = default;

    std::size_t nPoints(std::size_t index) const override;
    Point point(Index<0> index) const override;
    double weight(Index<0> index) const override;
};

/**
 * Um objeto do tipo GLRule1D fornece pontos e pesos necessários
 * para se realizar uma integração numérica em 1D usando o método de
 * Gauss-Legendre
 */
class GLRule1D : public GLRule<1>
{
public:
    typedef GLRule<1>::Point Point;

    GLRule1D() = default;
    ~GLRule1D() = default;

    virtual double point(std::size_t index) const = 0;
    virtual double weight(std::size_t index) const = 0;

    Point point(Index<1> index) const override;
    double weight(Index<1> index) const override;
};

/**
 * Um objeto do tipo GLRule2D fornece pontos e pesos necessários
 * para se realizar uma integração numérica em 2D usando o método de
 * Gauss-Legendre
 */
class GLRule2D : public GLRule<2>
{
public:
    typedef GLRule<2>::Point Point;

    GLRule2D() = default;
    ~GLRule2D() = default;

    virtual Point point(std::size_t index1, std::size_t index2) const = 0;
    virtual double weight(std::size_t index1, std::size_t index2) const = 0;

    Point point(Index<2> index) const override;
    double weight(Index<2> index) const override;
};

#endif
