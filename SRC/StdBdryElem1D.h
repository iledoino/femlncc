#ifndef StdBdryElem1D_H
#define StdBdryElem1D_H

#include "StdBdryElemInterp.h"

/**
 * Um objeto do tipo StdBdryElem1D implementa funcões necessárias para
 * elemento de fronteira em 1D (um ponto)
 */
class StdBdryElem1D : public StdBdryElemInterp<1>
{
public:
    typedef Vector<0> LocalPoint;
    typedef Vector<1> GlobalPoint;
    typedef Vector<1> NormalPoint;
    typedef Vector<1> Gradient;
    typedef Matrix<0, 0> Jacobian;

    StdBdryElem1D() = default;
    ~StdBdryElem1D() = default;

    void setUp(double P, const int& bdryType);
    void setUp(const GlobalPoint& P, const int& bdryType);
    void setUp(const Element<1>& elem, const int& bdryType) override;

    /*
     * Funções relacionadas à transformação
     */
    GlobalPoint globalPoint(const LocalPoint& localPoint) override
    {
        return P_;
    }
    NormalPoint normalPoint(const LocalPoint& localPoint) override
    {
        return NormalPoint({ 1. });
    }
    Jacobian jacobian(const LocalPoint& localPoint) override
    {
        return Jacobian();
    }
    double jacDeterminant(const LocalPoint& localPoint) override { return 1.; }

    /*
     * Funções relacionadas às funções base
     */
    std::size_t nBaseFunctions() const override { return 1; }
    double
    baseFunction(std::size_t index, const LocalPoint& localPoint) override
    {
        return P_[0];
    }
    Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint) override
    {
        return Gradient({ 1. });
    }

private:
    GlobalPoint P_;
};

#endif
