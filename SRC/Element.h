#ifndef Element_H
#define Element_H

#include "LinearAlgebra.h"
#include <cassert>
#include <cmath>
#include <initializer_list>
#include <vector>

/**
 * Um objeto do tipo Element<N> é um elemento finito na dimensão N
 */
template <std::size_t N>
class Element
{
public:
    typedef Vector<N> Point;
    typedef std::vector<Point> Points;
    typedef std::vector<std::size_t> Indices;

    Element() = default;
    ~Element() = default;

    const Points& points() const { return points_; }

    Indices& globalIndices() { return indices_; }
    const Indices& globalIndices() const { return indices_; }

protected:
    Points& points() { return points_; }

private:
    Points points_;
    Indices indices_;
};

/**
 * Um objeto do tipo QuadElement<N> é um elemento finito quadrilátero
 * na dimensão N
 */
template <std::size_t N>
class QuadElement : public Element<N>
{
public:
    typedef Vector<N> Point;

    QuadElement(std::initializer_list<Point> points);
    ~QuadElement() = default;
};

template <std::size_t N>
QuadElement<N>::QuadElement(std::initializer_list<Point> points__)
    : Element<N>()
{
    assert(points__.size() == std::pow(2, N));
    Element<N>::points() = points__;
}

/**
 * Um objeto do tipo QuadElementBdry<N> é um elemento de fronteira de um
 * quadrilátero finito na dimensão N
 */
template <std::size_t N>
class QuadElementBdry : public Element<N>
{
public:
    typedef Vector<N> Point;

    QuadElementBdry(std::initializer_list<Point> points);
    ~QuadElementBdry() = default;
};

template <std::size_t N>
QuadElementBdry<N>::QuadElementBdry(std::initializer_list<Point> points__)
    : Element<N>()
{
    assert(points__.size() == std::pow(2, N - 1));
    Element<N>::points() = points__;
}

/**
 * Um objeto do tipo SimplexElement<N> é um elemento simplex finito
 * na dimensão N
 *
 * PS: Um 2-simplex é um triângulo, enquanto que um 3-simplex é um
 * tetraedro (https://en.wikipedia.org/wiki/Simplex)
 */
template <std::size_t N>
class SimplexElement : public Element<N>
{
public:
    typedef Vector<N> Point;

    SimplexElement(std::initializer_list<Point> points);
    ~SimplexElement() = default;
};

template <std::size_t N>
SimplexElement<N>::SimplexElement(std::initializer_list<Point> points__)
    : Element<N>()
{
    assert(points__.size() == (N + 1));
    Element<N>::points() = points__;
}

/**
 * Um objeto do tipo SimplexElementBdry<N> é um elemento de fronteira de um
 * elemento finito do tipo simplex na dimensão N
 */
template <std::size_t N>
class SimplexElementBdry : public Element<N>
{
public:
    typedef Vector<N> Point;

    SimplexElementBdry(std::initializer_list<Point> points);
    ~SimplexElementBdry() = default;
};

template <std::size_t N>
SimplexElementBdry<N>::SimplexElementBdry(std::initializer_list<Point> points__)
    : Element<N>()
{
    assert(points__.size() == N);
    Element<N>::points() = points__;
}

#endif
