#ifndef TwoDimensionalGrid_H
#define TwoDimensionalGrid_H

#include "Grid.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

/*
 * Definição da grade de elementos em 2D
 */
class Grid2D : public Grid<2>
{
public:
    typedef Grid<2>::InteriorElement InteriorElement;
    typedef Grid<2>::BoundaryElement BoundaryElement;
    typedef Grid<2>::InteriorElements InteriorElements;
    typedef Grid<2>::BoundaryElements BoundaryElements;

    Grid2D(const Vector<2>& corner1, const Vector<2>& corner2,
            const Index<2>& nElements);
    Grid2D(std::string fileName);

    const InteriorElements& interior() const override;
    const BoundaryElements& boundary() const override;
    std::size_t size() const override;

    void print() const;

private:
    void readInputFiles() const;

    const bool is_quadrilateral;
    mutable int interiorCounterCall, bdryCounterCall;

    /*
     * Estrutura para quadrilatero
     */
    Vector<2> corner1_, corner2_;
    Index<2> nElements_;

    /*
     * Estrutura para triangulo (leitura de easymesh)
     */
    std::string fileName;
    mutable std::vector<Vector<2>> nodes_;

    /*
     * Estrutura comum a ambos os casos
     */
    mutable std::list<InteriorElement> interior_;
    mutable std::list<BoundaryElement> boundary_;
};

#endif
