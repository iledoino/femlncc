#ifndef Grid_H
#define Grid_H

#include "Element.h"
#include <list>

/**
 * Um objeto do tipo Grid<N> é uma grade de Element<N> (elementos em N-D)
 */
template <std::size_t N>
class Grid
{
public:
    typedef Element<N> InteriorElement;
    class BoundaryElement
    {
    public:
        BoundaryElement() = default;
        ~BoundaryElement() = default;

        Element<N> elem;
        int bdryType;
    };
    typedef std::list<InteriorElement> InteriorElements;
    typedef const std::list<BoundaryElement> BoundaryElements;

    Grid() = default;
    ~Grid() = default;

    virtual const InteriorElements& interior() const = 0;
    virtual const BoundaryElements& boundary() const = 0;
    virtual std::size_t size() const = 0;
};

#endif
