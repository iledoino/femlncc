#include "LinearAlgebra.h"

template <>
double
determinant(Matrix<2, 2> matrix)
{
    return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
}

template <>
Matrix<2, 2>
inverse(Matrix<2, 2> matrix)
{
    double det = determinant(matrix), oneOverDet = 1. / det;

#ifdef SECURE_COMPUTATION
    double delta = 1.e-14;
    double absDet = std::abs(det);
    assert(absDet > delta);
#endif

    Matrix<2, 2> inv;
    inv[0][0] = oneOverDet * matrix[1][1];
    inv[0][1] = -oneOverDet * matrix[0][1];
    inv[1][0] = -oneOverDet * matrix[1][0];
    inv[1][1] = oneOverDet * matrix[0][0];

    return inv;
}
