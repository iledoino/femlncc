#include "GLRule.h"

std::size_t
GLRule0D::nPoints(std::size_t index) const
{
    return 1;
}

typename GLRule0D::Point GLRule0D::point(Index<0> /* index */) const
{
    return Point();
}

double GLRule0D::weight(Index<0> /* index */) const
{
    return 1.;
}

typename GLRule1D::Point
GLRule1D::point(Index<1> index) const
{
    return Point({ point(index[0]) });
}

double
GLRule1D::weight(Index<1> index) const
{
    return weight(index[0]);
}

typename GLRule2D::Point
GLRule2D::point(Index<2> index) const
{
    return point(index[0], index[1]);
}

double
GLRule2D::weight(Index<2> index) const
{
    return weight(index[0], index[1]);
}
