#ifndef StdBdryElemInterp_H
#define StdBdryElemInterp_H

#include "Element.h"
#include "LinearAlgebra.h"
#include <cmath>

/**
 * Um objeto do tipo StdBdryElemInterp é um transformador de elementos
 * gerais para elementos padrão, e também um interpolador de funções
 * base para o elemento padrão. Tal elemento é nesse caso um elemento da
 * fronteira de um outro elemento
 */
template <std::size_t N>
class StdBdryElemInterp
{
public:
    typedef Vector<N - 1> LocalPoint;
    typedef Vector<N> GlobalPoint;
    typedef Vector<N> NormalPoint;
    typedef Vector<N> Gradient;
    typedef Matrix<N - 1, N - 1> Jacobian;

    StdBdryElemInterp() = default;
    ~StdBdryElemInterp() = default;

    virtual void setUp(const Element<N>& elem, const int& bdryType) = 0;

    /*
     * Funções relacionadas à transformação
     */
    virtual GlobalPoint globalPoint(const LocalPoint& localPoint) = 0;
    virtual NormalPoint normalPoint(const LocalPoint& localPoint) = 0;
    virtual Jacobian jacobian(const LocalPoint& localPoint) = 0;
    virtual double jacDeterminant(const LocalPoint& localPoint) = 0;

    /*
     * Funções relacionadas às funções base
     */
    virtual std::size_t nBaseFunctions() const = 0;
    virtual double baseFunction(std::size_t index, const LocalPoint& localPoint)
            = 0;
    virtual Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint)
            = 0;
    const int& bdry_type() const { return bdryType_; }

protected:
    int& bdryType() { return bdryType_; }

private:
    int bdryType_;
};

#endif
