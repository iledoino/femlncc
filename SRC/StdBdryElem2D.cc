#include "StdBdryElem2D.h"
#include <cassert>
#include <cmath>

void
StdBdryElem2D::setUp(
        const GlobalPoint& P1__, const GlobalPoint& P2__, const int& bdryType__)
{
    P1_ = P1__;
    P2_ = P2__;
    bdryType() = bdryType__;

    V_[0] = P2_[0] - P1_[0];
    V_[1] = P2_[1] - P1_[1];

    Vnorm = norm(V_);
#ifdef SECURE_COMPUTATION
    double tolerance = 1.0e-14;
    assert(Vnorm > tolerance)
#endif

            N_[0]
            = V_[1] / Vnorm;
    N_[1] = -V_[0] / Vnorm;
}

void
StdBdryElem2D::setUp(const Element<2>& elem__, const int& bdryType__)
{
#ifdef SECURE_COMPUTATION
    assert(elem__.points().size() == 2)
#endif
            setUp(elem__.points()[0], elem__.points()[1], bdryType__);
}

std::size_t
StdBdryElem2D::nBaseFunctions() const
{
    return 2;
}

double
StdBdryElem2D::baseFunction(std::size_t index, const LocalPoint& localPoint)
{
    double xi_ = localPoint[0], sign = (index == 1) ? 1. : -1.;
#ifdef SECURE_COMPUTATION
    assert(xi_ >= -1 && xi_ <= 1.);
    assert(index < 2);
#endif

    return 0.5 * (1. + sign * xi_);
}

typename StdBdryElem2D::Gradient
StdBdryElem2D::baseGradient(std::size_t index, const LocalPoint& localPoint)
{
    double xi_ = localPoint[0], sign = (index == 1) ? 1. : -1.;
#ifdef SECURE_COMPUTATION
    assert(xi_ >= -1 && xi_ <= 1.);
    assert(index < 2);
#endif
    Gradient gradTransf(mult(0.5 * xi_, V_));

    return mult(0.5 * sign / norm(gradTransf), gradTransf);
}

typename StdBdryElem2D::GlobalPoint
StdBdryElem2D::globalPoint(const LocalPoint& localPoint)
{
    double xi_ = localPoint[0];
#ifdef SECURE_COMPUTATION
    assert(xi_ >= -1 && xi_ <= 1.);
#endif
    return segmentPoint(V_, 0.5 * (1. + xi_), P1_);
}

typename StdBdryElem2D::NormalPoint
StdBdryElem2D::normalPoint(const LocalPoint& localPoint)
{
    return N_;
}

typename StdBdryElem2D::Jacobian
StdBdryElem2D::jacobian(const LocalPoint& localPoint)
{
    Jacobian j;
    j[0][0] = Vnorm * 0.5;
    return j;
}

double
StdBdryElem2D::jacDeterminant(const LocalPoint& localPoint)
{
    return Vnorm * 0.5;
}
