#ifndef StdElemInterp_H
#define StdElemInterp_H

#include "Element.h"
#include "LinearAlgebra.h"
#include <cmath>

/**
 * Um objeto do tipo StdElemInterp é um transformador de elementos
 * gerais para elementos padrão, e também um interpolador de funções
 * base para o elemento padrão
 */
template <std::size_t N>
class StdElemInterp
{
public:
    typedef Vector<N> LocalPoint;
    typedef Vector<N> GlobalPoint;
    typedef Vector<N> Gradient;
    typedef Matrix<N, N> Jacobian;
    typedef Matrix<N, N> JacInverse;

    StdElemInterp() = default;
    ~StdElemInterp() = default;

    virtual void setUp(const Element<N>& elem) = 0;

    /*
     * Funções relacionadas à transformação
     */
    virtual GlobalPoint globalPoint(const LocalPoint& localPoint) = 0;
    virtual Jacobian jacobian(const LocalPoint& localPoint) = 0;
    virtual JacInverse jacInverse(const LocalPoint& localPoint) = 0;
    virtual double jacDeterminant(const LocalPoint& localPoint) = 0;

    /*
     * Funções relacionadas às funções base
     */
    virtual std::size_t nBaseFunctions() const = 0;
    virtual double baseFunction(std::size_t index, const LocalPoint& localPoint)
            = 0;
    virtual Gradient
    baseGradient(std::size_t index, const LocalPoint& localPoint)
            = 0;
    virtual Jacobian
    baseJacobian(std::size_t index, const LocalPoint& localPoint)
            = 0;
};

#endif
