#include "QuadLinearInterp.h"
#include <cassert>

/**
 * Interpolação em 1d
 */
void
QuadLinearInterp1D::setUp(const Element<1>& elem__)
{
#ifdef SECURE_COMPUTATION
    assert(elem__.points().size() == 2)
#endif
            P1_
            = elem__.points()[0];
    P2_ = elem__.points()[1];
}

void
QuadLinearInterp1D::setUp(const GlobalPoint& P1__, const GlobalPoint& P2__)
{
    P1_ = P1__;
    P2_ = P2__;
}

std::size_t
QuadLinearInterp1D::nBaseFunctions() const
{
    return 2;
}

double
QuadLinearInterp1D::baseFunction(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 2)
#endif
            double xi
            = localPoint_[0],
            oneHalved = 0.5;
    double sign(index_ == 1 ? 1. : -1.);

    return (1. + sign * xi) * oneHalved;
}

typename QuadLinearInterp1D::Gradient
QuadLinearInterp1D::baseGradient(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 2)
#endif
            double oneHalved
            = 0.5;
    double sign(index_ == 1 ? 1. : -1.);
    Gradient g;

    g[0] = sign * oneHalved / jacobian(localPoint_)[0][0];
    return g;
}

typename QuadLinearInterp1D::Jacobian
QuadLinearInterp1D::baseJacobian(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 2)
#endif
            Jacobian j;

    j[0][0] = 0.;
    return j;
}

typename QuadLinearInterp1D::GlobalPoint
QuadLinearInterp1D::globalPoint(const LocalPoint& localPoint_)
{
    double xi = localPoint_[0], oneHalved = 0.5;
    double oneMxi = 1. - xi, onePxi = 1. + xi;

#ifdef SECURE_COMPUTATION
    assert(xi >= -1. && xi <= 1.);
#endif

    return { oneHalved * (P1()[0] * oneMxi + P2()[0] * onePxi) };
}

typename QuadLinearInterp1D::Jacobian
QuadLinearInterp1D::jacobian(const LocalPoint& /* localPoint_ */)
{
    double oneHalved = 0.5;

    Jacobian J_;
    J_[0][0] = oneHalved * (-P1()[0] + P2()[0]);

    return J_;
}

double
QuadLinearInterp1D::jacDeterminant(const LocalPoint& localPoint_)
{
    Jacobian J_(jacobian(localPoint_));
    return J_[0][0];
}

typename QuadLinearInterp1D::JacInverse
QuadLinearInterp1D::jacInverse(const LocalPoint& localPoint_)
{
    Jacobian J_(jacobian(localPoint_));
    JacInverse JInv_;
    JInv_[0][0] = 1. / J_[0][0];
    return JInv_;
}

/**
 * Interpolação em 2d
 */
void
QuadLinearInterp2D::setUp(const Element<2>& elem__)
{
#ifdef SECURE_COMPUTATION
    assert(elem__.points().size() == 4)
#endif
            P1_
            = elem__.points()[0];
    P2_ = elem__.points()[1];
    P3_ = elem__.points()[2];
    P4_ = elem__.points()[3];
}

void
QuadLinearInterp2D::setUp(const GlobalPoint& P1__, const GlobalPoint& P2__,
        const GlobalPoint& P3__, const GlobalPoint& P4__)
{
    P1_ = P1__;
    P2_ = P2__;
    P3_ = P3__;
    P4_ = P4__;
}

std::size_t
QuadLinearInterp2D::nBaseFunctions() const
{
    return 4;
}

double
QuadLinearInterp2D::baseFunction(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 4)
#endif
            double xi
            = localPoint_[0],
            eta = localPoint_[1], oneFourth = 0.25;

    if (index_ < 2) {
        double sign(index_ == 1 ? 1. : -1.);
        return (1. + sign * xi) * (1. - eta) * oneFourth;
    }
    else {
        double sign(index_ == 2 ? 1. : -1.);
        return (1. + sign * xi) * (1. + eta) * oneFourth;
    }
}

typename QuadLinearInterp2D::Gradient
QuadLinearInterp2D::baseGradient(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 4)
#endif
            double xi
            = localPoint_[0],
            eta = localPoint_[1], oneFourth = 0.25;
    Gradient g;

    if (index_ < 2) {
        double sign(index_ == 1 ? 1. : -1.);
        g[0] = sign * (1. - eta) * oneFourth;
        g[1] = -(1. + sign * xi) * oneFourth;
    }
    else {
        double sign(index_ == 2 ? 1. : -1.);
        g[0] = sign * (1. + eta) * oneFourth;
        g[1] = (1. + sign * xi) * oneFourth;
    }
    return mult(jacInverse(localPoint_), g);
}

/*
 * TO DO: Consertar o retorno dessa função (provavelmente entrará a
 * Hessiana da transformação)
 */
typename QuadLinearInterp2D::Jacobian
QuadLinearInterp2D::baseJacobian(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 4)
#endif
            double oneFourth
            = 0.25;
    Jacobian j;

    if (index_ < 2) {
        double sign(index_ == 1 ? 1. : -1.);
        j[0][0] = 0.;
        j[0][1] = -sign * oneFourth;
        j[1][0] = -sign * oneFourth;
        j[1][1] = 0.;
    }
    else {
        double sign(index_ == 2 ? 1. : -1.);
        j[0][0] = 0.;
        j[0][1] = -sign * oneFourth;
        j[1][0] = -sign * oneFourth;
        j[1][1] = 0.;
    }
    return j;
}

typename QuadLinearInterp2D::GlobalPoint
QuadLinearInterp2D::globalPoint(const LocalPoint& localPoint_)
{
    double xi = localPoint_[0], eta = localPoint_[1], oneFourth = 0.25;
    double oneMxi = 1. - xi, onePxi = 1. + xi;
    double oneMeta = 1. - eta, onePeta = 1. + eta;
    double MM = oneMxi * oneMeta, PM = onePxi * oneMeta;
    double PP = onePxi * onePeta, MP = oneMxi * onePeta;

#ifdef SECURE_COMPUTATION
    assert(xi >= -1. && xi <= 1.);
    assert(eta >= -1. && eta <= 1.);
#endif

    return {
        oneFourth * (P1()[0] * MM + P2()[0] * PM + P3()[0] * PP + P4()[0] * MP),
        oneFourth * (P1()[1] * MM + P2()[1] * PM + P3()[1] * PP + P4()[1] * MP)
    };
}

typename QuadLinearInterp2D::Jacobian
QuadLinearInterp2D::jacobian(const LocalPoint& localPoint_)
{
    double xi = localPoint_[0], eta = localPoint_[1], oneFourth = 0.25;
    double oneMxi = 1. - xi, onePxi = 1. + xi;
    double oneMeta = 1. - eta, onePeta = 1. + eta;

#ifdef SECURE_COMPUTATION
    assert(xi >= -1. && xi <= 1.);
    assert(eta >= -1. && eta <= 1.);
#endif

    Jacobian J_;
    for (std::size_t i = 0; i < 2; ++i) {
        J_[i][0] = oneFourth
                * (-P1()[i] * oneMeta + P2()[i] * oneMeta + P3()[i] * onePeta
                        - P4()[i] * onePeta);
        J_[i][1] = oneFourth
                * (-P1()[i] * oneMxi - P2()[i] * onePxi + P3()[i] * onePxi
                        + P4()[i] * oneMxi);
    }

    return J_;
}

double
QuadLinearInterp2D::jacDeterminant(const LocalPoint& localPoint_)
{
    Jacobian J_(jacobian(localPoint_));
    return determinant(J_);
}

typename QuadLinearInterp2D::JacInverse
QuadLinearInterp2D::jacInverse(const LocalPoint& localPoint_)
{
    Jacobian J_(jacobian(localPoint_));
    return inverse(J_);
}
