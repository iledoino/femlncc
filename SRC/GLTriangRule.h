#ifndef GLTriangRule_H
#define GLTriangRule_H

#include "GLQuadRule.h"
#include "GLRule.h"

/**
 * Um objeto do tipo GLTriangRule fornece pontos e pesos necessários
 * para se realizar uma integração numérica em 2D usando o método de
 * Gauss-Legendre, em um domínio do tipo triangular
 * PS: Tranforma-se o triângulo padrão em um quadrilátero padrão, e
 * pesos e pontos são calculados em função do caso quadrilátero.
 */
template <std::size_t N1, std::size_t N2>
class GLTriangRule : public GLRule2D
{
public:
    typedef Vector<2> Point;

    GLTriangRule() = default;
    ~GLTriangRule() = default;

    std::size_t nPoints(std::size_t index) const override;

    Point point(std::size_t index1, std::size_t index2) const override;
    double weight(std::size_t index1, std::size_t index2) const override;

private:
    GLQuadRule<N1> glQuadrature1_;
    GLQuadRule<N2> glQuadrature2_;
};

template <std::size_t N1, std::size_t N2>
std::size_t
GLTriangRule<N1, N2>::nPoints(std::size_t index) const
{
    return (index == 0) ? N1 : N2;
}

template <std::size_t N1, std::size_t N2>
typename GLTriangRule<N1, N2>::Point
GLTriangRule<N1, N2>::point(std::size_t index1, std::size_t index2) const
{
    double xiB = 0.5 * (1. + glQuadrature1_.point(index1));
    double etaB = 0.5 * (1. + glQuadrature2_.point(index2));
    Point point_({ (1. - 0.5 * etaB) * xiB, (1. - 0.5 * xiB) * etaB });
    return point_;
}

template <std::size_t N1, std::size_t N2>
double
GLTriangRule<N1, N2>::weight(std::size_t index1, std::size_t index2) const
{
    double xiB = 0.5 * (1. + glQuadrature1_.point(index1));
    double etaB = 0.5 * (1. + glQuadrature2_.point(index2));
    return 0.25 * (1. - 0.5 * (xiB + etaB)) * glQuadrature1_.weight(index1)
            * glQuadrature2_.weight(index2);
}

#endif
