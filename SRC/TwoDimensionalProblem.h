#ifndef TwoDimensionalProblem_H
#define TwoDimensionalProblem_H

#include "BilinearFormFunction.h"
#include "LinearFormFunction.h"
#include "StdBdryElem2D.h"
#include <vector>

/*
 * PROBLEMA CONTÍNUO: encontrar u pertencente a U tal que
 *
 *    { -∇·(Κ ∇u) + β ∇u + α u = f    em   Ω,
 *
 * sujeito às condições de fronteira:
 *
 *    {  ∇u·n + γ u = g    em   ΓR
 *    {  ∇u·n       = h    em   ΓN  .
 *    {   u         = w    em   ΓD
 *
 *
 *
 * PROBLEMA DISCRETO: encontrar uh pertencente a Uh tal que
 *
 *    { ∫_Ω (Κ ∇uh ∇vh) dA + ∫_Ω (β ∇uh vh) dA + ∫_Ω (α uhvh) dA + BDRY_LHS
 *    {
 *    {         = ∫_Ω (fvh) dA + BDRY_RHS,
 *
 * para todo uh pertencente a Uh, onde BDRY_LHS e BDRY_RHS são somas
 * de integrais definidas sobre os diferentes tipos de fronteira: Robin,
 * Neumann e Dirichlet. A saber, tais integrais são
 *
 * Robin:
 *      BDRY_LHS: ∫_ΓR  (γ uhvh) dS
 *      BDRY_RHS: ∫_ΓR  (gvh) dS
 *
 * Neumann:
 *      BDRY_LHS: 0
 *      BDRY_RHS: ∫_ΓN  (hvh) dS
 *
 * Dirichlet:
 *      BDRY_LHS: α_D ∫_ΓD  (∇vh·n uh) dS - ∫_ΓD  (∇uh·n vh) dS + β_D ∫_ΓD
 * (uhvh) dS BDRY_RHS: α_D ∫_ΓD  (∇vh·n w) dS + β_D ∫_ΓD  (wvh) dS
 *
 * No caso de Dirichlet, o método de Nitsche é empregado, e duas contantes
 * são necessárias: α_D, que multiplica os termos com gradiente, e
 * β_D que multiplica os termos com avaliações de funções base uh e vh.
 */

/***********************************************************************
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                         FORMA BILINEAR                            **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 ***********************************************************************
 */
class BilFormFunc2D : public BilinearFormFunction<2>
{
public:
    typedef Vector<2> LocalPointN;
    typedef Vector<1> LocalPointNm1;
    typedef double (BilFormFunc2D::*BoundaryConditionsImposer)(
            std::size_t, std::size_t, const LocalPointNm1&);

    BilFormFunc2D(StdElemInterp<2>& interp, StdBdryElem2D& interpBdry,
            const std::vector<double>& params = defaultParams_);

    double evaluate_interior(std::size_t i, std::size_t j,
            const LocalPointN& localPoint) override;
    double evaluate_boundary(std::size_t i, std::size_t j,
            const LocalPointNm1& localPoint) override;

    /* funções de definição da equação do modelo */
    Matrix<2, 2> kappa(const LocalPointN& localPoint);
    Vector<2> beta(const LocalPointN& localPoint);
    double alpha(const LocalPointN& localPoint);

    /* funções de definição da cond. de fronteira de Robin */
    double gamma(const LocalPointNm1& localPoint);

    /* funções de definição da cond. de fronteira de Dirichlet */
    double nitsche_func_const() const { return params_[0]; }
    double nitsche_grad_const() const { return params_[1]; }

    bool is_symmetric() const override;

private:
    double no_boundary_condition(
            std::size_t i, std::size_t j, const LocalPointNm1& localPoint);
    double robin_boundary_condition(
            std::size_t i, std::size_t j, const LocalPointNm1& localPoint);
    double neumann_boundary_condition(
            std::size_t i, std::size_t j, const LocalPointNm1& localPoint);
    double dirichlet_boundary_condition(
            std::size_t i, std::size_t j, const LocalPointNm1& localPoint);

    static std::vector<double> defaultParams_;
    std::vector<BoundaryConditionsImposer> bc_;

protected:
    const std::vector<double>& params_;
};

/***********************************************************************
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                           FORMA LINEAR                            **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 ***********************************************************************
 */
class LinearFormFunc2D : public LinearFormFunction<2>
{
public:
    typedef Vector<2> LocalPointN;
    typedef Vector<1> LocalPointNm1;
    typedef double (LinearFormFunc2D::*BoundaryConditionsImposer)(
            std::size_t, const LocalPointNm1&);

    LinearFormFunc2D(StdElemInterp<2>& interp, StdBdryElem2D& interpBdry,
            const std::vector<double>& params = defaultParams_);

    double
    evaluate_interior(std::size_t i, const LocalPointN& localPoint) override;
    double
    evaluate_boundary(std::size_t i, const LocalPointNm1& localPoint) override;

    /* funções de definição da equação do modelo */
    double f(const LocalPointN& localPoint);

    /* funções de definição da cond. de fronteira de Robin */
    double g(const LocalPointNm1& localPoint);

    /* funções de definição da cond. de fronteira de Neumann */
    double h(const LocalPointNm1& localPoint);

    /* funções de definição da cond. de fronteira de Dirichlet */
    double w(const LocalPointNm1& localPoint);
    double nitsche_func_const() const { return params_[0]; }
    double nitsche_grad_const() const { return params_[1]; }

private:
    double
    no_boundary_condition(std::size_t i, const LocalPointNm1& localPoint);
    double
    robin_boundary_condition(std::size_t i, const LocalPointNm1& localPoint);
    double
    neumann_boundary_condition(std::size_t i, const LocalPointNm1& localPoint);
    double dirichlet_boundary_condition(
            std::size_t i, const LocalPointNm1& localPoint);

    static std::vector<double> defaultParams_;
    std::vector<BoundaryConditionsImposer> bc_;

protected:
    const std::vector<double>& params_;
};

#endif
