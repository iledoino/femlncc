#ifndef GLQuadRule2D_H
#define GLQuadRule2D_H

#include "GLQuadRule.h"
#include "GLRule.h"

/**
 * Um objeto do tipo GLQuadRule2D fornece pontos e pesos necessários
 * para se realizar uma integração numérica em 2D usando o método de
 * Gauss-Legendre, em um domínio do tipo quadrilátero
 */
template <std::size_t N1, std::size_t N2>
class GLQuadRule2D : public GLRule2D
{
public:
    typedef Vector<2> Point;

    GLQuadRule2D() = default;
    ~GLQuadRule2D() = default;

    std::size_t nPoints(std::size_t index) const override;

    Point point(std::size_t index1, std::size_t index2) const override;
    double weight(std::size_t index1, std::size_t index2) const override;

    GLQuadRule<N1>& glQuadrature1() { return glQuadrature1_; }
    const GLQuadRule<N1>& glQuadrature1() const { return glQuadrature1_; }

    GLQuadRule<N2>& glQuadrature2() { return glQuadrature2_; }
    const GLQuadRule<N2>& glQuadrature2() const { return glQuadrature2_; }

private:
    GLQuadRule<N1> glQuadrature1_;
    GLQuadRule<N2> glQuadrature2_;
};

template <std::size_t N1, std::size_t N2>
std::size_t
GLQuadRule2D<N1, N2>::nPoints(std::size_t index) const
{
    return (index == 0) ? N1 : N2;
}

template <std::size_t N1, std::size_t N2>
typename GLQuadRule2D<N1, N2>::Point
GLQuadRule2D<N1, N2>::point(std::size_t index1, std::size_t index2) const
{
    Point point_(
            { glQuadrature1_.point(index1), glQuadrature2_.point(index2) });
    return point_;
}

template <std::size_t N1, std::size_t N2>
double
GLQuadRule2D<N1, N2>::weight(std::size_t index1, std::size_t index2) const
{
    return glQuadrature1_.weight(index1) * glQuadrature2_.weight(index2);
}

#endif
