#ifndef ElemMatrix_H
#define ElemMatrix_H

#include "BilinearFormFunction.h"
#include "Element.h"
#include "GLRule.h"
#include "LinearFormFunction.h"

/**
 * Um objeto do tipo ElemMatrix<N> é um construtor de matrizes de
 * elemento em N-D locais usadas na construção da matriz de rigidez
 */
template <std::size_t N>
class ElemMatrix
{
public:
    ElemMatrix(BilinearFormFunction<N>& bilinearFormFunction,
            LinearFormFunction<N>& linearFormFunction,
            const GLRule<N>& glRuleInterior,
            const GLRule<N - 1>& glRuleBoundary);

    std::vector<double> get(const Element<N>& elem, int bdryType = 0) const;
    std::vector<double> rhs(const Element<N>& elem, int bdryType = 0) const;

private:
    double integrate(std::size_t i, std::size_t j, int bdryType) const;
    double integrate(std::size_t i, int bdryType) const;
    std::size_t getFirstIndex(bool is_symmetric, std::size_t i) const;

    BilinearFormFunction<N>& bilinearFormFunction_;
    LinearFormFunction<N>& linearFormFunction_;
    const GLRule<N>& glRuleInterior_;
    const GLRule<N - 1>& glRuleBoundary_;
};

template <std::size_t N>
ElemMatrix<N>::ElemMatrix(BilinearFormFunction<N>& bilinearFormFunction__,
        LinearFormFunction<N>& linearFormFunction__,
        const GLRule<N>& glRuleInterior__,
        const GLRule<N - 1>& glRuleBoundary__)
    : bilinearFormFunction_(bilinearFormFunction__)
    , linearFormFunction_(linearFormFunction__)
    , glRuleInterior_(glRuleInterior__)
    , glRuleBoundary_(glRuleBoundary__)
{
}

template <std::size_t N>
std::size_t
ElemMatrix<N>::getFirstIndex(bool is_symmetric, std::size_t i) const
{
    return is_symmetric ? i : 0;
}

template <std::size_t N>
std::vector<double>
ElemMatrix<N>::get(const Element<N>& elem__, int bdryType) const
{
    std::size_t nBaseFunctions;
    if (bdryType == 0) {
        auto& stdElemInterp = bilinearFormFunction_.stdElemInterp();
        stdElemInterp.setUp(elem__);
        nBaseFunctions = stdElemInterp.nBaseFunctions();
    }
    else {
        auto& stdBdryElemInterp = bilinearFormFunction_.stdBdryElemInterp();
        stdBdryElemInterp.setUp(elem__, bdryType);
        nBaseFunctions = stdBdryElemInterp.nBaseFunctions();
    }
    bool is_symmetric = bilinearFormFunction_.is_symmetric();
    std::size_t nEntries(nBaseFunctions * nBaseFunctions);
    std::vector<double> matrix(nEntries, 0.);

    for (std::size_t i = 0, ii = 0; i < nBaseFunctions;
            i++, ii += nBaseFunctions)
        for (std::size_t j = getFirstIndex(is_symmetric, i); j < nBaseFunctions;
                ++j)
            matrix[ii + j] = integrate(i, j, bdryType);

    /*
     * TO DO: Fazer loop mais esperto para facilitar o gerenciamento da
     * cache
     */
    if (is_symmetric) {
        for (std::size_t i = 0, ii = 0; i < nBaseFunctions;
                i++, ii += nBaseFunctions)
            for (std::size_t j = i + 1, jj = j * nBaseFunctions;
                    j < nBaseFunctions; ++j, jj += nBaseFunctions)
                matrix[jj + i] = matrix[ii + j];
    }

    return matrix;
}

template <std::size_t N>
std::vector<double>
ElemMatrix<N>::rhs(const Element<N>& elem__, int bdryType) const
{
    std::size_t nBaseFunctions;
    if (bdryType == 0) {
        auto& stdElemInterp = linearFormFunction_.stdElemInterp();
        stdElemInterp.setUp(elem__);
        nBaseFunctions = stdElemInterp.nBaseFunctions();
    }
    else {
        auto& stdBdryElemInterp = linearFormFunction_.stdBdryElemInterp();
        stdBdryElemInterp.setUp(elem__, bdryType);
        nBaseFunctions = stdBdryElemInterp.nBaseFunctions();
    }
    std::vector<double> vector(nBaseFunctions, 0.);

    for (std::size_t i = 0; i < nBaseFunctions; i++)
        vector[i] = integrate(i, bdryType);

    return vector;
}

#endif
