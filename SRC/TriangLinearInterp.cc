#include "TriangLinearInterp.h"
#include <cassert>

void
TriangLinearInterp::setUp(const Element<2>& elem__)
{
#ifdef SECURE_COMPUTATION
    assert(elem__.points().size() == 3)
#endif
            setUp(elem__.points()[0], elem__.points()[1], elem__.points()[2]);
}

void
TriangLinearInterp::setUp(
        const LocalPoint& P1__, const LocalPoint& P2__, const LocalPoint& P3__)
{
    P1_ = P1__;
    P2_ = P2__;
    P3_ = P3__;
    x2mx1 = P2_[0] - P1_[0];
    x3mx1 = P3_[0] - P1_[0];
    y2my1 = P2_[1] - P1_[1];
    y3my1 = P3_[1] - P1_[1];
    area = x2mx1 * y3my1 - x3mx1 * y2my1;

#ifdef SECURE_COMPUTATION
    double delta = 1.e-14;
    double absArea = std::abs(area);
    assert(absArea > delta);
#endif

    oneOverArea = 1.0 / area;
    j11 = y3my1 * oneOverArea;
    j12 = -x3mx1 * oneOverArea;
    j21 = -y2my1 * oneOverArea;
    j22 = x2mx1 * oneOverArea;
    b1 = P1_[0];
    b2 = P1_[1];
}

std::size_t
TriangLinearInterp::nBaseFunctions() const
{
    return 3;
}

double
TriangLinearInterp::baseFunction(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 3)
#endif
            if (index_ == 0)
    {
        return 1. - localPoint_[0] - localPoint_[1];
    }
    else { return localPoint_[index_ - 1]; }
}

typename TriangLinearInterp::Gradient
TriangLinearInterp::baseGradient(
        std::size_t index_, const LocalPoint& localPoint_)
{
#ifdef SECURE_COMPUTATION
    assert(index_ < 3)
#endif
            Gradient g;

    if (index_ == 0) {
        g[0] = g[1] = -1.;
    }
    else {
        g[index_ - 1] = 1.;
        g[2 - index_] = 0.;
    }
    return mult(g, jacInverse(localPoint_));
}

// TO DO: Adicionar derivadas propriamente, contando com a transformacao
typename TriangLinearInterp::Jacobian
TriangLinearInterp::baseJacobian(
        std::size_t index_, const LocalPoint& localPoint_)
{
    Jacobian j;
    assign(j, 0.);
    return j;
}

typename TriangLinearInterp::GlobalPoint
TriangLinearInterp::globalPoint(const LocalPoint& localPoint_)
{
    double xi = localPoint_[0], eta = localPoint_[1];

#ifdef SECURE_COMPUTATION
    assert(xi >= 0. && xi <= 1.);
    assert(eta >= 0. && eta <= 1.);
#endif

    return { x2mx1 * xi + x3mx1 * eta + b1, y2my1 * xi + y3my1 * eta + b2 };
}

typename TriangLinearInterp::Jacobian
TriangLinearInterp::jacobian(const LocalPoint& localPoint_)
{
    Jacobian J_;
    J_[0][0] = x2mx1;
    J_[0][1] = x3mx1;
    J_[1][0] = y2my1;
    J_[1][1] = y3my1;
    return J_;
}

double
TriangLinearInterp::jacDeterminant(const LocalPoint& localPoint_)
{
    return area;
}

typename TriangLinearInterp::JacInverse
TriangLinearInterp::jacInverse(const LocalPoint& localPoint_)
{
    JacInverse JInv_;
    JInv_[0][0] = j11;
    JInv_[0][1] = j12;
    JInv_[1][0] = j21;
    JInv_[1][1] = j22;
    return JInv_;
}
