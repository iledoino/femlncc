# FEMLNCC

This is a finite element solver for convection-diffusion-reaction equations in 1D/2D. It has been created as part of a project requirement for passing the "Computation Fluid Mechanics" class at LNCC in 2017.
