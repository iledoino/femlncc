#include "ConjugateGradient.h"
#include "GLQuadRule.h"
#include "GLQuadRule2D.h"
#include "GLTriangRule.h"
#include "Grid.h"
#include "QuadLinearInterp.h"
#include "RigidityMatrix.h"
#include "StdBdryElem1D.h"
#include "StdBdryElem2D.h"
#include "TriangLinearInterp.h"
#include "TwoDimensionalGrid.h"
#include "TwoDimensionalProblem.h"
#include "lumbts.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#define SECURE_COMPUTATION
#define USE_TRIANGLE

class ErroL2
{
public:
    ErroL2(const Grid<2>& grid, const GLRule<2>& glRuleInterior,
            const GLRule<1>& glRuleBoundary, BilFormFunc2D& b,
            LinearFormFunc2D& l, std::vector<double> params);

    void make(const std::vector<double>& numSol);

    double realSol(const Vector<2> X);
    Vector<2> realSolGrad(const Vector<2> X);

private:
    const Grid<2>& grid_;
    const GLRule<2>& glRuleInterior_;
    const GLRule<1>& glRuleBoundary_;
    BilFormFunc2D& b_;
    LinearFormFunc2D& l_;
    std::vector<double> params_;
};

ErroL2::ErroL2(const Grid<2>& grid__, const GLRule<2>& glRuleInterior__,
        const GLRule<1>& glRuleBoundary__, BilFormFunc2D& b__,
        LinearFormFunc2D& l__, std::vector<double> params__)
    : grid_(grid__)
    , glRuleInterior_(glRuleInterior__)
    , glRuleBoundary_(glRuleBoundary__)
    , b_(b__)
    , l_(l__)
    , params_(params__)
{
}

double
ErroL2::realSol(const Vector<2> X)
{
    return (params_[4] / (2. * M_PI * params_[2] + params_[3]))
            * std::sin(M_PI * X[0]) * std::sin(M_PI * X[1]);
}

Vector<2>
ErroL2::realSolGrad(const Vector<2> X)
{
    double c = (params_[4] / (2. * M_PI * params_[2] + params_[3])) * M_PI;
    return { c * std::cos(M_PI * X[0]) * std::sin(M_PI * X[1]),
        c * std::sin(M_PI * X[0]) * std::cos(M_PI * X[1]) };
}

void
ErroL2::make(const std::vector<double>& numSol_)
{
    double l2norm = 0.0, gradl2norm = 0.0;
    double havrg = 0., qavrg = 0.0;
    int hnum = 0;

    for (const auto& elem_ : grid_.interior()) {

        double elemIntegral = 0.0, elemIntegralGrad = 0.0;

        auto& stdElemInterp = b_.stdElemInterp();
        stdElemInterp.setUp(elem_);

        std::vector<std::size_t> gI = elem_.globalIndices();
        std::size_t nPointsX = glRuleInterior_.nPoints(0);
        std::size_t nPointsY = glRuleInterior_.nPoints(1);
        for (std::size_t ii = 0; ii < nPointsX; ++ii)
            for (std::size_t jj = 0; jj < nPointsY; ++jj) {
                Index<2> index({ ii, jj });
                Vector<2> localPoint(glRuleInterior_.point(index));
                Vector<2> globalPoint(stdElemInterp.globalPoint(localPoint));

                double localSol_ = 0.0;
                for (std::size_t i = 0; i < gI.size(); ++i)
                    localSol_ += numSol_[gI[i]]
                            * stdElemInterp.baseFunction(i, localPoint);
                localSol_ -= realSol(globalPoint);

                Vector<2> localSolGrad_;
                localSolGrad_[0] = localSolGrad_[1] = 0.;
                for (std::size_t i = 0; i < gI.size(); ++i)
                    for (std::size_t j = 0; j < 2; ++j)
                        localSolGrad_[j] += numSol_[gI[i]]
                                * stdElemInterp.baseGradient(i, localPoint)[j];
                for (std::size_t j = 0; j < 2; ++j)
                    localSolGrad_[j] -= realSolGrad(globalPoint)[j];

                elemIntegral += glRuleInterior_.weight(index) * (localSol_)
                        * (localSol_) *stdElemInterp.jacDeterminant(localPoint);

                elemIntegralGrad += glRuleInterior_.weight(index)
                        * norm(localSolGrad_)
                        * stdElemInterp.jacDeterminant(localPoint);
            }

        l2norm += elemIntegral;
        gradl2norm += elemIntegralGrad;

        const auto points = elem_.points();
        if (gI.size() == 4) {
            Vector<2> diag1, diag2;
            diag1[0] = diag2[0] = diag1[1] = diag2[1] = 0.;

            for (int i = 0; i < 2; ++i) {
                diag1[i] = points[2][i] - points[0][i];
                diag2[i] = points[3][i] - points[1][i];
            }

            double nd1 = norm(diag1), nd2 = norm(diag2);
            if (nd1 > nd2)
                havrg += nd1;
            else
                havrg += nd2;
        }
        else if (gI.size() == 3) {
            Vector<2> diag1, diag2, diag3;
            diag1[0] = diag2[0] = diag3[0] = diag1[1] = diag2[1] = diag3[1]
                    = 0.;

            for (int i = 0; i < 2; ++i) {
                diag1[i] = points[0][i] - points[1][i];
                diag2[i] = points[1][i] - points[2][i];
                diag3[i] = points[0][i] - points[2][i];
            }

            double nd1 = norm(diag1), nd2 = norm(diag2), nd3 = norm(diag3);
            // if (nd1 > nd2)
            //{
            // if (nd1 > nd3)
            // havrg += nd1;
            // else
            // havrg += nd3 > nd2 ? nd3 : nd2;
            //}
            // else
            //{
            // if (nd2 > nd3)
            // havrg += nd2;
            // else
            // havrg += nd3 > nd1 ? nd3 : nd1;
            //}
            havrg += nd3 * nd2 * nd1
                    / std::sqrt(std::abs((nd1 + nd2 + nd3) * (nd2 + nd3 - nd1)
                            * (nd3 + nd1 - nd2) * (nd1 + nd2 - nd3)));
        }
        else {
            exit(1);
        }
        hnum++;
    }
    l2norm = std::sqrt(l2norm);
    gradl2norm = std::sqrt(gradl2norm);
    havrg /= hnum;

    std::cout << "l2norm = [l2norm " << l2norm << "];\n";
    std::cout << "gradl2norm = [gradl2norm " << gradl2norm << "];\n";
    std::cout << "havrg = [havrg " << havrg << "];\n";
    std::cout << "Pe = " << std::sqrt(10) * 0.5 * havrg / params_[2] << " \n";
}

//**********************************************************************
//                           TESTE EM 2D
//**********************************************************************
/**
 * PROBLEMA CONTÍNUO: encontrar u pertencente a U tal que
 *
 *    { -∇·(Κ ∇u) + β ∇u + α u = f    em   Ω,
 *
 * sujeito às condições de fronteira:
 *
 *    {  ∇u·n + γ u = g    em   ΓR
 *    {  ∇u·n       = h    em   ΓN  .
 *    {   u         = w    em   ΓD
 *
 *
 *
 * PROBLEMA DISCRETO: encontrar uh pertencente a Uh tal que
 *
 *    { ∫_Ω (Κ ∇uh ∇vh) dA + ∫_Ω (β ∇uh vh) dA + ∫_Ω (α uhvh) dA + BDRY_LHS
 *    {
 *    {         = ∫_Ω (fvh) dA + BDRY_RHS,
 *
 * para todo uh pertencente a Uh, onde BDRY_LHS e BDRY_RHS são somas
 * de integrais definidas sobre os diferentes tipos de fronteira: Robin,
 * Neumann e Dirichlet. A saber, tais integrais são
 *
 * Robin:
 *      BDRY_LHS: ∫_ΓR  (γ uhvh) dS
 *      BDRY_RHS: ∫_ΓR  (gvh) dS
 *
 * Neumann:
 *      BDRY_LHS: 0
 *      BDRY_RHS: ∫_ΓN  (hvh) dS
 *
 * Dirichlet:
 *      BDRY_LHS: α_D ∫_ΓD  (∇vh·n uh) dS - ∫_ΓD  (∇uh·n vh) dS + β_D ∫_ΓD
 * (uhvh) dS BDRY_RHS: α_D ∫_ΓD  (∇vh·n w) dS + β_D ∫_ΓD  (wvh) dS
 *
 * No caso de Dirichlet, o método de Nitsche é empregado, e duas contantes
 * são necessárias: α_D, que multiplica os termos com gradiente, e
 * β_D que multiplica os termos com avaliações de funções base uh e vh.
 */
/* Programa principal */
int
test2D(void)
{
    std::string fileName; // nome dos arquivos de saida do easymesh ou triangle
    std::cin >> fileName;
    std::size_t nele;
    std::cin >> nele;
    std::size_t paramsSize;
    std::cin >> paramsSize;
    /*
     * Parametros da fisica
     */
    std::vector<double> params(paramsSize, 0.);
    for (std::size_t i = 0; i < paramsSize; ++i)
        std::cin >> params[i];

    /*
     * Criação de um Grid2D
     */
    Vector<2> corner1({ 0., 0. }); // canto inferior do domínio retangular
    Vector<2> corner2({ 1., 1. }); // canto superior do domínio retangular
    Index<2> nElements(
            { nele, nele }); // número desejado de elementos, em cada direção

#ifdef USE_TRIANGLE
    Grid2D grid2D(fileName); // grade de elementos
#else
    Grid2D grid2D(corner1, corner2, nElements); // grade de elementos
#endif

    /*
     * Criação de uma ElemMatrix<2>
     */
#ifdef USE_TRIANGLE
    TriangLinearInterp
            elemInterp; // interpolante em 2D para elemento de referência
#else
    QuadLinearInterp2D
            elemInterp; // interpolante em 2D para elemento de referência
#endif
    StdBdryElem2D stdBdryElem2D; // interpolante em 2D para elementos-fronteira
                                 // de referência
    BilFormFunc2D bilFormFunc2D(
            elemInterp, stdBdryElem2D, params); // forma bilinear
    LinearFormFunc2D linearFormFunc2D(
            elemInterp, stdBdryElem2D, params); // forma linear

#ifdef USE_TRIANGLE
    GLTriangRule<2, 2> glRule2D; // regra de integração em 2D
#else
    GLQuadRule2D<4, 4> glRule2D; // regra de integração em 2D
#endif
    GLQuadRule<4> glRule1D; // regra de integração em 1D

    ElemMatrix<2> elemMatrix(bilFormFunc2D, linearFormFunc2D, glRule2D,
            glRule1D); // matrix de elemento, bem como termo de carga de
                       // elemento

    /*
     * Criação de uma RigidityMatrix<2>
     */
    RigidityMatrix<2> rigidityMatrix(
            grid2D, elemMatrix); // matriz de rigidez, bem como termo de carga

    /*
     * Solução do Sistema Linear
     */
    bool solveByLU = true;

    if (solveByLU) {
        std::vector<double> matrix, rhs;
        rigidityMatrix.get(matrix, rhs);
        std::size_t nRows = rhs.size();

        LU lu(nRows);
        std::vector<int> perm(nRows, 0.);
        double zero = 1.0e-16, invConditionNumber = 1.0;
        if (lu.LUdecomposition(
                    matrix.data(), perm.data(), zero, invConditionNumber)) {
            lu.LUSolver(matrix.data(), rhs.data(), 1, perm.data());

            grid2D.print();
#ifdef USE_TRIANGLE
            std::cout << "Znum = [";
            for (std::size_t i = 0; i < rhs.size(); ++i)
                std::cout << rhs[i] << std::endl;
            std::cout << "];" << std::endl;
            std::cout << "figure(1);\ntrisurf(ELEM, X, Y, Znum);" << std::endl;
            // std::cout << "Z = Znum;\nZ(:) = " << params[4]/(2.*M_PI*params[2]
            // + params[3]) << "*(sin(pi*X).*sin(pi*Y));" << std::endl; std::cout
            // << "figure(2);\ntrisurf(ELEM, X, Y, Z);" << std::endl; std::cout
            // << "figure(3);\ntrisurf(ELEM, X, Y, Z-Znum);" << std::endl;
#else
            std::cout << "Znum = [";
            for (std::size_t i = 0; i < nElements[1]; ++i) {
                for (std::size_t j = 0; j < nElements[0] + 1; ++j) {
                    std::cout << rhs[i * (nElements[0] + 1) + j] << " ";
                }
                std::cout << "; ";
            }
            for (std::size_t j = 0; j < nElements[0] + 1; ++j) {
                std::cout << rhs[(nElements[1]) * (nElements[0] + 1) + j]
                          << " ";
            }
            std::cout << "];" << std::endl;
            std::cout << "figure(1);\nsurf(X, Y, Znum);" << std::endl;
            // std::cout << "Z = Znum;\nZ(:) = " << params[4]/(2.*M_PI*params[2]
            // + params[3]) << "*(sin(pi*X).*sin(pi*Y));" << std::endl; std::cout
            // << "figure(2);\nsurf(X, Y, Z);" << std::endl; std::cout <<
            // "figure(3);\nsurf(X, Y, Z-Znum);" << std::endl; std::cout <<
            // "figure(1);\ntrisurf(ELEM, X, Y, Znum);" << std::endl; std::cout
            // << "Z = Znum;\nZ(:) = " << params[4]/(2.*M_PI*params[2] +
            // params[3]) << "*(sin(pi*X).*sin(pi*Y));" << std::endl; std::cout
            // << "figure(2);\ntrisurf(ELEM, X, Y, Z);" << std::endl; std::cout
            // << "figure(3);\ntrisurf(ELEM, X, Y, Z-Znum);" << std::endl;
#endif
            ErroL2 errol2(grid2D, glRule2D, glRule1D, bilFormFunc2D,
                    linearFormFunc2D, params);
            errol2.make(rhs);
        }
    }
    else {
        std::size_t nRows = grid2D.size();
        ConjugateGradient cg(nRows);
        rigidityMatrix.get(cg.A(), cg.b());

        std::size_t maxNIter = nRows;
        double residue_tol = 1.0e-6, step_tol = 1.0e-6;
        // TO DO: Antes de rodar cg, preencher guess
        cg.run(maxNIter, residue_tol, step_tol);

        grid2D.print();
#ifdef USE_TRIANGLE
        std::cout << "Znum = [";
        for (std::size_t i = 0; i < nRows; ++i)
            std::cout << cg.sol(i) << std::endl;
        std::cout << "];" << std::endl;
        std::cout << "figure(1);\ntrisurf(ELEM, X, Y, Znum);" << std::endl;
        std::cout << "Z = Znum;\nZ(:) = "
                  << params[4] / (2. * M_PI * params[2] + params[3])
                  << "*(sin(pi*X).*sin(pi*Y));" << std::endl;
        std::cout << "figure(2);\ntrisurf(ELEM, X, Y, Z);" << std::endl;
        std::cout << "figure(3);\ntrisurf(ELEM, X, Y, Z-Znum);" << std::endl;
#else
        std::cout << "Znum = [";
        for (std::size_t i = 0; i < nElements[1]; ++i) {
            for (std::size_t j = 0; j < nElements[0] + 1; ++j) {
                std::cout << cg.sol(i * (nElements[0] + 1) + j) << " ";
            }
            std::cout << "; ";
        }
        for (std::size_t j = 0; j < nElements[0] + 1; ++j) {
            std::cout << cg.sol((nElements[1]) * (nElements[0] + 1) + j) << " ";
        }
        std::cout << "];" << std::endl;
        // std::cout << "figure(1);\nsurf(X, Y, Znum);" << std::endl;
        // std::cout << "Z = Znum;\nZ(:) = " << params[4]/(2.*M_PI*params[2] +
        // params[3]) << "*(sin(pi*X).*sin(pi*Y));" << std::endl; std::cout <<
        // "figure(2);\nsurf(X, Y, Z);" << std::endl; std::cout <<
        // "figure(3);\nsurf(X, Y, Z-Znum);" << std::endl;
        std::cout << "figure(1);\ntrisurf(ELEM, X, Y, Znum);" << std::endl;
        std::cout << "Z = Znum;\nZ(:) = "
                  << params[4] / (2. * M_PI * params[2] + params[3])
                  << "*(sin(pi*X).*sin(pi*Y));" << std::endl;
        std::cout << "figure(2);\ntrisurf(ELEM, X, Y, Z);" << std::endl;
        std::cout << "figure(3);\ntrisurf(ELEM, X, Y, Z-Znum);" << std::endl;
#endif
        ErroL2 errol2(grid2D, glRule2D, glRule1D, bilFormFunc2D,
                linearFormFunc2D, params);
        errol2.make(cg.x__);
    }

    return 1;
}

//**********************************************************************

void test1();
void test2();
void test3();
void test4();

int
main()
{
    // test1();
    // test2();
    // test3();
    // test4();
    // test1D();
    test2D();
}
