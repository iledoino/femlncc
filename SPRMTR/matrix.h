#ifndef SMatrix_h
#define SMatrix_h

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

/*
 * De Numerical Recipes
 */
struct NRsparseCol
{
    int nrows;
    int nvals;
    std::vector<int> row_ind;
    std::vector<double> val;

    NRsparseCol(int m, int nnvals)
        : nrows(m), nvals(nnvals), row_ind(nnvals, 0), val(nnvals, 0.0)
    {
    }

    NRsparseCol() : nrows(0), nvals(0), row_ind(), val() {}

    void resize(int m, int nnvals)
    {
        nrows = m;
        nvals = nnvals;
        row_ind.resize(nnvals);
        val.resize(nnvals);
    }
};

struct NRsparseMat
{
    int nrows;
    int ncols;
    int nvals;
    std::vector<int> col_ptr;
    std::vector<int> row_ind;
    std::vector<double> val;

    NRsparseMat();
    NRsparseMat(int m, int n, int nnvals);
    std::vector<double> ax(const std::vector<double>& x) const;
    std::vector<double> atx(const std::vector<double>& x) const;
    NRsparseMat transpose() const;
};

/**                               LNCC
 *                  MESTRADO EM MODELAGEM COMPUTACIONAL
 *
 *               Trabalho 1 de Estrutura de Dados GA-024
 *               Professor: Antônio Tadeu Azevedo Gomes
 *                  Aluno: Ismael de Souza Ledoino
 *                         Período: 2016-I
 *
 *      Um TAD do tipo SMatrix auxilia em operações matriciais, com baixo
 * custo de memória. Este tipo de estrutura de dados foi especialmente
 * pensado para matrizes esparsas: matrizes onde as entradas nulas são
 * em número bem superior ao número de entradas não nulas.
 */

/*
 * Uma matriz esparsa é representada por nós do tipo matrix, que contém
 * informação de uma das entradas da matriz e em qual coluna e linha
 * está esta entrada. No caso em que a linha ou coluna é algum inteiro
 * não válido (-1), isso significa que o nó é a origem da linha ou
 * coluna que não contém o valor inválido. No caso em que ambos são não
 * válidos, o nó representa o "canto" da matrix, que é o nó origem de
 * todos os outros nós.
 */
typedef struct smatrix SMatrix;

/*
 * matrix_create --> cria a matriz, pegando seus valores não nulos da
 *                   entrada padrão
 */
int matrix_create(SMatrix** m);
int matrix_create(SMatrix** m, int nl, int nc);

/*
 * matrix_destroy --> destrói toda a memória cuja origem é a região de
 *                    memória apontada pelo nó passado como parâmetro.
 */
int matrix_destroy(SMatrix* m);

/*
 * matrix_print --> Imprime em stdout a matrix apontada por m.
 */
int matrix_print(const SMatrix* m);

/*
 * matrix_add --> Executa a soma matricial de m com n, criando em r uma
 *                matrix que guarda m + t*n.
 */
int matrix_add(const SMatrix* m, const SMatrix* n, SMatrix** r, double t = 1.);

/*
 * matrix_norm --> Encontra a norma da matrix, que é o máximo da soma
 *                  dos elementos de uma linha em  módulo
 */
int matrix_norm(const SMatrix* m, double* norm);

/*
 * matrix_multiply --> Executa a multiplicação matricial de m com n,
 *                     criando em r uma matrix que guarda o resultado.
 */
int matrix_multiply(const SMatrix* m, const SMatrix* n, SMatrix** r);

/*
 * matrix_internal_product --> Executa o produto interno entre m e n,
 *                     criando em r uma matrix que guarda o resultado.
 */
int matrix_internal_product(const SMatrix* m, const SMatrix* n, SMatrix** r);

/*
 * matrix_transpose --> Cria em r uma matrix que é a transposta de m
 */
int matrix_transpose(const SMatrix* m, SMatrix** r);

/*
 * matrix_getelem --> Consulta o valor guardado na linha x e coluna j,
 *                    copiando esse valor para elem.
 */
int matrix_getelem(const SMatrix* m, int x, int y, double* elem);

/*
 * matrix_getelem --> Insere na linha x e coluna y o valor não nulo de
 *                    elem
 */
int matrix_setelem(SMatrix* m, int x, int y, double elem);

/*
 * matrix_addelem --> Adiciona na linha x e coluna y o valor não nulo de
 *                    elem
 */
int matrix_addelem(SMatrix* m, int x, int y, double elem);

void matrix_to_NRsparseMat(SMatrix* m, NRsparseMat& m_spr);

#endif /* SMatrix_h */
