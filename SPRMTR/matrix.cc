#include "matrix.h"

//#define PRINTMATLAB // este define seleciona impressão que pode ser lida em
//matlab #define PRINTMATRIX // este define seleciona impressão para verificação
//visual

NRsparseMat::NRsparseMat()
    : nrows(0), ncols(0), nvals(0), col_ptr(), row_ind(), val()
{
}

NRsparseMat::NRsparseMat(int m, int n, int nnvals)
    : nrows(m)
    , ncols(n)
    , nvals(nnvals)
    , col_ptr(n + 1, 0)
    , row_ind(nnvals, 0)
    , val(nnvals, 0.0)
{
}

std::vector<double>
NRsparseMat::ax(const std::vector<double>& x) const
{
    std::vector<double> y(nrows, 0.0);
    for (int j = 0; j < ncols; j++) {
        for (int i = col_ptr[j]; i < col_ptr[j + 1]; i++)
            y[row_ind[i]] += val[i] * x[j];
    }
    return y;
}

std::vector<double>
NRsparseMat::atx(const std::vector<double>& x) const
{
    std::vector<double> y(ncols);
    for (int i = 0; i < ncols; i++) {
        y[i] = 0.0;
        for (int j = col_ptr[i]; j < col_ptr[i + 1]; j++)
            y[i] += val[j] * x[row_ind[j]];
    }
    return y;
}

NRsparseMat
NRsparseMat::transpose() const
{
    int i, j, k, index, m = nrows, n = ncols;
    NRsparseMat at(n, m, nvals);
    std::vector<int> count(m, 0);
    for (i = 0; i < n; i++)
        for (j = col_ptr[i]; j < col_ptr[i + 1]; j++) {
            k = row_ind[j];
            count[k]++;
        }
    for (j = 0; j < m; j++)
        at.col_ptr[j + 1] = at.col_ptr[j] + count[j];
    for (j = 0; j < m; j++)
        count[j] = 0;
    for (i = 0; i < n; i++)
        for (j = col_ptr[i]; j < col_ptr[i + 1]; j++) {
            k = row_ind[j];
            index = at.col_ptr[k] + count[k];
            at.row_ind[index] = i;
            at.val[index] = val[j];
            count[k]++;
        }
    return at;
}

/**                               LNCC
 *                  MESTRADO EM MODELAGEM COMPUTACIONAL
 *
 *               Trabalho 1 de Estrutura de Dados GA-024
 *               Professor: Antônio Tadeu Azevedo Gomes
 *                  Aluno: Ismael de Souza Ledoino
 *                         Período: 2016-I
 *
 *      Um TAD do tipo SMatrix auxilia em operações matriciais, com baixo
 * custo de memória. Este tipo de estrutura de dados foi especialmente
 * pensado para matrizes esparsas: matrizes onde as entradas nulas são
 * em número bem superior ao número de entradas não nulas.
 */

struct smatrix
{
    /*
     * right --> ponteiro que aponta para o próximo nó que contém um
     *       valor não nulo na mesma linha do nó atual.
     *
     * below --> ponteiro que aponta para o próximo nó que contém um
     *       valor não nulo na mesma coluna do nó atual.
     *
     * OBS: No caso de não haver próximos nós, os respectivos ponteiros
     * apontam para seus nós origem.
     */
    struct smatrix* right;
    struct smatrix* below;
    /*
     * line   --> índice da linha que contém o valor não nulo guardado
     *        em info.
     *
     * column --> índice da coluna que contém o valor não nulo guardado
     *        em info.
     *
     * OBS: No caso de nós origem, as variáveis descritas acima serão
     * preenchidos com o valor -1, e -n-1, onde n é o número da linha
     * ou coluna que tal nó dá origem na matriz.
     */
    int line;
    int column;
    /*
     * info --> contém o valor da entrada não nula da matriz.
     *
     * OBS: No caso de nós origem, o valor dessa variável corresponde à
     * quantidade de entradas não nulas na mesma linha, ou coluna, ou no
     * caso de nó origem da matrix, quantidade total de entradas não nulas.
     */
    double info;
};

/*
 * FUNÇÕES INTERNAS USADAS PARA MANIPULAÇÃO INTERNA
 */

int
has_not_been_created(const SMatrix* m)
{
    if ((m == NULL) || (m->right == NULL) || (m->below == NULL))
        return 0;
    return 1;
}

/*
 * 0 significa que dimensões foram checadas, e operações podem ser feitas
 */
int
check_for_dimensions(const SMatrix* m, int x, int y)
{
    if (has_not_been_created(m) == 0) {
        fprintf(stderr, "Erro: Sua matriz ainda não foi criada.\n");
        return 1;
    }

    if ((x < 1) || (y < 1) || (x > -m->line) || (y > -m->column)) {
        printf("Aviso: Tal elemento não existe na sua matriz.\n");
        return 1;
    }

    return 0;
}

/*
 * Faz uma pesquisa no nó-linha-origem, procurando pelo nó da linha x
 */
const SMatrix*
line_search(const SMatrix* m, int x)
{
    const SMatrix* m_origin = m;

    m = m->below;
    while ((m != m_origin) && (m->line < x)) {
        m = m->below;
    }

    return m;
}

/*
 * Faz uma pesquisa no nó-coluna-origem, procurando pelo nó da coluna y
 */
const SMatrix*
column_search(const SMatrix* m, int y)
{
    const SMatrix* m_origin = m;

    m = m->right;
    while ((m != m_origin) && (m->column < y)) {
        m = m->right;
    }

    return m;
}

/*
 * Faz uma pesquisa no nó-linha-origem, procurando pelo nó da linha x
 */
SMatrix*
below_line_search(SMatrix* m, int x)
{
    SMatrix* m_origin = m;

    while ((m->below != m_origin) && (m->below->line < x)) {
        m = m->below;
    }

    return m;
}

/*
 * Faz uma pesquisa no nó-coluna-origem, procurando pelo nó da coluna y
 */
SMatrix*
right_column_search(SMatrix* m, int y)
{
    SMatrix* m_origin = m;

    while ((m->right != m_origin) && (m->right->column < y)) {
        m = m->right;
    }

    return m;
}

/*
 * Libera a memória de uma linha
 */
void
line_destroy(SMatrix* m)
{
    SMatrix* m_tmp = NULL;
    SMatrix* m_origin = m;

    m = m->right;
    while (m != m_origin) {
        m_tmp = m;
        m = m->right;
        free(m_tmp);
    }
    m_origin->right = m_origin;
}

/*
 * matrix_delelem --> Remove elemento da matrix
 */
int
matrix_delelem(SMatrix* m, int x, int y)
{
    /*
     * Nesse caso a matriz é vazia ou deseja-se
     * remover elementos cujas posições na matriz
     * não fazem sentido
     */
    if ((has_not_been_created(m) == 0))
        return 0;
    if (check_for_dimensions(m, x, y) != 0)
        return 0;

    SMatrix* m_b_line = below_line_search(m, x);
    SMatrix* m_r_column = right_column_search(m, y);
    SMatrix* m_line = m_b_line->below;
    SMatrix* m_column = m_r_column->right;

    /*
     * Checa se a linha x e coluna y existem na matrix
     */
    if ((m_line == m) || (m_column == m) || (m_line->line != x)
            || (m_column->column != y))
        return 0;

    /*
     * Nesse ponto é certo que a linha x e coluna y existem
     * na matriz. Resta agora acessar o elemento cuja linha
     * é x e coluna é y. Depois da execução das próximas duas
     * funções, ambos m_line->right e m_column->below apontam
     * para o mesmo elemento
     */
    m_line->info -= 1.0;
    m_column->info -= 1.0;
    m->info -= 1.0;
    m_line = right_column_search(m_line, y);
    m_column = below_line_search(m_column, x);
    if (m_line->right != m_column->below) // INSERIDO POR ISMAEL EM 3/12/16
        return 0;

    /*
     * Deleta o elemento
     */
    SMatrix* m_del = m_line->right;
    m_column->below = m_del->below;
    m_line->right = m_del->right;
    free(m_del);

    return 0;
}

/*
 * matrix_create_base --> altera a região de memória apontada por m, que é um
 *                   nó matrix, fazendo desse nó um canto da matriz,
 *                   isto é, o nó origem da matrix esparsa.
 */
int
matrix_create_base(SMatrix** m)
{
    if (((*m) != NULL) && (((*m)->right != NULL) && ((*m)->below != NULL))) {
        // printf("Aviso: Sua matrix já foi criada: Nada a executar.\n");
        return 2;
    }

    (*m) = (SMatrix*) malloc(sizeof(SMatrix));

    if ((*m) == NULL) {
        fprintf(stderr, "Erro: memória insuficiente. Matriz não criada.\n");
        return 1;
    }

    (*m)->right = (*m)->below = (*m);
    (*m)->line = (*m)->column = -1;
    (*m)->info = 0.0;

    return 0;
}

/*
 * Aloca nós raízes linha
 */
int
matrix_create_lines(SMatrix* m, int nl)
{
    SMatrix* m_copy = m;
    for (int i = 1; i <= nl; ++i) {
        SMatrix* new_line = (SMatrix*) malloc(sizeof(SMatrix));
        if (new_line == NULL) {
            fprintf(stderr, "Erro: memória insuficiente.\n");
            return 1;
        }
        new_line->below = m_copy->below;
        m_copy->below = new_line;
        new_line->right = new_line;

        new_line->line = i;
        new_line->column = -1;
        new_line->info = 0.0;

        m_copy = new_line;
    }
    m->line = -nl;

    return 0;
}

/*
 * Aloca nós raízes coluna
 */
int
matrix_create_columns(SMatrix* m, int nc)
{
    SMatrix* m_copy = m;
    for (int i = 1; i <= nc; ++i) {
        SMatrix* new_column = (SMatrix*) malloc(sizeof(SMatrix));
        if (new_column == NULL) {
            fprintf(stderr, "Erro: memória insuficiente.\n");
            return 1;
        }
        new_column->right = m_copy->right;
        m_copy->right = new_column;
        new_column->below = new_column;

        new_column->column = i;
        new_column->line = -1;
        new_column->info = 0.0;

        m_copy = new_column;
    }
    m->column = -nc;

    return 0;
}

/*
 * FUNÇÕES EXTERNAS USADAS PARA MANIPULAÇÃO DO USUÁRIO
 */

/*
 * matrix_create --> cria a matriz, pegando seus valores não nulos da
 *                   entrada padrão
 */
int
matrix_create(SMatrix** m)
{
    if (matrix_create_base(m) != 0)
        return 1;

    printf("Sintaxe:\n");
    printf("    Primeiro digite o numero total de linhas nl e total de colunas "
           "nc: [ nl nc ]\n");
    printf("    Depois insira elemento elem na linha l e coluna c: [ l c elem "
           "]\n");
    printf("    Parar inserção: [ 0 ]\n");

    int nl, nc;

    scanf("%d %d", &nl, &nc);

    if ((nl <= 0) || (nc <= 0) || ((nl + nc) <= 0)) {
        printf("Dimensões de matriz inválidas:\n");
        return 1;
    }

    if (matrix_create_lines((*m), nl) != 0)
        return 1;

    if (matrix_create_columns((*m), nc) != 0)
        return 1;

    /*
     * Lê elementos até que 0 seja digitado para linha
     */
    int l = 0, c = 0;
    double elem;
    while ((l >= 0) && (c >= 0)) {
        scanf("%d", &l);
        if (l == 0)
            return 0;
        scanf("%d %lf", &c, &elem);
        if (matrix_setelem((*m), l, c, elem) != 0)
            return 1;
        l = c = 0;
    }

    return 0;
}

/*
 * Cria a SMatrix dados numero de linhas e de colunas
 */
int
matrix_create(SMatrix** m, int nl, int nc)
{
    if (matrix_create_base(m) != 0)
        return 1;

    if ((nl <= 0) || (nc <= 0) || ((nl + nc) <= 0)) {
        printf("Dimensões de matriz inválidas:\n");
        return 1;
    }

    if (matrix_create_lines((*m), nl) != 0)
        return 1;

    if (matrix_create_columns((*m), nc) != 0)
        return 1;

    return 0;
}

/*
 * matrix_destroy --> destrói toda a memória cuja origem é a região de
 *                    memória apontada pelo nó passado como parâmetro.
 */
int
matrix_destroy(SMatrix* m)
{
    if (has_not_been_created(m) == 0) {
        printf("Aviso: Não há nada a ser destruído.\n");
        return 0;
    }

    if ((m->right == m) && (m->below == m)) {
        m->right = m->below = NULL;
        free(m);
        return 0;
    }

    /*
     * Essa função é útil apenas para uso interno, uma vez que o usuário
     * só tem acesso ao nó origem da matriz.
     */
    if ((m->line >= 0) && (m->column >= 0)) {
        fprintf(stderr,
                "Erro: Esse nó corresponde a uma entrada da matrix. Nada a ser "
                "feito.\n");
        return 1;
    }

    SMatrix* m_current_line = NULL;
    SMatrix* m_element = NULL;

    /*
     * Libera a memória da linha de nós-coluna-origem
     */
    line_destroy(m);

    /*
     * Libera a memória da coluna de nós-linha-origem
     */
    m_current_line = m->below;
    while (m_current_line != m) {
        /*
         * Guarda um ponteiro para próxima linha, de depois libera o
         * espaço de memória na linha atual
         */
        m_element = m_current_line;
        m_current_line = m_current_line->below;
        line_destroy(m_element);
        free(m_element);
    }

    /*
     * Libera a memória do canto da matriz, ou nó origem principal
     */
    m->right = m->below = NULL;
    free(m);

    return 0;
}

/*
 * matrix_print --> Imprime em stdout a matrix apontada por m.
 */
int
matrix_print(const SMatrix* m)
{
    if (has_not_been_created(m) == 0) {
        fprintf(stderr, "Erro: Não há matriz para ser impressa.\n");
        return 1;
    }

    if ((m->right == m) && (m->below == m)) {
        printf("Aviso: Não há entradas para serem impressas.\n");
        return 2;
    }

#ifdef PRINTMATLAB
    int previous_column_index = 0;
    printf("\n[");
    for (SMatrix* iterator_line = m->below; iterator_line != m;
            iterator_line = iterator_line->below) {
        printf("  ");
        SMatrix* iterator_column = iterator_line->right;
        previous_column_index = 0;
        while (iterator_column != iterator_line) {
            for (int i = ++previous_column_index; i != iterator_column->column;
                    ++i)
                printf("%e ", 0.0);
            printf("%e ", iterator_column->info);
            previous_column_index = iterator_column->column;
            iterator_column = iterator_column->right;
        }
        for (int i = ++previous_column_index; i <= -m->column; ++i)
            printf("%e ", 0.0);
        if (iterator_line->below != m)
            printf(";\n");
        else
            printf("  ];\n");
    }
    printf("\n");
#else
#ifdef PRINTMATRIX
    /*
     * O proximo loop varre as linhas
     */
    int previous_column_index = 0;
    printf("\n");
    for (SMatrix* iterator_line = m->below; iterator_line != m;
            iterator_line = iterator_line->below) {
        /*
         * O proximo loop varre as colunas
         */
        printf("| ");
        SMatrix* iterator_column = iterator_line->right;
        previous_column_index = 0;
        while (iterator_column != iterator_line) {
            /*
             * Imprime espaços no lugar de zeros
             */
            for (int i = ++previous_column_index; i != iterator_column->column;
                    ++i)
                printf("%*c", 6, ' ');
            /*
             * Imprime o elemento não nulo
             */
            printf("%e ", iterator_column->info);
            previous_column_index = iterator_column->column;
            iterator_column = iterator_column->right;
        }
        /*
         * Imprime espaços no lugar de zeros
         */
        for (int i = ++previous_column_index; i <= -m->column; ++i)
            printf("%*c", 6, ' ');
        printf("|\n");
    }
    printf("\n");
#else
    /*
     * O proximo loop varre as linhas
     */
    printf("\n");
    printf("%d %d\n", -m->line, -m->column);
    for (SMatrix* iterator_line = m->below; iterator_line != m;
            iterator_line = iterator_line->below) {
        /*
         * O proximo loop varre as colunas
         */
        SMatrix* iterator_column = iterator_line->right;
        while (iterator_column != iterator_line) {
            /*
             * Imprime o elemento não nulo
             */
            printf("%d %d %lf\n", iterator_column->line,
                    iterator_column->column, iterator_column->info);
            iterator_column = iterator_column->right;
        }
    }
    printf("%d\n", 0);
#endif
#endif
    return 0;
}

/*
 * matrix_add --> Executa a soma matricial de m com n, criando em r uma
 *                matrix que guarda esta soma.
 */
int
matrix_add(const SMatrix* m, const SMatrix* n, SMatrix** r, double t)
{
    if ((has_not_been_created(m) == 0) || (has_not_been_created(n) == 0)) {
        fprintf(stderr, "Erro: Uma das matrizes ainda não foi criada.\n");
        return 1;
    }

    if ((m->line != n->line) || (m->column != n->column)) {
        fprintf(stderr,
                "Erro: Dimensões das matrizes precisam ser compatíveis.\n");
        return 1;
    }

    if (matrix_create_base(r) != 2) {
        int r_nl = ((-m->line) > (-n->line)) ? (-m->line) : (-n->line);
        int r_nc = ((-m->column) > (-n->column)) ? (-m->column) : (-n->column);

        if (matrix_create_lines((*r), r_nl) != 0)
            return 1;

        if (matrix_create_columns((*r), r_nc) != 0)
            return 1;
    }

    const SMatrix* tmp1 = m;
    const SMatrix* tmp2 = m;

    while (tmp1->below != m) {
        tmp1 = tmp1->below;
        tmp2 = tmp1;
        while (tmp2->right != tmp1) {
            tmp2 = tmp2->right;
            if (matrix_setelem((*r), tmp2->line, tmp2->column, tmp2->info) == 1)
                return 1;
        }
    }

    tmp1 = n;
    tmp2 = n;
    double info;

    while (tmp1->below != n) {
        tmp1 = tmp1->below;
        tmp2 = tmp1;
        while (tmp2->right != tmp1) {
            tmp2 = tmp2->right;
            matrix_getelem((*r), tmp2->line, tmp2->column, &info);
            if (matrix_setelem(
                        (*r), tmp2->line, tmp2->column, t * tmp2->info + info)
                    == 1)
                return 1;
        }
    }

    return 0;
}

/*
 * matrix_norm --> Encontra a norma da matrix, que é o máximo da soma
 *                  dos elementos de uma linha em  módulo
 */
int
matrix_norm(const SMatrix* m, double* norm)
{
    if (has_not_been_created(m) == 0) {
        fprintf(stderr, "Erro: Sua matriz ainda não foi criada.\n");
        return 1;
    }

    const SMatrix* tmp1 = m;
    const SMatrix* tmp2 = m;
    double currNorm = 0.;

    while (tmp1->below != m) {
        double rowNorm = 0, elem = 0;
        tmp1 = tmp1->below;
        tmp2 = tmp1;
        while (tmp2->right != tmp1) {
            tmp2 = tmp2->right;
            if (matrix_getelem(m, tmp2->line, tmp2->column, &elem) == 1)
                return 1;
            rowNorm += fabs(elem);
        }
        if (rowNorm > currNorm)
            currNorm = rowNorm;
    }
    (*norm) = currNorm;

    return 0;
}

/*
 * matrix_multiply --> Executa a multiplicação matricial de m com n,
 *                     criando em r uma matrix que guarda o resultado.
 */
int
matrix_multiply(const SMatrix* m, const SMatrix* n, SMatrix** r)
{
    if ((has_not_been_created(m) == 0) || (has_not_been_created(n) == 0)) {
        fprintf(stderr, "Erro: Uma das matrizes ainda não foi criada.\n");
        return 1;
    }

    if (m->column != n->line) {
        fprintf(stderr,
                "Erro: Dimensões das matrizes precisam ser compatíveis.\n");
        fprintf(stderr, "m->line --> %i, m->column --> %i \n", m->line,
                m->column);
        fprintf(stderr, "n->line --> %i, n->column --> %i \n", n->line,
                n->column);
        return 1;
    }

    if ((m->info == 0) || (n->info == 0)) {
        // printf("Aviso: Matriz sem elementos. Nada a executar.\n");
        return 0;
    }

    if (matrix_create_base(r) != 2) {
        if (matrix_create_lines((*r), -m->line) != 0)
            return 1;

        if (matrix_create_columns((*r), -n->column) != 0)
            return 1;
    }

    /*
     * Enquanto houverem linhas não nulas em m, é necessário fazer a
     * multiplicação
     */
    const SMatrix* m_line = m;
    while (m_line->below != m) {
        m_line = m_line->below;
        /*
         * Enquanto houverem colunas não nulas em n, é necessário fazer
         * a multiplicação
         */
        const SMatrix* n_column = n;
        while (n_column->right != n) {
            n_column = n_column->right;
            /*
             * sum conterá ao fim do próximo while o produto interno de
             * uma linha de m com uma coluna de n
             */
            double sum = 0.0;

            /*
             * Enquanto houverem colunas na linha atual de m, é necessário
             * fazer a multiplicação
             */
            const SMatrix* n_line = n_column;
            const SMatrix* m_column = m_line;
            while (m_column->right != m_line) {
                m_column = m_column->right;
                /*
                 * Pesquisa na coluna atual da matrix n se existe algum
                 * elemento não nulo na linha correspondente à coluna
                 * da linha atual da matrix m
                 */
                n_line = line_search(n_column, m_column->column);
                if ((n_line != n_column)) {
                    /*
                     * O while abaixo assegura que nenhuma outra chamada de
                     * line_search retornará o mesmo elemento n_line atual
                     */
                    while ((m_column->right != m_line)
                            && (m_column->column < n_line->line))
                        m_column = m_column->right;

                    if (m_column->column == n_line->line)
                        sum += m_column->info * n_line->info;
                }
            }

            if (sum != 0.0)
                if (matrix_setelem((*r), m_line->right->line,
                            n_column->below->column, sum)
                        == 1)
                    return 1;
        }
    }

    return 0;
}

/*
 * matrix_internal_product --> Executa o produto interno entre m e n,
 *                     criando em r uma matrix que guarda o resultado.
 */
int
matrix_internal_product(
        const SMatrix* m, const SMatrix* n, SMatrix** r)
{
    if ((has_not_been_created(m) == 0) || (has_not_been_created(n) == 0)) {
        fprintf(stderr, "Erro: Uma das matrizes ainda não foi criada.\n");
        return 1;
    }

    if ((m->line != n->line) || (m->column != n->column)) {
        fprintf(stderr,
                "Erro: Dimensões das matrizes precisam ser compatíveis.\n");
        fprintf(stderr, "m->line --> %i, m->column --> %i \n", m->line,
                m->column);
        fprintf(stderr, "n->line --> %i, n->column --> %i \n", n->line,
                n->column);
        return 1;
    }

    if ((m->info == 0) || (n->info == 0)) {
        // printf("Aviso: Matriz sem elementos. Nada a executar.\n");
        return 0;
    }

    if (matrix_create_base(r) != 2) {
        if (matrix_create_lines((*r), -m->column) != 0)
            return 1;

        if (matrix_create_columns((*r), -n->column) != 0)
            return 1;
    }

    /*
     * Enquanto houverem colunas não nulas em m, é necessário fazer a
     * multiplicação
     */
    const SMatrix* m_column = m;
    while (m_column->right != m) {
        m_column = m_column->right;
        /*
         * Enquanto houverem colunas não nulas em n, é necessário fazer
         * a multiplicação
         */
        const SMatrix* n_column = n;
        while (n_column->right != n) {
            n_column = n_column->right;
            /*
             * sum conterá ao fim do próximo while o produto interno de
             * uma coluna de m com uma coluna de n
             */
            double sum = 0.0;

            /*
             * Enquanto houverem linhas na coluna atual de m, é necessário
             * fazer a multiplicação
             */
            const SMatrix* n_line = n_column;
            const SMatrix* m_line = m_column;
            while (m_line->below != m_column) {
                m_line = m_line->below;
                /*
                 * Pesquisa na coluna atual da matrix n se existe algum
                 * elemento não nulo na linha correspondente à linha
                 * da coluna atual da matrix m
                 */
                n_line = line_search(n_column, m_line->line);
                if ((n_line != n_column)) {
                    /*
                     * O while abaixo assegura que nenhuma outra chamada de
                     * line_search retornará o mesmo elemento n_line atual
                     */
                    while ((m_line->below != m_column)
                            && (m_line->line < n_line->line))
                        m_line = m_line->below;

                    if (m_line->line == n_line->line)
                        sum += m_line->info * n_line->info;
                }
            }

            if (sum != 0.0)
                if (matrix_setelem((*r), m_column->below->column,
                            n_column->below->column, sum)
                        == 1)
                    return 1;
        }
    }

    return 0;
}

/*
 * matrix_transpose --> Cria em r uma matrix que é a transposta de m
 */
int
matrix_transpose(const SMatrix* m, SMatrix** r)
{
    if (has_not_been_created(m) == 0) {
        fprintf(stderr, "Erro: Sua matriz ainda não foi criada.\n");
        return 1;
    }

    if (matrix_create_base(r) != 2) {
        if (matrix_create_lines((*r), -m->line) != 0)
            return 1;

        if (matrix_create_columns((*r), -m->column) != 0)
            return 1;
    }

    const SMatrix* m_tmp1 = m;
    const SMatrix* m_tmp2 = m;
    while (m_tmp1->below != m) {
        m_tmp1 = m_tmp1->below;
        m_tmp2 = m_tmp1;
        while (m_tmp2->right != m_tmp1) {
            m_tmp2 = m_tmp2->right;
            if (matrix_setelem((*r), m_tmp2->column, m_tmp2->line, m_tmp2->info)
                    == 1)
                return 1;
        }
    }

    return 0;
}

/*
 * matrix_getelem --> Consulta o valor guardado na linha x e coluna j,
 *                    copiando esse valor para elem.
 */
int
matrix_getelem(const SMatrix* m, int x, int y, double* elem)
{
    (*elem) = 0.0;

    if (has_not_been_created(m) == 0)
        return 1;
    if (check_for_dimensions(m, x, y) != 0)
        return 0;

    const SMatrix* m_origin = m;
    m = line_search(m, x);

    if (m->line == x) {
        m_origin = m;
        m = column_search(m, y);

        if (m == m_origin)
            return 0;

        if (m->column == y) {
            (*elem) = m->info;
            return 0;
        }

        return 0;
    }
    else {
        return 0;
    }
}

/*
 * matrix_getelem --> Insere na linha x e coluna y o valor não nulo de
 *                    elem
 */
int
matrix_setelem(SMatrix* m, int x, int y, double elem)
{
    if (has_not_been_created(m) == 0)
        return 1;
    if (check_for_dimensions(m, x, y) != 0)
        return 0;

    if (elem == 0.0) {
        matrix_delelem(m, x, y);
        return 0;
    }

    SMatrix* m_line_tmp = below_line_search(m, x);
    m_line_tmp = m_line_tmp->below;
    SMatrix* m_column_tmp = right_column_search(m, y);
    m_column_tmp = m_column_tmp->right;

    /*
     * Nesse ponto temos os nós origem linha e coluna que conterão ao
     * longo de suas listas o elemento a ser inserido. O campo info é
     * adicionado de 1 indicando a inserção de mais um elemento
     */
    m_line_tmp->info += 1.0;
    m_column_tmp->info += 1.0;
    m->info += 1.0;

    SMatrix* origin = m_line_tmp;
    m_line_tmp = right_column_search(m_line_tmp, y);
    /*
     * Verifica-se agora se o nó cujo campo info deverá conter elem já
     * existe ou não. Caso não, ele é construído.
     */
    if ((m_line_tmp->right == origin) || (m_line_tmp->right->column != y)) {
        SMatrix* new_element = (SMatrix*) malloc(sizeof(SMatrix));
        if (new_element == NULL) {
            fprintf(stderr,
                    "Erro: memória insuficiente. Elemento não inserido, nós "
                    "origem criados.\n");
            m_line_tmp->info -= 1.0;
            m_column_tmp->info -= 1.0;
            m->info -= 1.0;
            return 1;
        }
        new_element->right = m_line_tmp->right;
        m_line_tmp->right = new_element;
        new_element->line = x;
        new_element->column = y;
        new_element->info = 0.0;
    }
    m_line_tmp = m_line_tmp->right;

    origin = m_column_tmp;
    m_column_tmp = below_line_search(m_column_tmp, x);
    /*
     * Verifica-se aqui a posição do novo elemento com relação à coluna.
     * Note que se o elemento foi criado anteriormente, então esse
     * elemento não pertence à lista de elementos da coluna m_column_tmp,
     * e portanto é necessário entrar no loop abaixo. Caso contrário, o
     * elemento já existia anteriormente, então não é necessário modificá-lo.
     * Note também que não é necessária a construção devido ao if anterior.
     */
    if ((m_column_tmp->below == origin) || (m_column_tmp->below->line != x)) {
        m_line_tmp->below = m_column_tmp->below;
        m_column_tmp->below = m_line_tmp;
    }

    /*
     * Nesse ponto tudo o que era necessário ser criado ou verificado já
     * foi feito. Resta apenas alterar o campo info.
     */
    m_line_tmp->info = elem;

    return 0;
}

/*
 * matrix_addelem --> Adiciona na linha x e coluna y o valor não nulo de
 *                    elem
 */
int
matrix_addelem(SMatrix* m, int x, int y, double elem)
{
    if (has_not_been_created(m) == 0)
        return 1;
    if (check_for_dimensions(m, x, y) != 0)
        return 0;

    if (elem == 0.0) {
        matrix_delelem(m, x, y);
        return 0;
    }

    SMatrix* m_line_tmp = below_line_search(m, x);
    m_line_tmp = m_line_tmp->below;
    SMatrix* m_column_tmp = right_column_search(m, y);
    m_column_tmp = m_column_tmp->right;

    /*
     * Nesse ponto temos os nós origem linha e coluna que conterão ao
     * longo de suas listas o elemento a ser inserido. O campo info é
     * adicionado de 1 indicando a inserção de mais um elemento
     */
    m_line_tmp->info += 1.0;
    m_column_tmp->info += 1.0;
    m->info += 1.0;

    SMatrix* origin = m_line_tmp;
    m_line_tmp = right_column_search(m_line_tmp, y);
    /*
     * Verifica-se agora se o nó cujo campo info deverá conter elem já
     * existe ou não. Caso não, ele é construído.
     */
    if ((m_line_tmp->right == origin) || (m_line_tmp->right->column != y)) {
        SMatrix* new_element = (SMatrix*) malloc(sizeof(SMatrix));
        if (new_element == NULL) {
            fprintf(stderr,
                    "Erro: memória insuficiente. Elemento não inserido, nós "
                    "origem criados.\n");
            m_line_tmp->info -= 1.0;
            m_column_tmp->info -= 1.0;
            m->info -= 1.0;
            return 1;
        }
        new_element->right = m_line_tmp->right;
        m_line_tmp->right = new_element;
        new_element->line = x;
        new_element->column = y;
        new_element->info = 0.0;
    }
    m_line_tmp = m_line_tmp->right;

    origin = m_column_tmp;
    m_column_tmp = below_line_search(m_column_tmp, x);
    /*
     * Verifica-se aqui a posição do novo elemento com relação à coluna.
     * Note que se o elemento foi criado anteriormente, então esse
     * elemento não pertence à lista de elementos da coluna m_column_tmp,
     * e portanto é necessário entrar no loop abaixo. Caso contrário, o
     * elemento já existia anteriormente, então não é necessário modificá-lo.
     * Note também que não é necessária a construção devido ao if anterior.
     */
    if ((m_column_tmp->below == origin) || (m_column_tmp->below->line != x)) {
        m_line_tmp->below = m_column_tmp->below;
        m_column_tmp->below = m_line_tmp;
    }

    /*
     * Nesse ponto tudo o que era necessário ser criado ou verificado já
     * foi feito. Resta apenas alterar o campo info.
     */
    m_line_tmp->info += elem;

    return 0;
}

void
matrix_to_NRsparseMat(SMatrix* m, NRsparseMat& m_spr)
{
    m_spr.nrows = -m->line;
    m_spr.ncols = -m->column;
    m_spr.nvals = m->info;
    m_spr.col_ptr.resize(m_spr.ncols + 1);
    m_spr.row_ind.resize(m_spr.nvals);
    m_spr.val.resize(m_spr.nvals);

    for (int j = 0; j < m_spr.ncols; ++j)
        m_spr.col_ptr[j] = 0;
    m_spr.col_ptr[m_spr.ncols] = m_spr.nvals;

    int i = 0;
    /*
     * Enquanto houverem colunas em m, é possível ser necessário
     * fazer o assigment
     */
    const SMatrix* m_column = m;
    while (m_column->right != m) {
        m_column = m_column->right;
        m_spr.col_ptr[m_column->column - 1] = i;

        /*
         * Enquanto houverem linhas não nulas em m, é necessário fazer o
         * assignment
         */
        const SMatrix* m_line = m_column;
        while (m_line->below != m_column) {
            m_line = m_line->below;
            m_spr.row_ind[i] = m_line->line - 1;
            m_spr.val[i] = m_line->info;
            ++i;
        }
    }
}
