#include "ConjugateGradient.h"

#define SHOW_CONVERGENCE_INFO
//#define VERSAO_CALCULO_DIRETO
#define ATEMPT_TO_CHANGE_CGM

ConjugateGradient::ConjugateGradient(std::size_t dimension__)
    : dimension_(dimension__)
    , A_(NULL)
    , b_(NULL)
    , guess_(NULL)
    , sol_(NULL)
    , x__(dimension__, 0.)
{
    matrix_create(&A_, dimension_, dimension_);
    matrix_create(&b_, dimension_, 1);
    matrix_create(&guess_, dimension_, 1);
    matrix_create(&sol_, dimension_, 1);
}

ConjugateGradient::~ConjugateGradient()
{
    matrix_destroy(A_);
    matrix_destroy(b_);
    matrix_destroy(guess_);
    matrix_destroy(sol_);
}

double
ConjugateGradient::A(std::size_t i, std::size_t j)
{
    double value;
    matrix_getelem(A_, i + 1, j + 1, &value);
    return value;
}

void
ConjugateGradient::A(std::size_t i, std::size_t j, double value)
{
    matrix_setelem(A_, i + 1, j + 1, value);
}

void
ConjugateGradient::A_add(std::size_t i, std::size_t j, double value)
{
    matrix_addelem(A_, i + 1, j + 1, value);
}

void
ConjugateGradient::A_empty()
{
    matrix_destroy(A_);
    matrix_create(&A_, dimension_, dimension_);
}

void
ConjugateGradient::A_print()
{
    matrix_print(A_);
}

double
ConjugateGradient::b(std::size_t i)
{
    double value;
    matrix_getelem(b_, i + 1, 1, &value);
    return value;
}

void
ConjugateGradient::b(std::size_t i, double value)
{
    matrix_setelem(b_, i + 1, 1, value);
}

void
ConjugateGradient::b_add(std::size_t i, double value)
{
    matrix_addelem(b_, i + 1, 1, value);
}

void
ConjugateGradient::b_empty()
{
    matrix_destroy(b_);
    matrix_create(&b_, dimension_, 1);
}

void
ConjugateGradient::b_print()
{
    matrix_print(b_);
}

double
ConjugateGradient::guess(std::size_t i)
{
    double value;
    matrix_getelem(guess_, i + 1, 1, &value);
    return value;
}

void
ConjugateGradient::guess(std::size_t i, double value)
{
    matrix_setelem(guess_, i + 1, 1, value);
}

void
ConjugateGradient::guess_print()
{
    matrix_print(guess_);
}

double
ConjugateGradient::sol(std::size_t i)
{
    double value;
    matrix_getelem(sol_, i + 1, 1, &value);
    return value;
}

void
ConjugateGradient::sol_print()
{
    matrix_print(sol_);
}

void
ConjugateGradient::run(std::size_t maxNIter, double tol1, double tol2)
{

#ifdef VERSAO_CALCULO_DIRETO
    SprMtr assigner(NULL), assignerT(NULL), r(NULL);
    SprMtr rAux(NULL), p(NULL), z(NULL), _copy_(NULL);
    SprMtr singleValue1(NULL), singleValue2(NULL);

    matrix_create(&assigner, dimension_, 1);
    matrix_create(&assignerT, 1, dimension_);
    matrix_create(&r, dimension_, 1);
    matrix_create(&rAux, dimension_, 1);
    matrix_create(&p, dimension_, 1);
    matrix_create(&z, dimension_, 1);
    matrix_create(&_copy_, dimension_, 1);
    matrix_create(&singleValue1, 1, 1);
    matrix_create(&singleValue2, 1, 1);

    matrix_add(guess_, assigner, &sol_); // x=guess;
    matrix_multiply(A_, sol_, &rAux);
    matrix_add(b_, rAux, &r, -1); // r=b-A*x;
    matrix_add(r, assigner, &p);  // p=r;
    for (std::size_t i = 0; i < maxNIter; ++i) {
        double sv1, sv2, t, pNorm, rNorm;
        matrix_multiply(A_, p, &z); // z=A*p;
        matrix_internal_product(p, r, &singleValue1);
        matrix_internal_product(p, z, &singleValue2);
        matrix_getelem(singleValue1, 1, 1, &sv1);
        matrix_getelem(singleValue2, 1, 1, &sv2);
        t = sv1 / sv2; // t=p'*r/(p'*z);
        matrix_norm(p, &pNorm);
        if (std::abs(t * pNorm) < std::abs(tol2)) {
#ifdef SHOW_CONVERGENCE_INFO
            std::cerr << "Converged by step tol in " << i << " iterations \n";
#endif
            break;
        }
        matrix_norm(r, &rNorm);
        if (rNorm < std::abs(tol1)) {
#ifdef SHOW_CONVERGENCE_INFO
            std::cerr << "Converged by residue tol in " << i
                      << " iterations \n";
#endif
            break;
        }
        matrix_add(sol_, assigner, &_copy_);
        matrix_add(_copy_, p, &sol_, t); // x=x+t*p;
        matrix_multiply(A_, sol_, &rAux);
        matrix_add(b_, rAux, &r, -1); // r=b-A*x;
        matrix_internal_product(r, z, &singleValue1);
        matrix_getelem(singleValue1, 1, 1, &sv1);
        t = sv1 / sv2; // t=(r'*z/(p'*z));
        matrix_add(p, assigner, &_copy_);
        matrix_add(r, _copy_, &p, -t); // p=r-t*p;
    }
#ifdef SHOW_CONVERGENCE_INFO
    std::cerr << "Did not converge in " << maxNIter << " iterations \n";
#endif
    matrix_destroy(assigner);
    matrix_destroy(assignerT);
    matrix_destroy(r);
    matrix_destroy(rAux);
    matrix_destroy(p);
    matrix_destroy(z);
    matrix_destroy(_copy_);
    matrix_destroy(singleValue1);
    matrix_destroy(singleValue2);
#else

#ifdef ATEMPT_TO_CHANGE_CGM
    std::vector<double> r(dimension_, 0.), p(dimension_, 0.);
    std::vector<double> x(dimension_, 0.), ax(dimension_, 0.);
    NRsparseMat _A_;
    matrix_to_NRsparseMat(A_, _A_);
    double c, d, e, t;

    for (std::size_t i = 0; i < dimension_; ++i) // x=guess;
        x[i] = guess(i);
    ax = _A_.ax(x);
    for (std::size_t i = 0; i < dimension_; ++i) // r=b-A*x;
        r[i] = p[i] = b(i) - ax[i];              // p=r;
    c = 0.0;
    for (std::size_t i = 0; i < dimension_; ++i)
        c += r[i] * r[i]; // c = (r,r);
    for (std::size_t i = 0; i < maxNIter; ++i) {
        std::vector<double> z(_A_.ax(p)); // z=A*p;
        d = 0.0;
        for (std::size_t i = 0; i < dimension_; ++i)
            d += p[i] * z[i];
        t = c / d; // t=c/(p,z);
        e = 0.0;
        for (std::size_t i = 0; i < dimension_; ++i)
            e += p[i] * p[i];
        if (std::sqrt(t * t * e)
                < std::abs(tol2)) // if (t*p,t*p)^0.5 < tol2 then exit loop;
        {
#ifdef SHOW_CONVERGENCE_INFO
            std::cerr << "Converged by step tol in " << i << " iterations \n";
#endif
            break;
        }
        for (std::size_t i = 0; i < dimension_; ++i)
            x[i] += t * p[i]; // x=x+t*p;
        for (std::size_t i = 0; i < dimension_; ++i)
            r[i] -= t * z[i]; // r=r-tz;
        d = 0.0;
        for (std::size_t i = 0; i < dimension_; ++i)
            d += r[i] * r[i];              // d = (r,r);
        if (std::sqrt(d) < std::abs(tol1)) // if (d^0.5 < tol1) then exit loop;
        {
#ifdef SHOW_CONVERGENCE_INFO
            std::cerr << "Converged by residue tol in " << i
                      << " iterations \n";
#endif
            break;
        }
        for (std::size_t i = 0; i < dimension_; ++i)
            p[i] = r[i] + d * p[i] / c; // p=r+(d/c)p;
        c = d;
    }
#ifdef SHOW_CONVERGENCE_INFO
    std::cerr << "Did not converge in " << maxNIter << " iterations \n";
#endif
    for (std::size_t i = 0; i < dimension_; ++i) {
        matrix_setelem(sol_, i + 1, 1, x[i]);
        x__[i] = x[i];
    }
#else
    SprMtr assigner(NULL), assignerT(NULL), r(NULL), rT(NULL);
    SprMtr rAux(NULL), p(NULL), pT(NULL), z(NULL), _copy_(NULL);
    SprMtr singleValue(NULL);
    double c, d, e, t;

    matrix_create(&assigner, dimension_, 1);
    matrix_create(&assignerT, 1, dimension_);
    matrix_create(&r, dimension_, 1);
    matrix_create(&rT, 1, dimension_);
    matrix_create(&rAux, dimension_, 1);
    matrix_create(&p, dimension_, 1);
    matrix_create(&pT, 1, dimension_);
    matrix_create(&z, dimension_, 1);
    matrix_create(&_copy_, dimension_, 1);
    matrix_create(&singleValue, 1, 1);

    matrix_add(guess_, assigner, &sol_); // x=guess;
    matrix_multiply(A_, sol_, &rAux);
    matrix_add(b_, rAux, &r, -1); // r=b-A*x;
    matrix_add(r, assigner, &p);  // p=r;
    matrix_internal_product(r, r, &singleValue);
    matrix_getelem(singleValue, 1, 1, &c); // c = (r,r);
    for (std::size_t i = 0; i < maxNIter; ++i) {
        matrix_multiply(A_, p, &z); // z=A*p;
        matrix_internal_product(p, z, &singleValue);
        matrix_getelem(singleValue, 1, 1, &d);
        t = c / d; // t=c/(p,z);
        matrix_internal_product(p, p, &singleValue);
        matrix_getelem(singleValue, 1, 1, &e);
        if (std::sqrt(t * t * e)
                < std::abs(tol2)) // if (t*p,t*p)^0.5 < tol2 then exit loop;
        {
#ifdef SHOW_CONVERGENCE_INFO
            std::cerr << "Converged by step tol in " << i << " iterations \n";
#endif
            break;
        }
        matrix_add(sol_, assigner, &_copy_);
        matrix_add(_copy_, p, &sol_, t); // x=x+t*p;
        matrix_add(r, assigner, &_copy_);
        matrix_add(_copy_, z, &r, -t); // r=r-tz;
        matrix_internal_product(r, r, &singleValue);
        matrix_getelem(singleValue, 1, 1, &d); // d = (r,r);
        if (std::sqrt(d) < std::abs(tol1)) // if (d^0.5 < tol1) then exit loop;
        {
#ifdef SHOW_CONVERGENCE_INFO
            std::cerr << "Converged by residue tol in " << i
                      << " iterations \n";
#endif
            break;
        }
        matrix_add(p, assigner, &_copy_);
        matrix_add(r, _copy_, &p, d / c); // p=r+(d/c)p;
        c = d;
    }
#ifdef SHOW_CONVERGENCE_INFO
    std::cerr << "Did not converge in " << maxNIter << " iterations \n";
#endif
    matrix_destroy(assigner);
    matrix_destroy(assignerT);
    matrix_destroy(r);
    matrix_destroy(rT);
    matrix_destroy(rAux);
    matrix_destroy(p);
    matrix_destroy(pT);
    matrix_destroy(z);
    matrix_destroy(_copy_);
    matrix_destroy(singleValue);
#endif

#endif

#ifdef SHOW_CONVERGENCE_INFO
    std::cerr << "Left CG ROUTINE\n";
#endif
}
