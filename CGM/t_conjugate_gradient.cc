#include "ConjugateGradient.h"
#include <cmath>
#include <iomanip>
#include <iostream>

int
main()
{
    ConjugateGradient cj(3);

    /*
     * Definição de um sistema linear Ax=b, onde cj.A == A e cj.b = b
     */
    cj.A(0, 0, 2.);
    cj.A(0, 1, -1.5);
    cj.A(0, 2, 0.);
    cj.A(1, 0, 0.);
    cj.A(1, 1, 10.);
    cj.A(1, 2, 1.);
    cj.A_add(1, 2, 15.);
    cj.A(2, 0, 0.);
    cj.A(2, 1, 16.);
    cj.A(2, 2, 1.);
    cj.A_print();

    cj.b(0, 0.5);
    cj.b(1, 6.);
    cj.b_add(1, 20.);
    cj.b(2, 17.);
    cj.b_print();

    cj.run(10, 1.0e-10, 1.0e-10);

    cj.sol_print();

    return 0;
}
