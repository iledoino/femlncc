#ifndef ConjugateGradient_H
#define ConjugateGradient_H

#include "SprMtr.h"
#include <cmath>
#include <iostream>

class ConjugateGradient
{
public:
    ConjugateGradient(std::size_t dimension);
    ~ConjugateGradient();

    SprMtr& A() { return A_; }
    double A(std::size_t i, std::size_t j);
    void A(std::size_t i, std::size_t j, double value);
    void A_add(std::size_t i, std::size_t j, double value);
    void A_empty();
    void A_print();

    SprMtr& b() { return b_; }
    double b(std::size_t i);
    void b(std::size_t i, double value);
    void b_add(std::size_t i, double value);
    void b_empty();
    void b_print();

    SprMtr& guess() { return guess_; }
    double guess(std::size_t i);
    void guess(std::size_t i, double value);
    void guess_print();

    double sol(std::size_t i);
    void sol_print();
    std::vector<double> x__;

    void run(std::size_t maxNIter, double residue_tol, double step_tol);

private:
    std::size_t dimension_;
    SprMtr A_, b_, guess_, sol_;
};

#endif
