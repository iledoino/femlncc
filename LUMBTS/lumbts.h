#include <iostream>
#include <math.h>

#ifndef LUMBTS_H
#define LUMBTS_H

/*
 * This class handles everything related to the linear system which is
 * intended to be solved by LU Method
 */
class LU
{
public:
    /*
     * Dimension of the solution vector
     */
    const int Xdimension;

    /*
     * auxiliar array
     */
    int* pcopy;

    /*
     * Function that does the LU decomposition of a Matrix A stored
     * in M row by row. p is a permutation array, and it has same
     * dimension as the solution vector. zero is a limit to which a
     * value of a pivot from principal diagonal can be. conditionNumber
     * is a variable to hold an upper estimation of the condition
     * number of A.
     */
    int LUdecomposition(
            double* M, int* p, const double& zero, double& conditionNumber);
    /*
     * Function that solves AX=B, where A is stored in M and B is
     * stored in RHS, both row by row. RHSnOfColumns is the number of
     * columns stored in RHS. p is a permutation array, and it has same
     * dimension as the solution vector. Notice that p has to be the
     * same used on LUdecomposition function.
     */
    void LUSolver(double* M, double* RHS, int RHSnOfColumns, int* p);
    /*
     * Function that solves XA=B, where A is stored in M and B is
     * stored in RHS, both row by row. RHSnOfColumns is the number of
     * columns stored in RHS. p is a permutation array, and it has same
     * dimension as the solution vector. Notice that p has to be the
     * same used on LUdecomposition function.
     */
    void
    LUSolver_TransposeSystem(double* M, double* RHS, int RHSnOfColumns, int* p);
    /*
     * Functions used to multiply the permutation array by a matrix
     */
    void PermutationProduct(double* RHS, int RHSnOfColumns, int* p);
    void
    PermutationProduct_TransposeSystem(double* RHS, int RHSnOfColumns, int* p);
    /*
     * Constructor
     */
    LU(const int& Xdimension_);
    /*
     * Destructor
     */
    ~LU();
    /*
     * Function for identification
     */
    static std::string About()
    {
        return "***************************************************************"
               "*****************\n                                 LU SOLVER  "
               "                                   "
               "\n*************************************************************"
               "*******************\n\nCopyright: Ismael de Souza Ledoino "
               "<ismael.sledoino@gmail.com>\n**********************************"
               "**********************************************\n\n";
    }
};

/*
 * This class handles everything related to the blocktridiagonal system
 */
class LUMBTS
{
private:
    /*
     * class used to solve the internal linear systems
     */
    LU fullLU;

    /*
     * variable that holds an upper limit for the conditionNumber
     */
    double conditionNumber, conditionNumberLimit;

    /*
     * variables to specify the number of blocks and the block's dimension
     */
    int nOfBlocks, blockDimension, blockSize;

    /*
     * Auxiliar Functions
     */
    void AddMatrixMatrixProduct(
            double* SolutionB, double* M_L, double* M_R, const double& ct);
    void AddMatrixVectorProduct(
            double* SolutionV, double* M_L, double* V_R, const double& ct);

public:
    /*
     * Pointer to hold the permutation array
     */
    int* ipvi;

    /*
     * Pointer to function that states which matrix product funtion
     * to use
     */
    typedef void (LUMBTS::*MatrixProductFunction)(
            double*, double*, double*, const double&);
    MatrixProductFunction AddMatrixProduct[2];

    /*
     * Function to decrease the number of blocks, in case it is necessary
     */
    void NOfBlocks(const int& nOfBlocks_)
    {
        if (nOfBlocks_ <= nOfBlocks) {
            nOfBlocks = nOfBlocks_;
        }
        else {
            std::cerr << "It is not possible to increase the number of blocks"
                      << std::endl;
        }
    }

    /*
     * This function does the block lu decomposition, that is, if A
     * is a square matrix, then it makes A into LU, where L is block
     * lower diagonal and U is block upper diagonal
     */
    int BlockLUDecomposition(double* D, double* L, double* U);

    /*
     * This function does the Forward substitution necessary to solve
     * the system LY=B. The integer defines whether the RHS array is
     * filled with square blocks or vectors (options 0 or 1). The
     * square blocks have to have dimension blockDimensionxblockDimension
     */
    void BlockForwardSubstitution(double* L, double* RHS, int kindOfRHS = 0);

    /*
     * This function does the Backward substitution necessary to solve
     * the system UX=Y, where X is the final solution. The integer
     * defines whether the RHS array is  filled with square blocks
     * or vectors (options 0 or 1). The  square blocks have to have
     * dimension blockDimensionxblockDimension
     */
    void BlockBackwardSubstitution(
            double* D, double* U, double* RHS, int kindOfRHS = 0);

    /*
     * Function that solves the linear system, returning 1 in case
     * it was succesfully solved
     */
    int Solve(double* D, double* L, double* U, double* RHS);

    /*
     * Constructor
     */
    LUMBTS(const int& nOfBlocks_, const int& blockDimension_,
            const double& conditionNumberLimit_);

    /*
     * Destructor
     */
    ~LUMBTS();

    /*
     * Function for identification
     */
    static std::string About()
    {
        return "***************************************************************"
               "*****************\n              Block LU           Method for "
               "Block Tridiagonal Systems          "
               "\n*************************************************************"
               "*******************\n\nCopyright: Ismael de Souza Ledoino "
               "<ismael.sledoino@gmail.com>\n**********************************"
               "**********************************************\n\n";
    }
};

#endif
