#include "lumbts.h"

/*
 * Constructor
 */
LU::LU(const int& Xdimension_) : Xdimension(Xdimension_)
{
    pcopy = new int[Xdimension];
}

/*
 * Destructor
 */
LU::~LU()
{
    delete[] pcopy;
}

int
LU::LUdecomposition(
        double* M, int* p, const double& zero, double& conditionNumber)
{
    int t, aux;
    double tmp;

    for (int i = 0; i != Xdimension; ++i)
        p[i] = i;

    for (int i = 0, iM = 0; i < Xdimension - 1; i++, iM += Xdimension) {
        t = i;
        for (int j = i + 1, jM = (i + 1) * Xdimension; j < Xdimension;
                ++j, jM += Xdimension)
            if (fabs(M[t * Xdimension + i]) < fabs(M[jM + i]))
                t = j;

        if (t != i) {
            aux = p[i];
            p[i] = p[t];
            p[t] = aux;

            t = t * Xdimension;
            for (int j = 0; j != Xdimension; ++j) {
                tmp = M[iM + j];
                M[iM + j] = M[t + j];
                M[t + j] = tmp;
            }
        }

        if ((M[iM + i] > zero) || (-M[iM + i] > zero)) {
            conditionNumber /= M[iM + i];

            for (int j = i + 1, jM = (i + 1) * Xdimension; j < Xdimension;
                    j++, jM += Xdimension) {
                M[jM + i] = M[jM + i] / M[iM + i];
                for (t = i + 1; t < Xdimension; t++) {
                    M[jM + t] -= M[jM + i] * M[iM + t];
                }
            }
        }
        else {
            conditionNumber = 1.0;
            std::cerr << "Sorry, one of the pivots from the diagonal blocks "
                         "does not have norm greater than "
                      << zero << "." << std::endl;
            return 0;
        }
    }

    return 1;
}

void
LU::LUSolver(double* M, double* RHS, int RHSnOfColumns, int* p)
{
    double aux;

    PermutationProduct(RHS, RHSnOfColumns, p);

    for (int i = 1, iM = Xdimension, iRHS = RHSnOfColumns;
            iM != Xdimension * Xdimension;
            iM += Xdimension, iRHS += RHSnOfColumns, ++i) {
        for (int jRHS = 0, jM = 0; jM != i; jRHS += RHSnOfColumns, ++jM) {
            aux = M[iM + jM];
            for (int t = 0; t < RHSnOfColumns; t++)
                RHS[iRHS + t] -= aux * RHS[jRHS + t];
        }
    }

    for (int i = (Xdimension - 1), iM = (Xdimension - 1) * Xdimension,
             iRHS = (Xdimension - 1) * RHSnOfColumns;
            iM >= 0; iM -= Xdimension, iRHS -= RHSnOfColumns, --i) {
        for (int jM = (Xdimension - 1), jRHS = (Xdimension - 1) * RHSnOfColumns;
                jM > i; --jM, jRHS -= RHSnOfColumns) {
            aux = M[iM + jM];
            for (int t = 0; t < RHSnOfColumns; ++t)
                RHS[iRHS + t] -= aux * RHS[jRHS + t];
        }

        for (int t = 0; t < RHSnOfColumns; ++t) {
            RHS[iRHS + t] /= M[iM + i];
        }
    }
}

void
LU::LUSolver_TransposeSystem(double* M, double* RHS, int RHSnOfColumns, int* p)
{
    double aux;

    for (int i = 0; i < Xdimension; i++) {
        for (int j = i - 1, jM = (i - 1) * Xdimension; j >= 0;
                --j, jM -= Xdimension) {
            aux = M[jM + i];
            for (int tRHS = 0; tRHS < RHSnOfColumns * Xdimension;
                    tRHS += Xdimension)
                RHS[tRHS + i] -= aux * RHS[tRHS + j];
        }

        aux = 1.0 / M[i * Xdimension + i];
        for (int tRHS = 0; tRHS < RHSnOfColumns * Xdimension;
                tRHS += Xdimension)
            RHS[tRHS + i] *= aux;
    }

    for (int i = Xdimension - 2; i >= 0; i--) {
        for (int j = i + 1, jM = (i + 1) * Xdimension; j < Xdimension;
                j++, jM += Xdimension) {
            aux = M[jM + i];
            for (int tRHS = 0; tRHS < RHSnOfColumns * Xdimension;
                    tRHS += Xdimension) {
                RHS[tRHS + i] -= aux * RHS[tRHS + j];
            }
        }
    }

    PermutationProduct_TransposeSystem(RHS, RHSnOfColumns, p);
}

void
LU::PermutationProduct(double* RHS, int RHSnOfColumns, int* p)
{
    double tmp;

    for (int i = 0; i < Xdimension; i++)
        pcopy[i] = p[i];

    for (int i = 0; i < Xdimension - 1; i++)
        if (pcopy[i] != i) {
            for (int j = 0; j < RHSnOfColumns; j++) {
                tmp = RHS[i * RHSnOfColumns + j];
                RHS[i * RHSnOfColumns + j] = RHS[pcopy[i] * RHSnOfColumns + j];
                RHS[pcopy[i] * RHSnOfColumns + j] = tmp;
            }
            for (int j = i; j < Xdimension; j++)
                if (pcopy[j] == i) {
                    pcopy[j] = pcopy[i];
                    j = Xdimension;
                }
        }
}

void
LU::PermutationProduct_TransposeSystem(double* RHS, int RHSnOfColumns, int* p)
{
    double aux;

    for (int i = 0; i < Xdimension; i++)
        pcopy[p[i]] = i;

    for (int i = 0; i < RHSnOfColumns - 1; i++)
        if (pcopy[i] != i) {
            for (int j = 0; j < Xdimension; j++) {
                aux = RHS[j * Xdimension + i];
                RHS[j * Xdimension + i] = RHS[j * Xdimension + pcopy[i]];
                RHS[j * Xdimension + pcopy[i]] = aux;
            }
            for (int j = i; j < Xdimension; j++)
                if (pcopy[j] == i) {
                    pcopy[j] = pcopy[i];
                    j = Xdimension;
                }
        }
}

/*
 * Constructor
 */
LUMBTS::LUMBTS(const int& nOfBlocks_, const int& blockDimension_,
        const double& conditionNumberLimit_)
    : fullLU(blockDimension_)
    , conditionNumberLimit(conditionNumberLimit_)
    , nOfBlocks(nOfBlocks_)
    , blockDimension(blockDimension_)
    , blockSize(blockDimension_ * blockDimension_)
{
    AddMatrixProduct[0] = &LUMBTS::AddMatrixVectorProduct;
    AddMatrixProduct[1] = &LUMBTS::AddMatrixMatrixProduct;

    ipvi = new int[blockDimension * nOfBlocks];
}

/*
 * Destructor
 */
LUMBTS::~LUMBTS()
{
    delete[] ipvi;
}

void
LUMBTS::AddMatrixMatrixProduct(
        double* SolutionB, double* M_L, double* M_R, const double& ct)
{
    double tmp;

    for (int t = 0; t != blockSize; t += blockDimension)
        for (int i = 0, k = 0; i != blockDimension; ++i, k += blockDimension) {
            tmp = ct * M_L[t + i];
            for (int j = 0; j != blockDimension; ++j)
                SolutionB[t + j] += tmp * M_R[k + j];
        }
}

void
LUMBTS::AddMatrixVectorProduct(
        double* SolutionV, double* M_L, double* V_R, const double& ct)
{
    for (int i = 0, k = 0; i < blockDimension; i++, k += blockDimension)
        for (int j = 0; j < blockDimension; j++)
            SolutionV[i] += ct * M_L[k + j] * V_R[j];
}

int
LUMBTS::BlockLUDecomposition(double* D, double* L, double* U)
{
    conditionNumber = 1.0;

    for (int i = 0, iN = blockSize, j = 0; i < blockSize * (nOfBlocks - 1);
            i = iN, iN += blockSize, j += blockDimension) {
        // trecho que encontra o bloco \overline{B}_i = B_i
        // (\overline{D}_i)^{-1}
        if (fullLU.LUdecomposition(D + i, ipvi + j, 1.0e-16, conditionNumber)
                != 1) {
            std::cout << "Exiting LUMBTS" << std::endl;
            return 0;
        }
        fullLU.LUSolver_TransposeSystem(D + i, L + i, blockDimension, ipvi + j);

        // encontra \overline{D}_{i+1}, dado por \overline{D}_i =
        // D_i-\overline{B}_{i}*\overline{P}_{i}
        AddMatrixMatrixProduct(D + iN, L + i, U + i, -1.0);
    }
    if (fullLU.LUdecomposition(D + blockSize * (nOfBlocks - 1),
                ipvi + blockDimension * (nOfBlocks - 1), 1.0e-16,
                conditionNumber)
            != 1) {
        std::cout << "Exiting LUMBTS" << std::endl;
        return 0;
    }

    if (conditionNumber < conditionNumberLimit) {
        return 1;
    }
    std::cerr << "The condition number of the matrix is greater than the value "
                 "allowed."
              << std::endl;
    return -1;
}

void
LUMBTS::BlockForwardSubstitution(double* L, double* RHS, int kindOfRHS)
{
    for (int i = 0, iN = blockDimension, j = 0;
            i < blockDimension * (nOfBlocks - 1);
            i = iN, iN += blockDimension, j += blockSize) {
        // b_{i+1} = b_{i+1} - B_i*b_i
        (this->*AddMatrixProduct[kindOfRHS])(RHS + iN, L + j, RHS + i, -1.0);
    }
}

void
LUMBTS::BlockBackwardSubstitution(
        double* D, double* U, double* RHS, int kindOfRHS)
{
    // Y_M = (D_M)^{-1}*Y_M
    fullLU.LUSolver(D + blockSize * (nOfBlocks - 1),
            RHS + blockDimension * (nOfBlocks - 1), 1,
            ipvi + blockDimension * (nOfBlocks - 1));

    for (int i = blockDimension * (nOfBlocks - 2),
             iB = blockSize * (nOfBlocks - 2);
            i >= 0; i -= blockDimension, iB -= blockSize) {
        // Y_{i-1} = Y_{i-1} - S_i*Y_i
        (this->*AddMatrixProduct[kindOfRHS])(
                RHS + i, U + iB, RHS + i + blockDimension, -1.0);

        // Y_{i-1} = (D_{i-1})^{-1}*Y_{i-1}
        fullLU.LUSolver(D + iB, RHS + i, 1, ipvi + i);
    }
}

int
LUMBTS::Solve(double* D, double* L, double* U, double* RHS)
{
    int i = BlockLUDecomposition(D, L, U);

    if (i == 1) {
        BlockForwardSubstitution(L, RHS);
        BlockBackwardSubstitution(D, U, RHS);
    }

    return i;
}
