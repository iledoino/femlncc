#include "GLQuadRule.h"
#include "GLQuadRule2D.h"
#include "GLTriangRule.h"
#include "QuadLinearInterp.h"
#include "RigidityMatrix.h"
#include "TriangLinearInterp.h"
#include <iomanip>
#include <iostream>
#include <string>

//**********************************************************************
//                           TESTES DE ESTRUTURA
//**********************************************************************
void
test1()
{
    QuadLinearInterp2D interp;

    interp.setUp({ 0, 0 }, { 2, 0 }, { 2, 2 }, { 0, 2 });

    std::cout << "P1 --> " << interp.P1() << std::endl;
    std::cout << "P2 --> " << interp.P2() << std::endl;
    std::cout << "P3 --> " << interp.P3() << std::endl;
    std::cout << "P4 --> " << interp.P4() << std::endl;

    std::cout << std::endl;

    std::size_t gridSize = 5;
    std::cout << "Imprimindo grade " << gridSize << "x" << gridSize << ":"
              << std::endl;
    double stepSize = 2. / (gridSize - 1.);
    typename QuadLinearInterp2D::LocalPoint localPoint;
    for (std::size_t i = 0; i < gridSize; ++i) {
        for (std::size_t j = 0; j < gridSize; ++j) {
            localPoint = { -1. + (double) (i) *stepSize,
                -1. + (double) (j) *stepSize };
            std::cout << interp.globalPoint(localPoint) << " ";
            // std::cout << localPoint << " ";
        }
        std::cout << std::endl;
    }
}

double
f(double x)
{
    return 2. * x * x * x - 3. * x * x + 0.5 * x + 10.;
}

double
fl(double x)
{
    return 6. * x * x - 6. * x + 0.5;
}

void
test2()
{
    const std::size_t N = 64;
    double realIntegral = f(1.) - f(-1.);
    double numericIntegral = 0.;
    GLQuadRule<N> glq;
    for (std::size_t i = 0; i < N; ++i)
        numericIntegral += fl(glq.point(i)) * glq.weight(i);
    std::cout << "Integral real --> " << realIntegral << std::endl;
    std::cout << "Integral numerica --> " << numericIntegral << std::endl;
    std::cout << "Diferenca entre integral real e numerica: "
              << realIntegral - numericIntegral << std::endl;
}

void
test3()
{
    TriangLinearInterp interp;

    interp.setUp({ -2, 0 }, { 2, 0 }, { 0, 2 });

    std::cout << "P1 --> " << interp.P1() << std::endl;
    std::cout << "P2 --> " << interp.P2() << std::endl;
    std::cout << "P3 --> " << interp.P3() << std::endl;

    std::cout << std::endl;

    typename TriangLinearInterp::LocalPoint localPoint;

    localPoint = { 0., 0. };
    std::cout << localPoint << " --> " << interp.globalPoint(localPoint)
              << std::endl;
    localPoint = { 0., 1. };
    std::cout << localPoint << " --> " << interp.globalPoint(localPoint)
              << std::endl;
    localPoint = { 1., 0. };
    std::cout << localPoint << " --> " << interp.globalPoint(localPoint)
              << std::endl;
    localPoint = { .5, .5 };
    std::cout << localPoint << " --> " << interp.globalPoint(localPoint)
              << std::endl;
    localPoint = { .5, 0. };
    std::cout << localPoint << " --> " << interp.globalPoint(localPoint)
              << std::endl;
    localPoint = { 0., .5 };
    std::cout << localPoint << " --> " << interp.globalPoint(localPoint)
              << std::endl;
}

double
g(double x, double y)
{
    return (2. * x * x - x + 1.) * (y * y - 10 * y - 3.);
}

double
gx(double x)
{
    return (2. * x * x * x / 3. - 0.5 * x * x + x);
}

double
gy(double y)
{
    return (y * y * y / 3. - 5 * y * y - 3. * y);
}

void
test4()
{
    const std::size_t N = 2;
    double realIntegral = (gx(1.) - gx(-1.)) * (gy(1.) - gy(-1.));
    double numericIntegral = 0.;
    GLQuadRule2D<N, N> glq;
    for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < N; ++j) {
            Vector<2> vector(glq.point(i, j));
            numericIntegral += g(vector[0], vector[1]) * glq.weight(i, j);
        }
    std::cout << "Integral real --> " << realIntegral << std::endl;
    std::cout << "Integral numerica --> " << numericIntegral << std::endl;
    std::cout << "Diferenca entre integral real e numerica: "
              << realIntegral - numericIntegral << std::endl;

    realIntegral = -3.005555556;
    numericIntegral = 0.;
    GLTriangRule<N, N> glq2;
    for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < N; ++j) {
            Vector<2> vector(glq2.point(i, j));
            numericIntegral += g(vector[0], vector[1]) * glq2.weight(i, j);
        }
    std::cout << "Integral real --> " << realIntegral << std::endl;
    std::cout << "Integral numerica --> " << numericIntegral << std::endl;
    std::cout << "Diferenca entre integral real e numerica: "
              << realIntegral - numericIntegral << std::endl;
}

//**********************************************************************
