

//**********************************************************************
//                           TESTE EM 1D
//**********************************************************************

/*
 * Definição da grade de elementos em 1D
 */
class Grid1D : public Grid<1>
{
public:
    typedef Grid<1>::InteriorElement InteriorElement;
    typedef Grid<1>::BoundaryElement BoundaryElement;

    Grid1D(const double& xL, const double& xR, const std::size_t& nElements);

    const std::list<InteriorElement>& interior() const override;
    const std::list<BoundaryElement>& boundary() const override;
    std::size_t size() const override;

private:
    const double &xL_, &xR_;
    const std::size_t& nElements_;
    mutable std::list<InteriorElement> interior_;
    mutable std::list<BoundaryElement> boundary_;
};

Grid1D::Grid1D(
        const double& xL__, const double& xR__, const std::size_t& nElements__)
    : Grid<1>()
    , xL_(xL__)
    , xR_(xR__)
    , nElements_(nElements__)
    , interior_()
    , boundary_()
{
}

const std::list<Grid<1>::InteriorElement>&
Grid1D::interior() const
{
    interior_.clear();

    double dx((xR_ - xL_) / nElements_);

    for (std::size_t i = 0; i < nElements_; i++) {
        QuadElement<1>::Point p0({ xL_ + static_cast<double>(i) * dx }),
                p1({ p0[0] + dx });
        QuadElement<1> quadElement({ p0, p1 });
        quadElement.globalIndices() = { i, i + 1 };

        interior_.push_back(quadElement);
    }

    return interior_;
}

const std::list<Grid<1>::BoundaryElement>&
Grid1D::boundary() const
{
    boundary_.clear();

    QuadElementBdry<1>::Point pL({ xL_ });
    QuadElementBdry<1> quadElementL({ pL });
    quadElementL.globalIndices() = { 0 };
    BoundaryElement bdryL;
    bdryL.elem = quadElementL;
    bdryL.bdryType = -1;

    QuadElementBdry<1>::Point pR({ xR_ });
    QuadElementBdry<1> quadElementR({ pR });
    quadElementR.globalIndices() = { nElements_ };
    BoundaryElement bdryR;
    bdryR.elem = quadElementR;
    bdryR.bdryType = 1;

    boundary_.push_back(bdryL);
    boundary_.push_back(bdryR);

    return boundary_;
}

std::size_t
Grid1D::size() const
{
    return nElements_ + 1;
}

/*
 * Definição do operator Laplaciano em 1D (forma variacional)
 */
class BilFormFunc : public BilinearFormFunction<1>
{
public:
    typedef Vector<1> LocalPointN;
    typedef Vector<0> LocalPointNm1;

    BilFormFunc(QuadLinearInterp1D& interp, StdBdryElem1D& interpBdry);

    double evaluate_interior(std::size_t i, std::size_t j,
            const LocalPointN& localPoint) override;
    double evaluate_boundary(std::size_t i, std::size_t j,
            const LocalPointNm1& localPoint) override;

    double gamma(const LocalPointNm1& localPoint);

    bool is_symmetric() const override { return true; }
};

BilFormFunc::BilFormFunc(
        QuadLinearInterp1D& interp__, StdBdryElem1D& interpBdry__)
    : BilinearFormFunction<1>(interp__, interpBdry__)
{
}

double
BilFormFunc::evaluate_interior(
        std::size_t i, std::size_t j, const LocalPointN& localPoint)
{
    /*
     * Implementação do operador laplaciano em sua forma variacional,
     * a saber: grad \phi _i ^T * grad \phi _j
     */
    return stdElemInterp().baseGradient(i, localPoint)[0]
            * stdElemInterp().baseGradient(j, localPoint)[0];
}

double
BilFormFunc::evaluate_boundary(
        std::size_t i, std::size_t j, const LocalPointNm1& localPoint)
{
    /*
     * Implementação da condição de fronteira de Robin
     */
    return gamma(localPoint) * stdBdryElemInterp().baseFunction(i, localPoint)
            * stdBdryElemInterp().baseFunction(j, localPoint);
}

double
BilFormFunc::gamma(const LocalPointNm1& localPoint)
{
    return 1.;
}

/*
 * Definição do termo de fonte em 1D (forma variacional)
 */
class LinearFormFunc : public LinearFormFunction<1>
{
public:
    typedef Vector<1> LocalPointN;
    typedef Vector<0> LocalPointNm1;

    LinearFormFunc(QuadLinearInterp1D& interp, StdBdryElem1D& interpBdry);

    double
    evaluate_interior(std::size_t i, const LocalPointN& localPoint) override;
    double
    evaluate_boundary(std::size_t i, const LocalPointNm1& localPoint) override;

    double f(const LocalPointN& localPoint);
    double g(const LocalPointNm1& localPoint);
};

LinearFormFunc::LinearFormFunc(
        QuadLinearInterp1D& interp__, StdBdryElem1D& interpBdry__)
    : LinearFormFunction<1>(interp__, interpBdry__)
{
}

double
LinearFormFunc::evaluate_interior(std::size_t i, const LocalPointN& localPoint)
{
    /*
     * Implementação do termo de reação em sua forma variacional,
     * a saber: \phi _i * f
     */
    return stdElemInterp().baseFunction(i, localPoint) * f(localPoint);
}

double
LinearFormFunc::evaluate_boundary(
        std::size_t i, const LocalPointNm1& localPoint)
{
    /*
     * Implementação do termo referente à condição de fronteira de Robin,
     * a saber: \phi _i * g
     */
    return stdBdryElemInterp().baseFunction(i, localPoint) * g(localPoint);
}

double
LinearFormFunc::f(const LocalPointN& localPoint)
{
    double x = stdElemInterp().globalPoint(localPoint)[0];

    return -6. * x;
}

double
LinearFormFunc::g(const LocalPointNm1& localPoint)
{
    double x = stdBdryElemInterp().globalPoint(localPoint)[0];

    return x * x * (3. + x);
}

/*
 * Programa principal
 */
int
test1D(void)
{
    /*
     * Criação de um Grid1D
     */
    double xL = 0.;             // limite inferior do domínio
    double xR = 01.;            // limite superior do domínio
    std::size_t nElements = 10; // número desejado de elementos

    Grid1D grid1D(xL, xR, nElements); // grade de elementos

    /*
     * Criação de uma ElemMatrix<1>
     */
    QuadLinearInterp1D quadLinearInterp1D; // interpolante em 1D para elemento
                                           // de referência
    StdBdryElem1D
            stdBdryElem1D; // interpolante em 1D para elementos-fronteira de
                           // referência (nesse caso não há interpolação de
                           // fato, pois a fronteira é apenas um ponto. Contudo,
                           // o código exige o fornecimento de tal classe, mesmo
                           // que ela não seja usada)
    BilFormFunc bilFormFunc(quadLinearInterp1D, // forma bilinear
            stdBdryElem1D);
    LinearFormFunc linearFormFunc(quadLinearInterp1D, // forma linear
            stdBdryElem1D);
    GLQuadRule<2> glQuadRule; // regra de integração em 1D
    GLRule0D glRule0D; // regra de integração em 0D (mais uma vez, esta classe é
                       // necessária apenas para que o código pudesse ser
                       // generalizado. Ela não será de fato usada)

    ElemMatrix<1> elemMatrix(bilFormFunc, linearFormFunc, glQuadRule,
            glRule0D); // matrix de elemento, bem como termo de carga de
                       // elemento

    /*
     * Criação de uma RigidityMatrix<1>
     */
    RigidityMatrix<1> rigidityMatrix(
            grid1D, elemMatrix); // matriz de rigidez, bem como termo de carga

    std::vector<double> matrix, rhs;
    rigidityMatrix.get(matrix, rhs);

    std::size_t nRows = rhs.size(), nColumns = rhs.size();
    std::cout << "Impressão da matriz de rigidez" << std::endl;
    for (std::size_t i = 0; i < nRows; ++i) {
        std::cout << "[ ";
        for (std::size_t j = 0; j < nColumns - 1; ++j) {
            std::cout << matrix[i * nRows + j] << ", ";
        }
        std::cout << matrix[i * nRows + nColumns - 1] << " ]" << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Impressão do termo de carga" << std::endl;
    for (std::size_t i = 0; i < nRows; ++i) {
        std::cout << "[ " << rhs[i] << " ]" << std::endl;
    }
    std::cout << std::endl;

    LU lu(nRows);
    std::vector<int> perm(nRows, 0.);
    double zero = 1.0e-16, invConditionNumber = 1.0;
    if (lu.LUdecomposition(
                matrix.data(), perm.data(), zero, invConditionNumber)) {
        lu.LUSolver(matrix.data(), rhs.data(), 1, perm.data());
        std::cout << "Inverse of the Condition Number: " << invConditionNumber
                  << "\n"
                  << std::endl;
        std::cout << "Impressão da solução" << std::endl;
        for (std::size_t i = 0; i < nRows; ++i) {
            std::cout << "[ " << rhs[i] << " ]" << std::endl;
        }
        std::cout << std::endl;

        std::cout << "Impressão da norma do erro" << std::endl;
        for (std::size_t i = 0; i < nRows; ++i) {
            double x = static_cast<double>(i) / static_cast<double>(nRows - 1);
            std::cout << "[ " << std::abs(x * x * x - rhs[i]) << " ]"
                      << std::endl;
        }
        std::cout << std::endl;
    }

    return 1;
}

//**********************************************************************
