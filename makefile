CCOMPILER = gcc
CPPOMPILER = g++
CFLAGS   = -I./LUMBTS/ -I./SPRMTR/ -I./CGM/ -I./SRC -Wall -Wextra -lm -std=gnu++11
OBJFOLDER = ./OBJ

UTIL = ./SRC/LinearAlgebra.cc ./SRC/LinearAlgebra.h
UTIL_CC = ./SRC/LinearAlgebra.cc
ELEMENT = ./SRC/Element.h ./SRC/ElemMatrix.cc ./SRC/Grid.h
ELEMENT_CC = ./SRC/ElemMatrix.cc
TWODGRID = ./SRC/TwoDimensionalGrid.cc ./SRC/TwoDimensionalGrid.h
TWODGRID_CC = ./SRC/TwoDimensionalGrid.cc
VARIATIONAL = ./SRC/BilinearFormFunction.h ./SRC/LinearFormFunction.h  ./SRC/ElemMatrix.h
GLRULES = ./SRC/GLRule.cc ./SRC/GLRule.h ./SRC/GLQuadRule.cc ./SRC/GLQuadRule.h ./SRC/GLQuadRule2D.h ./SRC/GLTriangRule.h
GLRULES_CC1 = ./SRC/GLRule.cc
GLRULES_CC2 = ./SRC/GLQuadRule.cc
ELMINTERP = ./SRC/StdElemInterp.h ./SRC/QuadLinearInterp.cc ./SRC/QuadLinearInterp.h ./SRC/TriangLinearInterp.cc ./SRC/TriangLinearInterp.h
ELMINTERP_CC1 = ./SRC/QuadLinearInterp.cc
ELMINTERP_CC2 = ./SRC/TriangLinearInterp.cc
BDRYELMINTERP = ./SRC/StdBdryElemInterp.h ./SRC/StdBdryElem1D.cc ./SRC/StdBdryElem1D.h ./SRC/StdBdryElem2D.cc ./SRC/StdBdryElem2D.h
BDRYELMINTERP_CC1 = ./SRC/StdBdryElem1D.cc
BDRYELMINTERP_CC2 = ./SRC/StdBdryElem2D.cc
RIGIDITY = ./SRC/RigidityMatrix.cc ./SRC/RigidityMatrix.h
RIGIDITY_CC = ./SRC/RigidityMatrix.cc
LUMBTS = ./LUMBTS/luMethodForBlockTridiagonalSystems.cc ./LUMBTS/lumbts.h
LUMBTS_CC = ./LUMBTS/luMethodForBlockTridiagonalSystems.cc
SPRMTR = ./SPRMTR/matrix.cc ./SPRMTR/matrix.h ./SPRMTR/SprMtr.h
SPRMTR_CC = ./SPRMTR/matrix.cc
CGM = ./CGM/ConjugateGradient.cc ./CGM/ConjugateGradient.h
CGM_CC = ./CGM/ConjugateGradient.cc
EASYMESH = ./EASYMESH/easymesh_1_4.c
COBJ = $(OBJFOLDER)/util.o $(OBJFOLDER)/element.o $(OBJFOLDER)/twodgrid.o $(OBJFOLDER)/glrules1.o $(OBJFOLDER)/glrules2.o $(OBJFOLDER)/elminterp1.o $(OBJFOLDER)/elminterp2.o $(OBJFOLDER)/bdryelminterp1.o $(OBJFOLDER)/bdryelminterp2.o $(OBJFOLDER)/rigidity.o $(OBJFOLDER)/lumbts.o $(OBJFOLDER)/sprmtr.o $(OBJFOLDER)/cgm.o
DEPENDENCY = $(COBJ) $(VARIATIONAL) #./EASYMESH/easymesh

default: main

main: main.cc $(DEPENDENCY)
	$(CPPOMPILER) $(CFLAGS) -o main main.cc ./SRC/TwoDimensionalProblem.cc ./TOY/toy.cc ./TESTS/structure.cc $(COBJ)

dr: main.cc $(DEPENDENCY)
	$(CPPOMPILER) $(CFLAGS) -o main main.cc ./SRC/TwoDimensionalProblem.cc ./DR/dr.cc ./TESTS/structure.cc $(COBJ)

cdr: main.cc $(DEPENDENCY)
	$(CPPOMPILER) $(CFLAGS) -o main main.cc ./SRC/TwoDimensionalProblem.cc ./CDR/cdr.cc ./TESTS/structure.cc $(COBJ)

$(OBJFOLDER)/util.o: $(UTIL)
	$(CPPOMPILER) $(CFLAGS) -c $(UTIL_CC) -o $(OBJFOLDER)/util.o

$(OBJFOLDER)/element.o: $(ELEMENT)
	$(CPPOMPILER) $(CFLAGS) -c $(ELEMENT_CC) -o $(OBJFOLDER)/element.o

$(OBJFOLDER)/twodgrid.o: $(TWODGRID)
	$(CPPOMPILER) $(CFLAGS) -c $(TWODGRID_CC) -o $(OBJFOLDER)/twodgrid.o

$(OBJFOLDER)/glrules1.o: $(GLRULES)
	$(CPPOMPILER) $(CFLAGS) -c $(GLRULES_CC1) -o $(OBJFOLDER)/glrules1.o

$(OBJFOLDER)/glrules2.o: $(GLRULES)
	$(CPPOMPILER) $(CFLAGS) -c $(GLRULES_CC2) -o $(OBJFOLDER)/glrules2.o

$(OBJFOLDER)/elminterp1.o: $(ELMINTERP)
	$(CPPOMPILER) $(CFLAGS) -c $(ELMINTERP_CC1) -o $(OBJFOLDER)/elminterp1.o

$(OBJFOLDER)/elminterp2.o: $(ELMINTERP)
	$(CPPOMPILER) $(CFLAGS) -c $(ELMINTERP_CC2) -o $(OBJFOLDER)/elminterp2.o

$(OBJFOLDER)/bdryelminterp1.o: $(BDRYELMINTERP)
	$(CPPOMPILER) $(CFLAGS) -c $(BDRYELMINTERP_CC1) -o $(OBJFOLDER)/bdryelminterp1.o

$(OBJFOLDER)/bdryelminterp2.o: $(BDRYELMINTERP)
	$(CPPOMPILER) $(CFLAGS) -c $(BDRYELMINTERP_CC2) -o $(OBJFOLDER)/bdryelminterp2.o

$(OBJFOLDER)/rigidity.o: $(RIGIDITY)
	$(CPPOMPILER) $(CFLAGS) -c $(RIGIDITY_CC) -o $(OBJFOLDER)/rigidity.o

$(OBJFOLDER)/lumbts.o: $(LUMBTS)
	$(CPPOMPILER) $(CFLAGS) -c $(LUMBTS_CC) -o $(OBJFOLDER)/lumbts.o

$(OBJFOLDER)/sprmtr.o: $(SPRMTR)
	$(CPPOMPILER) $(CFLAGS) -c $(SPRMTR_CC) -o $(OBJFOLDER)/sprmtr.o

$(OBJFOLDER)/cgm.o: $(CGM) $(SPRMTR)
	$(CPPOMPILER) $(CFLAGS) -c $(CGM_CC) -o $(OBJFOLDER)/cgm.o

./EASYMESH/easymesh:
	$(CCOMPILER) -o ./EASYMESH/easymesh $(EASYMESH) -lm -O3

run: main
	./main < ./in/main.in

matlab: main
	./main < ./in/main.in > plotSolution.m

grid:
	./TRIANGLE/triangle -eaD ./in/square.poly > ./out/square.out

showgrid:
	./TRIANGLE/showme ./in/square.poly

previousgrid:
	./EASYMESH/easymesh example1 +dxf -r -s

clean:
	rm -f main $(OBJFOLDER)/*.o

refineTriangleTest:
	./TRIANGLE/triangle -eaD ./in/square_2dot5e-1.poly > ./out/square_2dot5e-1.out
	./TRIANGLE/showme square_2dot5e-1.poly &
	./main < ./in/square_2dot5e-1.in > ./out/plotSolution_2dot5em1.m 2> square_2dot5e-1.err
	./TRIANGLE/triangle -eaD ./in/square_2dot5e-2.poly > ./out/square_2dot5e-2.out
	./TRIANGLE/showme square_2dot5e-2.poly &
	./main < ./in/square_2dot5e-2.in > ./out/plotSolution_2dot5em2.m 2> square_2dot5e-2.err
	./TRIANGLE/triangle -eaD ./in/square_2dot5e-3.poly > ./out/square_2dot5e-3.out
	./TRIANGLE/showme square_2dot5e-3.poly &
	./main < ./in/square_2dot5e-3.in > ./out/plotSolution_2dot5em3.m 2> square_2dot5e-3.err
	./TRIANGLE/triangle -eaD ./in/square_2dot5e-4.poly > ./out/square_2dot5e-4.out
	./TRIANGLE/showme square_2dot5e-4.poly &
	./main < ./in/square_2dot5e-4.in > ./out/plotSolution_2dot5em4.m 2> square_2dot5e-4.err
	./TRIANGLE/triangle -eaD ./in/square_2dot5e-5.poly > ./out/square_2dot5e-5.out
	./TRIANGLE/showme square_2dot5e-5.poly &
	./main < ./in/square_2dot5e-5.in > ./out/plotSolution_2dot5em5.m 2> square_2dot5e-5.err
	./TRIANGLE/triangle -eaD ./in/square_2dot5e-6.poly > ./out/square_2dot5e-6.out
	./TRIANGLE/showme square_2dot5e-6.poly &
	./main < ./in/square_2dot5e-6.in > ./out/plotSolution_2dot5em6.m 2> square_2dot5e-6.err

refineQuadTest:
	./main < ./in/square4.in > ./out/plotSolution4.m 2> ./out/square4.err
	./main < ./in/square8.in > ./out/plotSolution8.m 2> ./out/square8.err
	./main < ./in/square16.in > ./out/plotSolution16.m 2> ./out/square16.err
	./main < ./in/square32.in > ./out/plotSolution32.m 2> ./out/square32.err
	./main < ./in/square64.in > ./out/plotSolution64.m 2> ./out/square64.err
	./main < ./in/square128.in > ./out/plotSolution128.m 2> ./out/square128.err
	./main < ./in/square256.in > ./out/plotSolution256.m 2> ./out/square256.err
