#include "TwoDimensionalProblem.h"

/*
 * PROBLEMA CONTÍNUO: encontrar u pertencente a U tal que
 *
 *    { -∇·(Κ ∇u) + β ∇u + α u = f    em   Ω,
 *
 * sujeito às condições de fronteira:
 *
 *    {  ∇u·n + γ u = g    em   ΓR
 *    {  ∇u·n       = h    em   ΓN  .
 *    {   u         = w    em   ΓD
 *
 *
 *
 * PROBLEMA DISCRETO: encontrar uh pertencente a Uh tal que
 *
 *    { ∫_Ω (Κ ∇uh ∇vh) dA + ∫_Ω (β ∇uh vh) dA + ∫_Ω (α uhvh) dA + BDRY_LHS
 *    {
 *    {         = ∫_Ω (fvh) dA + BDRY_RHS,
 *
 * para todo uh pertencente a Uh, onde BDRY_LHS e BDRY_RHS são somas
 * de integrais definidas sobre os diferentes tipos de fronteira: Robin,
 * Neumann e Dirichlet. A saber, tais integrais são
 *
 * Robin:
 *      BDRY_LHS: ∫_ΓR  (γ uhvh) dS
 *      BDRY_RHS: ∫_ΓR  (gvh) dS
 *
 * Neumann:
 *      BDRY_LHS: 0
 *      BDRY_RHS: ∫_ΓN  (hvh) dS
 *
 * Dirichlet:
 *      BDRY_LHS: α_D ∫_ΓD  (∇vh·n uh) dS - ∫_ΓD  (∇uh·n vh) dS + β_D ∫_ΓD
 * (uhvh) dS BDRY_RHS: α_D ∫_ΓD  (∇vh·n w) dS + β_D ∫_ΓD  (wvh) dS
 *
 * No caso de Dirichlet, o método de Nitsche é empregado, e duas contantes
 * são necessárias: α_D, que multiplica os termos com gradiente, e
 * β_D que multiplica os termos com avaliações de funções base uh e vh.
 */

/***********************************************************************
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                         FORMA BILINEAR                            **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 ***********************************************************************
 */

bool
BilFormFunc2D::is_symmetric() const
{
    return true;
}

Matrix<2, 2>
BilFormFunc2D::kappa(const LocalPointN& localPoint)
{
    Matrix<2, 2> k;
    k[0][0] = k[1][1] = params_[2];
    k[0][1] = k[1][0] = 0.0;

    return k;
}

Vector<2>
BilFormFunc2D::beta(const LocalPointN& localPoint)
{
    Vector<2> b;
    b[0] = b[1] = 0.;

    return b;
}

double
BilFormFunc2D::alpha(const LocalPointN& localPoint)
{
    return params_[3];
}

double
BilFormFunc2D::gamma(const LocalPointNm1& localPoint)
{
    return 0.5;
}

/***********************************************************************
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                           FORMA LINEAR                            **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 **                                                                   **
 ***********************************************************************
 */

double
LinearFormFunc2D::f(const LocalPointN& localPoint)
{
    double x = stdElemInterp().globalPoint(localPoint)[0];
    double y = stdElemInterp().globalPoint(localPoint)[1];

    return params_[4] * std::sin(M_PI * x) * std::sin(M_PI * y); //-6.*(x+y);
}

double
LinearFormFunc2D::g(const LocalPointNm1& localPoint)
{
    return 0.;
}

double
LinearFormFunc2D::h(const LocalPointNm1& /* localPoint */)
{
    return 0.;
}

double
LinearFormFunc2D::w(const LocalPointNm1& /* localPoint */)
{
    return 0.;
}
